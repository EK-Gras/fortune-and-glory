package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Map;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class GrowthEnchantment extends Enchantment {
    private static final UUID[] UUIDS = {
            UUID.fromString("6bf95c80-23cd-4037-abaf-d260683f3fea"), UUID.fromString("16f0623b-425f-42a5-be29-fe051d94f62b"),
            UUID.fromString("c1c9c375-e1ce-478f-a73c-34bfeafb8194"), UUID.fromString("3d0d2c1d-e133-4449-b930-9c41c49542d9"),
            UUID.fromString("9db8fced-d136-41d6-a098-6670b2aef0c5"), UUID.fromString("65d3b090-e38a-4f38-9c58-ca1638038001")
    };

    public GrowthEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.ARMOR, EquipmentSlotType.values());
    }

    public int getMaxLevel() {
        return 4;
    }

    public static UUID getUUIDForSlot(EquipmentSlotType slot) {
        UUID uuid = null;
        switch (slot) {
            case HEAD:
                uuid = UUIDS[0];
                break;
            case CHEST:
                uuid = UUIDS[1];
                break;
            case LEGS:
                uuid = UUIDS[2];
                break;
            case FEET:
                uuid = UUIDS[3];
                break;
            case MAINHAND:
                uuid = UUIDS[4];
                break;
            case OFFHAND:
                uuid = UUIDS[5];
                break;
        }
        return uuid;
    }

    @SubscribeEvent
    public static void onEquipmentChange(LivingEquipmentChangeEvent event) {
        LivingEntity entity = event.getEntityLiving();

        ItemStack from = event.getFrom();
        Map<Enchantment, Integer> fromEnchants = EnchantmentHelper.getEnchantments(from);
        ItemStack to = event.getTo();
        Map<Enchantment, Integer> toEnchants = EnchantmentHelper.getEnchantments(to);

        if (from.sameItemStackIgnoreDurability(to)) return;

        if (fromEnchants.containsKey(FnGEnchantments.GROWTH.get())) {
            entity.getAttributes().getInstance(Attributes.MAX_HEALTH).
                    removeModifier(getUUIDForSlot(event.getSlot()));
            if (entity.getHealth() > entity.getMaxHealth())
                entity.setHealth(entity.getMaxHealth());
        }

        if (toEnchants.containsKey(FnGEnchantments.GROWTH.get())) {
            int level = toEnchants.get(FnGEnchantments.GROWTH.get());
            UUID uuid = getUUIDForSlot(event.getSlot());
            if (event.getSlot().getType() == EquipmentSlotType.Group.ARMOR || EnchantmentUtils.shouldApplyInHand(to)) {
                AttributeModifier modifier = new AttributeModifier(uuid, "growth_enchant", level, AttributeModifier.Operation.ADDITION);
                entity.getAttributes().getInstance(Attributes.MAX_HEALTH).removeModifier(getUUIDForSlot(event.getSlot()));
                entity.getAttributes().getInstance(Attributes.MAX_HEALTH).addTransientModifier(modifier);
            }
        }
    }
}
