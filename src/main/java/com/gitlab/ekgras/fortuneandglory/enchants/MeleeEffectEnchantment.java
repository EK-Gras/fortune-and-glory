package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Map;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class MeleeEffectEnchantment extends Enchantment {

    // Type of effect
    private Effect effect;

    // Duration of effect (in ticks)
    private int duration;

    public MeleeEffectEnchantment(Effect effect, int duration) {
        super(Enchantment.Rarity.VERY_RARE, EnchantmentType.WEAPON, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
        this.effect = effect;
        this.duration = duration;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    // A LivingDamageEvent listener actually works better than doPostHurt because it can discern between melee attacks and ranged attacks
    @SubscribeEvent
    public static void onAttack(LivingHurtEvent event) {
        if (event.getSource().getDirectEntity() instanceof LivingEntity) {
            LivingEntity attacker = (LivingEntity) event.getSource().getDirectEntity();
            ItemStack stack = attacker.getMainHandItem();
            LivingEntity entity = event.getEntityLiving();
            Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
            for (Enchantment enchant : enchants.keySet()) {
                if (enchant instanceof MeleeEffectEnchantment) {
                    MeleeEffectEnchantment meleeEnchant = (MeleeEffectEnchantment) enchant;
                    entity.addEffect(new EffectInstance(meleeEnchant.effect, meleeEnchant.duration, enchants.get(enchant) - 1));
                }
            }
        }
    }
}
