package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Map;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class DrainshotEnchantment extends Enchantment {

    public DrainshotEnchantment() {
        super(Enchantment.Rarity.VERY_RARE, EnchantmentType.BOW, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
    }

    @Override
    public int getMaxLevel() {
        return 10;
    }

    @SubscribeEvent
    public static void onAttack(LivingDamageEvent event) {
        float damageAmount = event.getAmount();
        if (event.getSource().getDirectEntity() instanceof IEnchantmentProjectileData) {
            IEnchantmentProjectileData projectile = (IEnchantmentProjectileData) event.getSource().getDirectEntity();
            Map<Enchantment, Integer> enchants = projectile.getItemEnchantments();
            if (enchants.containsKey(FnGEnchantments.DRAINSHOT.get())
                    && event.getSource().getEntity() instanceof LivingEntity) {
                ((LivingEntity) event.getSource().getEntity()).heal(0.1f * damageAmount * enchants.get(FnGEnchantments.DRAINSHOT.get()));
            }
        }
    }
}
