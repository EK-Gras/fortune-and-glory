package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Map;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class ProjectileEffectEnchantment extends Enchantment {

    // Type of effect
    private Effect effect;

    // Duration of effect (in ticks)
    private int duration;

    public ProjectileEffectEnchantment(Effect effect, int duration) {
        super(Enchantment.Rarity.VERY_RARE, EnchantmentType.BOW, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
        this.effect = effect;
        this.duration = duration;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @SubscribeEvent
    public static void onAttack(LivingHurtEvent event) {
        if (event.getSource().getDirectEntity() instanceof IEnchantmentProjectileData) {
            IEnchantmentProjectileData projectile = (IEnchantmentProjectileData) event.getSource().getDirectEntity();
            Map<Enchantment, Integer> enchants = projectile.getItemEnchantments();
            LivingEntity entity = event.getEntityLiving();
            for (Enchantment enchant : enchants.keySet()) {
                if (enchant instanceof ProjectileEffectEnchantment) {
                    ProjectileEffectEnchantment projectileEnchant = (ProjectileEffectEnchantment) enchant;
                    entity.addEffect(new EffectInstance(projectileEnchant.effect, projectileEnchant.duration, enchants.get(enchant) - 1));
                }
            }
        }
    }

    // TODO make the effect particle display its actual color
//    @SubscribeEvent
//    public static void onWorldTick(TickEvent.WorldTickEvent event) {
//        ServerWorld world = (ServerWorld) event.world;
//        for (Entity entity : world.getAllEntities()) {
//            if (entity instanceof IEnchantmentProjectileData) {
//                IEnchantmentProjectileData projectile = (IEnchantmentProjectileData) entity;
//                Map<Enchantment, Integer> enchants = projectile.getItemEnchantments();
//                Set<EffectInstance> effects = new HashSet<>();
//                for (Enchantment enchant : enchants.keySet()) {
//                    if (enchant instanceof ProjectileEffectEnchantment) {
//                        ProjectileEffectEnchantment projectileEnchant = (ProjectileEffectEnchantment) enchant;
//                        effects.add(new EffectInstance(projectileEnchant.effect, projectileEnchant.duration, enchants.get(enchant)));
//                    }
//                }
//                if (!effects.isEmpty()) {
//                    int i = PotionUtils.getColor(effects);
//                    if (i != -1) {
//                        double d0 = (double)(i >> 16 & 255) / 255.0D;
//                        double d1 = (double)(i >> 8 & 255) / 255.0D;
//                        double d2 = (double)(i >> 0 & 255) / 255.0D;
//
//                        world.sendParticles(ParticleTypes.ENTITY_EFFECT, entity.getX(), entity.getY(), entity.getZ(), 1, d0, d1, d2, 1);
//                    }
//                }
//            }
//        }
//    }
}
