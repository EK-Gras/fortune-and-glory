package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Arrays;

import static net.minecraft.inventory.EquipmentSlotType.*;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class FrostShellEnchantment extends Enchantment {

    public FrostShellEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.ARMOR,
                new EquipmentSlotType[]{ HEAD, CHEST, LEGS, FEET });
    }

    public int getMaxLevel() {
        return 3;
    }

    @SubscribeEvent
    public static void onAttack(LivingDamageEvent event) {
        float damageAmount = event.getAmount();
        LivingEntity victim = event.getEntityLiving();
        int level = EnchantmentUtils.getTotalEnchantmentLevel(victim, FnGEnchantments.FROST_SHELL.get());
        if (event.getSource().getEntity() instanceof LivingEntity && level >= 0) {
            ElementalPercentages attacker = (ElementalPercentages) event.getSource().getEntity();
            attacker.addFreezePercentage((float)(damageAmount * 3 * level));
        }
    }
}