package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.tags.FnGTags;
import com.gitlab.ekgras.fortuneandglory.util.FallingBlockUtils;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.data.ForgeBlockTagsProvider;
import net.minecraftforge.event.entity.ProjectileImpactEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.lwjgl.system.CallbackI;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class QuakeshotEnchantment extends Enchantment {
    public QuakeshotEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.BOW, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
    }

    @Override
    public int getMaxLevel() {
        return 5;
    }

    @SubscribeEvent
    public static void onProjectileImpact (ProjectileImpactEvent event) {
        if (event.getRayTraceResult() instanceof BlockRayTraceResult && event.getEntity() instanceof IEnchantmentProjectileData) {
            IEnchantmentProjectileData projectileData = (IEnchantmentProjectileData) event.getEntity();
            BlockRayTraceResult result = (BlockRayTraceResult) event.getRayTraceResult();
            World level = event.getEntity().level;
            BlockPos pos = result.getBlockPos();
            BlockState block = level.getBlockState(pos);
            if (projectileData.getItemEnchantments().containsKey(FnGEnchantments.QUAKESHOT.get()) && !level.isClientSide) {
                FallingBlockUtils.disruptCascade((ServerWorld) level, pos, projectileData.getItemEnchantments().get(FnGEnchantments.QUAKESHOT.get()) - 1);
                projectileData.getItemEnchantments().remove(FnGEnchantments.QUAKESHOT.get());
            }
        }
    }
}

