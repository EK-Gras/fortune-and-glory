package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class IceAspectEnchantment extends Enchantment {

    public IceAspectEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.WEAPON, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    // A LivingDamageEvent listener actually works better than doPostHurt because it can discern between melee attacks and ranged attacks
    @SubscribeEvent
    public static void onAttack(LivingHurtEvent event) {
        if (event.getSource().getDirectEntity() instanceof LivingEntity) {
            LivingEntity attacker = (LivingEntity) event.getSource().getDirectEntity();
            ItemStack stack = attacker.getMainHandItem();
            float damage = event.getAmount();
            LivingEntity entity = event.getEntityLiving();
            if (EnchantmentHelper.getEnchantments(stack).containsKey(FnGEnchantments.ICE_ASPECT.get()) && !entity.hasEffect(FnGEffects.FREEZE_RESISTANCE.get())) {
                int level = EnchantmentHelper.getEnchantments(stack).get(FnGEnchantments.ICE_ASPECT.get());
                ElementalPercentages castedEntity = ((ElementalPercentages) entity);
                if (castedEntity.getFreezePercentage() < 100)
                    castedEntity.addFreezePercentage(damage * 2);
                if (castedEntity.getFreezePercentage() >= 100) {
                    entity.addEffect(new EffectInstance(FnGEffects.FREEZE.get(), 60 * level));
                    castedEntity.setFreezePercentage(0);
                }
            }
        }
    }
}
