package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.init.FnGBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.world.World;

import static net.minecraft.inventory.EquipmentSlotType.*;
import static net.minecraft.inventory.EquipmentSlotType.FEET;

public class StoneWalkerEnchantment extends Enchantment {
    public StoneWalkerEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.ARMOR_FEET, new EquipmentSlotType[]{ HEAD, CHEST, LEGS, FEET });
    }

    public int getMaxLevel() {
        return 2;
    }

    public static void onEntityMoved(LivingEntity pLiving, World pLevel, BlockPos pPos, int pLevelConflicting) {
        if (pLiving.isOnGround()) {
            BlockState blockstate = FnGBlocks.HOLLOW_STONE.get().defaultBlockState();
            float f = (float)Math.min(16, 2 + pLevelConflicting);
            BlockPos.Mutable blockpos$mutable = new BlockPos.Mutable();

            for(BlockPos blockpos : BlockPos.betweenClosed(pPos.offset((double)(-f), -1.0D, (double)(-f)), pPos.offset((double)f, -1.0D, (double)f))) {
                if (blockpos.closerThan(pLiving.position(), (double)f)) {
                    blockpos$mutable.set(blockpos.getX(), blockpos.getY() + 1, blockpos.getZ());
                    BlockState blockstate1 = pLevel.getBlockState(blockpos$mutable);
                    if (blockstate1.isAir(pLevel, blockpos$mutable)) {
                        BlockState blockstate2 = pLevel.getBlockState(blockpos);
                        boolean isFull = blockstate2.getBlock() == Blocks.LAVA && blockstate2.getValue(FlowingFluidBlock.LEVEL) == 0;
                        if (blockstate2.getMaterial() == Material.LAVA && isFull && blockstate.canSurvive(pLevel, blockpos) && pLevel.isUnobstructed(blockstate, blockpos, ISelectionContext.empty()) && !net.minecraftforge.event.ForgeEventFactory.onBlockPlace(pLiving, net.minecraftforge.common.util.BlockSnapshot.create(pLevel.dimension(), pLevel, blockpos), net.minecraft.util.Direction.UP)) {
                            pLevel.setBlockAndUpdate(blockpos, blockstate);
                            pLevel.getBlockTicks().scheduleTick(blockpos, FnGBlocks.HOLLOW_STONE.get(), MathHelper.nextInt(pLiving.getRandom(), 60, 120));
                        }
                    }
                }
            }

        }
    }

}