package com.gitlab.ekgras.fortuneandglory.enchants;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.inventory.EquipmentSlotType;

// TODO actually code refill enchantment
public class RefillEnchantment extends Enchantment {
    public RefillEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.ARMOR, EquipmentSlotType.values());
    }

    public int getMaxLevel() {
        return 4;
    }


}

