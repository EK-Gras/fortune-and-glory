package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.items.CoinItem;
import net.minecraft.block.AbstractFireBlock;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;
import java.util.Map;

import static net.minecraftforge.common.util.Constants.BlockFlags.DEFAULT_AND_RERENDER;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class FirestepEnchantment extends Enchantment {

    public FirestepEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.ARMOR_FEET, new EquipmentSlotType[]{ EquipmentSlotType.FEET });
    }

    private static final Map<PlayerEntity, BlockPos> previousTickBlockMap = new HashMap<>();
    private static final Map<PlayerEntity, BlockPos> lastBlockMap = new HashMap<>();

    @SubscribeEvent
    public static void onTick(TickEvent.PlayerTickEvent event) {
        if (!FnGEnchantments.FIRESTEP.isPresent()) return;

        PlayerEntity player = event.player;
        int level = EnchantmentHelper.getEnchantmentLevel(FnGEnchantments.FIRESTEP.get(), player);
        if (player.getMainHandItem().getItem() instanceof CoinItem)
            level += EnchantmentHelper.getItemEnchantmentLevel(FnGEnchantments.FIRESTEP.get(), player.getMainHandItem());
        if (player.getOffhandItem().getItem() instanceof CoinItem)
            level += EnchantmentHelper.getItemEnchantmentLevel(FnGEnchantments.FIRESTEP.get(), player.getOffhandItem());
        World world = player.getCommandSenderWorld();

        // Need to make sure this runs on server only; otherwise previousTickBlockMap is updated twice per tick
        // World.setBlock can update the client from the server anyways so there's no reason for this to run on the client
        if (level == 0 || world.isClientSide) return;

        BlockPos pos = event.player.blockPosition();
        BlockPos placePos = null;

        if (world.getBlockState(pos).getBlock() instanceof AbstractFireBlock) world.removeBlock(pos, false);

        if (!previousTickBlockMap.containsKey(player))
            previousTickBlockMap.put(player, pos);

        if (!lastBlockMap.containsKey(player))
            lastBlockMap.put(player, pos);

        if (!previousTickBlockMap.get(player).equals(pos)) {
            if (!lastBlockMap.get(player).equals(pos) && !lastBlockMap.get(player).equals(previousTickBlockMap.get(player))) {
                placePos = lastBlockMap.get(player);
            }
            lastBlockMap.put(player, previousTickBlockMap.get(player));
        }
        previousTickBlockMap.put(player, pos);

        if (placePos == null || !AbstractFireBlock.canBePlacedAt(world, placePos, Direction.UP) || player.isShiftKeyDown()) return;
        world.setBlock(placePos, AbstractFireBlock.getState(world, placePos), DEFAULT_AND_RERENDER);
    }
}
