package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.items.BoomerangItem;
import com.gitlab.ekgras.fortuneandglory.items.CoinItem;
import com.gitlab.ekgras.fortuneandglory.items.EnergyItem;
import com.gitlab.ekgras.fortuneandglory.items.ThrowableItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.util.Hand;

import static net.minecraft.inventory.EquipmentSlotType.*;

public class EnchantmentUtils {

    // I kinda hate this lol
    public static boolean shouldApplyInHand(Item item) {
        return item instanceof TieredItem || item instanceof EnergyItem || item instanceof ShootableItem ||
                item instanceof TridentItem || item instanceof BoomerangItem || item instanceof FishingRodItem ||
                item instanceof ThrowableItem || item instanceof CoinItem;
    }

    public static boolean shouldApplyInHand(ItemStack stack) {
        Item item = stack.getItem();
        return shouldApplyInHand(item);
    }

    public static int getTotalEnchantmentLevel(LivingEntity livingEntity, Enchantment enchantment) {
        int level = 0;
        level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemBySlot(HEAD));
        level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemBySlot(CHEST));
        level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemBySlot(LEGS));
        level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemBySlot(FEET));
        if (shouldApplyInHand(livingEntity.getItemInHand(Hand.MAIN_HAND)))
            level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemInHand(Hand.MAIN_HAND));
        if (shouldApplyInHand(livingEntity.getItemInHand(Hand.OFF_HAND)))
            level += EnchantmentHelper.getItemEnchantmentLevel(enchantment, livingEntity.getItemInHand(Hand.OFF_HAND));
        return level;
    }
}
