package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.items.CoinItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Potion;
import net.minecraft.util.Hand;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.world.NoteBlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.minecraft.inventory.EquipmentSlotType.*;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class UserEffectEnchantment extends Enchantment {

    // Type of effect
    private Effect effect;

    @Override
    public String getDescriptionId() {
        return super.getDescriptionId();
    }

    public UserEffectEnchantment(Effect effect) {
        super(Enchantment.Rarity.VERY_RARE, EnchantmentType.ARMOR, new EquipmentSlotType[]{ HEAD, CHEST, LEGS, EquipmentSlotType.FEET });
        this.effect = effect;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    // This seems potentially very laggy. Might want to move to EquipmentChangeEvent
    @SubscribeEvent
    public static void onTick(TickEvent.PlayerTickEvent event) {
        PlayerEntity player = event.player;
        Map<Effect, Integer> effects = new HashMap<>();
        if (!player.getItemBySlot(HEAD).isEmpty())
            addAllUserEnchantments(player.getItemBySlot(HEAD), effects);
        if (!player.getItemBySlot(CHEST).isEmpty())
            addAllUserEnchantments(player.getItemBySlot(CHEST), effects);
        if (!player.getItemBySlot(LEGS).isEmpty())
            addAllUserEnchantments(player.getItemBySlot(LEGS), effects);
        if (!player.getItemBySlot(FEET).isEmpty())
            addAllUserEnchantments(player.getItemBySlot(FEET), effects);
        if (EnchantmentUtils.shouldApplyInHand(player.getItemInHand(Hand.MAIN_HAND)))
            addAllUserEnchantments(player.getItemInHand(Hand.MAIN_HAND), effects);
        if (EnchantmentUtils.shouldApplyInHand(player.getItemInHand(Hand.OFF_HAND)))
            addAllUserEnchantments(player.getItemInHand(Hand.OFF_HAND), effects);
        for (Effect effect : effects.keySet()) {
            player.addEffect(new EffectInstance(effect, 1, effects.get(effect)));
        }
    }

    public static void addAllUserEnchantments(ItemStack stack, Map<Effect, Integer> effects) {
        EnchantmentHelper.getEnchantments(stack).entrySet().stream()
                .filter(e -> e.getKey() instanceof UserEffectEnchantment).collect(Collectors.toSet()).forEach(
                        e -> {
                            UserEffectEnchantment enchantment = ((UserEffectEnchantment) e.getKey());
                            Effect effect = enchantment.effect;
                            if (effects.containsKey(effect)) effects.put
                                    (effect, effects.get(effect) + EnchantmentHelper.getItemEnchantmentLevel(enchantment, stack));
                            else effects.put(effect, EnchantmentHelper.getItemEnchantmentLevel(enchantment, stack) - 1);
                        }
                );

    }

}
