package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.init.FnGParticles;
import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.particles.IParticleData;
import net.minecraft.potion.EffectInstance;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Map;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class ElectroshotEnchantment extends Enchantment {

    public ElectroshotEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.BOW, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }

    @SubscribeEvent
    public static void onAttack(LivingHurtEvent event) {
        if (event.getSource().getDirectEntity() instanceof IEnchantmentProjectileData) {
            IEnchantmentProjectileData projectile = (IEnchantmentProjectileData) event.getSource().getDirectEntity();
            Map<Enchantment, Integer> enchants = projectile.getItemEnchantments();
            LivingEntity entity = event.getEntityLiving();
            if (enchants.containsKey(FnGEnchantments.ELECTROSHOT.get()) && !entity.hasEffect(FnGEffects.SHOCK_RESISTANCE.get())) {
                float damage = event.getAmount();
                int level = enchants.get(FnGEnchantments.ELECTROSHOT.get());
                ElementalPercentages castedEntity = ((ElementalPercentages) entity);
                if (castedEntity.getShockPercentage() < 100)
                    castedEntity.addShockPercentage(damage * 2);
                if (castedEntity.getShockPercentage() >= 100) {
                    entity.addEffect(new EffectInstance(FnGEffects.SHOCK.get(), 60 * level));
                    castedEntity.setShockPercentage(0);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onWorldTick(TickEvent.WorldTickEvent event) {
        ServerWorld world = (ServerWorld) event.world;
        for (Entity entity : world.getAllEntities()) {
            if (entity instanceof IEnchantmentProjectileData) {
                IEnchantmentProjectileData projectile = (IEnchantmentProjectileData) entity;
                if (projectile.getItemEnchantments().containsKey(FnGEnchantments.ELECTROSHOT.get())) {
                    world.sendParticles((IParticleData) FnGParticles.SPARK.get(), entity.getX(), entity.getEyeY(), entity.getZ(), 1, 0, 0, 0, 1);
                }
            }
        }
    }
}
