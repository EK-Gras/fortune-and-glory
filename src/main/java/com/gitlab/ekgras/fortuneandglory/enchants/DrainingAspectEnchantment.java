package com.gitlab.ekgras.fortuneandglory.enchants;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class DrainingAspectEnchantment extends Enchantment  {

    public DrainingAspectEnchantment() {
        super(Rarity.VERY_RARE, EnchantmentType.WEAPON, new EquipmentSlotType[]{ EquipmentSlotType.MAINHAND });
    }

    @Override
    public int getMaxLevel() {
        return 10;
    }

    @SubscribeEvent
    public static void onAttack(LivingDamageEvent event) {
        float damageAmount = event.getAmount();
        if (event.getSource().getDirectEntity() instanceof LivingEntity) {
            LivingEntity attacker = (LivingEntity) event.getSource().getDirectEntity();
            int lifestealLevel = EnchantmentHelper.getItemEnchantmentLevel(
                FnGEnchantments.DRAINING_ASPECT.get(), attacker.getMainHandItem());

            if (lifestealLevel > 0) {
                attacker.heal(0.1f * lifestealLevel * damageAmount);
            }
        }
    }
}
