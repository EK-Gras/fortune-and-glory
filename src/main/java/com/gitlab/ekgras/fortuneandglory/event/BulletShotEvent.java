package com.gitlab.ekgras.fortuneandglory.event;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.world.NoteBlockEvent;
import net.minecraftforge.eventbus.api.Event;

public class BulletShotEvent extends Event {

    private final PlayerEntity player;
    private final ItemStack gun;
    private final World world;

    // This is the standard BulletShotEvent. It fires when a player shoots a gun (Does NOT fire if the gun is reloading or in cooldown and therefore doesn't shoot).

    public BulletShotEvent(PlayerEntity playerIn, ItemStack gunIn, World worldIn) {
        this.player = playerIn;
        this.gun = gunIn;
        this.world = worldIn;
    }

    public BulletShotEvent(PlayerEntity playerIn) {
        this.player = playerIn;
        this.gun = player.getItemBySlot(EquipmentSlotType.MAINHAND);
        this.world = player.getCommandSenderWorld();
    }

    public PlayerEntity getPlayer() {
        return this.player;
    }

    public ItemStack getGun() {
        return this.gun;
    }

    public World getWorld() {
        return this.world;
    }

    public boolean isCancelable() {
        return false;
    }

    //GunTickEvent fires on the tick after the gun is fired. It's used for automatic guns in order to hold down left click in order to shoot.

    public static class GunTickEvent extends BulletShotEvent {

        public GunTickEvent(PlayerEntity playerIn, ItemStack gunIn, World worldIn) {
            super(playerIn, gunIn, worldIn);
        }

    }

}
