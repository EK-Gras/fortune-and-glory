package com.gitlab.ekgras.fortuneandglory.abilities;

import net.minecraft.entity.LivingEntity;

/**
 * This is the basic Ability interface.
 * I created abilities because I noticed that there were several things in the game design document which would ultimately cause a player to perform a nonspecific action.
 * Hence, abilities are a way to make a standard class for a weapon that may need to do a large variety of things (for example, charge-based weapons take an ability in their constructor).
 */
public interface Ability {

    default boolean trigger(LivingEntity entityIn) {
        return true;
    }
}
