package com.gitlab.ekgras.fortuneandglory.abilities;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.EnergyBallEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public class FireEnergyBallAbility implements Ability {

    private boolean noGravity;
    private boolean constantMomentum;
    private int life;
    private BasicParticleType particle;
    private boolean invisible;
    private float velocity;
    private float inaccuracy;
    private double damage;
    private SoundEvent sound;

    public FireEnergyBallAbility(boolean noGravity, boolean constantMomentum, int life, BasicParticleType particle, boolean invisible, float velocity, float inaccuracy, double damage, SoundEvent sound) {
        this.noGravity = noGravity;
        this.constantMomentum = constantMomentum;
        this.life = life;
        this.particle = particle;
        this.invisible = invisible;
        this.velocity = velocity;
        this.inaccuracy = inaccuracy;
        this.damage = damage;
        this.sound = sound;
    }

    public boolean trigger(LivingEntity entityIn) {
        World world = entityIn.getCommandSenderWorld();
        EnergyBallEntity ball = new EnergyBallEntity(FnGEntities.ENERGY_BALL.get(), entityIn, world, noGravity, constantMomentum, life, particle, invisible, false);
        ball.shoot(entityIn, entityIn.xRot, entityIn.yRot, velocity, inaccuracy, damage, sound);
        world.addFreshEntity(ball);
        return true;
    }
}
