package com.gitlab.ekgras.fortuneandglory.abilities;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.DamageSource;
import org.lwjgl.system.CallbackI;

public class DamageAbility implements Ability{
    private float damage;

    public DamageAbility(float damage) {
        this.damage = damage;
    }

    @Override
    public boolean trigger(LivingEntity entity) {
        DamageSource source;
        if (entity instanceof PlayerEntity) {
            source = DamageSource.playerAttack((PlayerEntity) entity);
        }
        else {
            source = DamageSource.mobAttack(entity);
        }
        source.setMagic();
        entity.hurt(source, damage);
        return true;
    }
}
