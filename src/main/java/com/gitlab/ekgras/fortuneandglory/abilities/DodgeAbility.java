package com.gitlab.ekgras.fortuneandglory.abilities;

import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import net.minecraft.client.GameConfiguration;
import net.minecraft.client.GameSettings;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3d;

public class DodgeAbility implements Ability {

    private float speed;
    private float airspeed;
    private int invincibiltyFrames;

    public DodgeAbility (float speedIn, float airSpeedIn, int iFramesIn) {
        this.speed = speedIn;
        this.airspeed = airSpeedIn;
        this.invincibiltyFrames = iFramesIn;
    }

    public boolean trigger(LivingEntity entityIn) {
        if (entityIn.getCommandSenderWorld().isClientSide()) {
            Minecraft mc = Minecraft.getInstance();
            GameSettings gameSettings = mc.options;

            // Set float s to the normal speed if the player is on the ground, or the modified air speed if they are not. s is used for the speed modifier when applying the motion
            float s = entityIn.isOnGround() ? speed : airspeed;

            double x = Math.sin(Math.toRadians(entityIn.yRot + 180));
            double z = -(Math.cos(Math.toRadians(entityIn.yRot + 180)));

            // We need to rotate the vector 45 DEGREES, and Math.sin expects a RADIAN value, so we must convert 45 degrees to radians first.
            double rad45 = Math.toRadians(45);

            // The vector (xr, zr) represents the vector (x, z) rotated 45 degrees
            double xr = ((Math.cos(rad45) * x) - (Math.cos(rad45) * z));
            double zr = ((Math.sin(rad45) * x) + (Math.cos(rad45) * z));

            boolean up = gameSettings.keyUp.isDown();
            boolean down = gameSettings.keyDown.isDown();
            boolean left = gameSettings.keyLeft.isDown();
            boolean right = gameSettings.keyRight.isDown();

            // FORWARD
            if ((up && !down) && (right == left)) {
                entityIn.setDeltaMovement(x * s, 0, z * s);
            }

            // FORWARD/RIGHT
            else if (up && right && !down && !left) {
                entityIn.setDeltaMovement(xr * s, 0, zr * s);
            }

            // RIGHT
            else if ((up == down) && (right && !left)) {
                entityIn.setDeltaMovement(-z * s, 0, x * s);
            }

            // BACK/RIGHT
            else if (!up && right && down && !left) {
                entityIn.setDeltaMovement(-zr * s, 0, xr * s);
            }

            // BACK
            else if ((!up && down) && (right == left)) {
                entityIn.setDeltaMovement(-x * s, 0, -z * s);
            }

            // BACK/LEFT
            else if (!up && !right && down && left) {
                entityIn.setDeltaMovement(-xr * s, 0, -zr * s);
            }

            // LEFT
            else if ((up == down) && (!right && left)) {
                entityIn.setDeltaMovement(z * s, 0, -x * s);
            }

            // FORWARD/LEFT
            else if (up && !right && !down && left) {
                entityIn.setDeltaMovement(zr * s, 0, -xr * s);
            }

            // ALL OTHER COMBINATIONS (Also moves player forward)
            else {
                entityIn.setDeltaMovement(x * s, 0, z * s);
            }

            entityIn.invulnerableTime = invincibiltyFrames;
        }
        return true;
    }
}
