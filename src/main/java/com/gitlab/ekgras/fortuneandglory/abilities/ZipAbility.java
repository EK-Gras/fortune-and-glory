package com.gitlab.ekgras.fortuneandglory.abilities;

import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;

public class ZipAbility implements Ability {

    private double speed;
    private SoundEvent sound;
    private float volume;
    private float pitch;
    private int floatDuration;


    public ZipAbility(double speedIn, SoundEvent soundIn, float volumeIn, float pitchIn) {
        this.speed = speedIn;
        this.sound = soundIn;
        this.volume = volumeIn;
        this.pitch = pitchIn;
        this.floatDuration = 0;
    }

    public ZipAbility(double speedIn, int floatDurationIn, SoundEvent soundIn, float volumeIn, float pitchIn) {
        this.speed = speedIn;
        this.sound = soundIn;
        this.volume = volumeIn;
        this.pitch = pitchIn;
        this.floatDuration = floatDurationIn;
    }


    @Override
    public boolean trigger(LivingEntity entityIn) {
        // Get the entity's vector for the direction it's facing
        Vector3d vector = entityIn.getLookAngle();
        // Play the passed in sound
        entityIn.getCommandSenderWorld().playSound(null, entityIn.getX(), entityIn.getY(), entityIn.getZ(), sound, SoundCategory.PLAYERS, volume, pitch);
        // This is broken, currently. About 50% of the time it just doesn't work.
        // If floatDuration is above 50, set the entity to not have gravity on every tick, and at the end set it back to false.
        if (floatDuration > 0) {
            // On every tick until floatDuration runs out, set the player to have no gravity
            for (int i = 0; i < floatDuration; i++) {
                TickScheduler.schedule(i, entityIn.getCommandSenderWorld().isClientSide(), () -> {
                        entityIn.setNoGravity(true);
                    }
                );
            }
            // Schedule the player to return to normal gravity once floatDuration runs out
            TickScheduler.schedule(floatDuration, entityIn.getCommandSenderWorld().isClientSide(), () -> {
                    entityIn.setNoGravity(false);
                }
            );
        }
        // Set the entity's motion to their vectors times the speed.
        entityIn.setDeltaMovement(vector.x * speed, vector.y * speed, vector.z * speed);
        // Return true meaning the ability succeeded in triggering
        return true;
    }
}
