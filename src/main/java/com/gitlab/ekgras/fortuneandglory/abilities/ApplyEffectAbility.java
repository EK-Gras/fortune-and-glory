package com.gitlab.ekgras.fortuneandglory.abilities;

import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;

public class ApplyEffectAbility implements Ability {

    private Effect effect;
    private int duration;
    private int amplifier;
    private boolean showParticles;
    private SoundEvent sound;
    private float volume;
    private float pitch;

    public ApplyEffectAbility(Effect effectIn, int durationIn, int amplifierIn, boolean particles, SoundEvent soundIn, float volumeIn, float pitchIn) {
        this.effect = effectIn;
        this.duration = durationIn;
        this.amplifier = amplifierIn;
        this.showParticles = particles;
        this.sound = soundIn;
        this.volume = volumeIn;
        this.pitch = pitchIn;
    }

    public boolean trigger(LivingEntity entity) {
        entity.getCommandSenderWorld().playSound(null, entity.getX(), entity.getY(), entity.getZ(), sound, SoundCategory.PLAYERS, volume, pitch);
        entity.addEffect(new EffectInstance(effect, duration, amplifier, false, showParticles));
        return true;
    }
}
