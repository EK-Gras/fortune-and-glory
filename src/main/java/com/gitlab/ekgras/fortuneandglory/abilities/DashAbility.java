package com.gitlab.ekgras.fortuneandglory.abilities;

import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;

public class DashAbility implements Ability {

    private double speed;
    private SoundEvent sound;
    private float volume;
    private float pitch;
    private boolean groundOnly;

    public DashAbility(double speed, SoundEvent sound, float volume, float pitch, boolean mustBeOnGround) {
        this.speed = speed;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.groundOnly = mustBeOnGround;
    }

    @Override
    public boolean trigger(LivingEntity entity) {
        if (!this.groundOnly || entity.isOnGround()) {
            // Get the entity's vector for the direction it's facing
            Vector3d vector = entity.getLookAngle();
            // Play the passed in sound
            entity.getCommandSenderWorld().playSound(null, entity.getX(), entity.getY(), entity.getZ(), sound, SoundCategory.PLAYERS, volume, pitch);
            //Set the entity's motion to their vectors times the speed
            entity.setDeltaMovement(vector.x * speed, entity.getDeltaMovement().y, vector.z * speed);
            return true;
        }
        return false;
    }

}
