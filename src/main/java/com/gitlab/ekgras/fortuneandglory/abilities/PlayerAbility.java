package com.gitlab.ekgras.fortuneandglory.abilities;

import net.minecraft.entity.LivingEntity;

/*
This is an extension of the ability class which is used for abilities that are player-only, with the method requiring a PlayerEntity to fire.
 */

public abstract class PlayerAbility implements Ability {
    public PlayerAbility() {}

    public boolean trigger(LivingEntity entityIn) {
        return true;
    }
}
