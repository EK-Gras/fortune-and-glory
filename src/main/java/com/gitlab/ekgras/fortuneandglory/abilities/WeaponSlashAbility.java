package com.gitlab.ekgras.fortuneandglory.abilities;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.WeaponSlashEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class WeaponSlashAbility implements Ability {

    private int damage;
    private int life;
    private int speed;
    private SoundEvent sound;
    private float volume;
    private float pitch;

    public WeaponSlashAbility(int damageIn, int lifeIn, int speedIn, SoundEvent soundIn, float volumeIn, float pitchIn) {
        this.damage = damageIn;
        this.life = lifeIn;
        this.speed = speedIn;
        this.sound = soundIn;
        this.volume = volumeIn;
        this.pitch = pitchIn;
    }

    @Override
    public boolean trigger(LivingEntity entity) {
        World world = entity.getCommandSenderWorld();
        double x = Math.sin(Math.toRadians(entity.yRot + 180)) * speed;
        double z = -(Math.cos(Math.toRadians(entity.yRot + 180))) * speed;
        WeaponSlashEntity weaponSlashEntity = new WeaponSlashEntity(world, entity, x, 0d, z, life, damage);
        weaponSlashEntity.setDeltaMovement(x, 0, z);
        weaponSlashEntity.yRot = entity.yRot;
        weaponSlashEntity.setPos(entity.getX(), entity.getY() + 0.5, entity.getZ());
        entity.getCommandSenderWorld().playSound(null, entity.getX(), entity.getY(), entity.getZ(), sound, SoundCategory.PLAYERS, volume, pitch);
        world.addFreshEntity(weaponSlashEntity);
        return true;
    }

}
