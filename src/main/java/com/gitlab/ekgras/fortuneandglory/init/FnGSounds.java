package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FnGSounds {
    public static final DeferredRegister<SoundEvent> SOUND_EVENTS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, FortuneAndGlory.MODID);

    public static final RegistryObject<SoundEvent> ENTITY_BULLET_SHOOT = SOUND_EVENTS.register("entity.bullet.shoot",
            () -> new SoundEvent(new ResourceLocation(FortuneAndGlory.MODID, "entity.bullet.shoot")));
    public static final RegistryObject<SoundEvent> ENTITY_BULLET_HIT = SOUND_EVENTS.register("entity.bullet.hit",
            () -> new SoundEvent(new ResourceLocation(FortuneAndGlory.MODID, "entity.bullet.hit")));
    public static final RegistryObject<SoundEvent> ITEM_GUN_START_RELOAD = SOUND_EVENTS.register("item.gun.start_reload",
            () -> new SoundEvent(new ResourceLocation(FortuneAndGlory.MODID, "item.gun.start_reload")));
    public static final RegistryObject<SoundEvent> ITEM_GUN_FINISH_RELOAD = SOUND_EVENTS.register("item.gun.finish_reload",
            () -> new SoundEvent(new ResourceLocation(FortuneAndGlory.MODID, "item.gun.finish_reload")));
}
