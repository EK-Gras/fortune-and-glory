package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.render.*;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.BowArrowEntity;
import com.gitlab.ekgras.fortuneandglory.items.BoomerangItem;
import com.gitlab.ekgras.fortuneandglory.items.FnGCrossbowItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.TippedArrowRenderer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.util.InputMappings;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import org.lwjgl.glfw.GLFW;

import java.util.Map;

import static net.minecraft.item.ItemModelsProperties.register;

@OnlyIn(Dist.CLIENT)
@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class FnGClientRegistry {

    public static KeyBinding KEY_BADGE_1 = new KeyBinding("key.badge_1", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_C, "key.categories.fortune-and-glory");
    public static KeyBinding KEY_BADGE_2 = new KeyBinding("key.badge_2", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_V, "key.categories.fortune-and-glory");
    public static KeyBinding KEY_BADGE_3 = new KeyBinding("key.badge_3", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_B, "key.categories.fortune-and-glory");
    public static KeyBinding KEY_ALT_ITEM_FUNCTION = new KeyBinding("key.alternate_item_function", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_R, "key.categories.fortune-and-glory");

    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event) {
        Minecraft mc = event.getMinecraftSupplier().get();
        ItemRenderer itemRenderer = mc.getItemRenderer();

        // Entity renderers
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.WEAPON_SLASH.get(), WeaponSlashRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.BULLET.get(), BulletRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.SPEAR.get(), SpearRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.BOOMERANG.get(), manager -> new BoomerangRenderer(manager, itemRenderer));
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.THROWABLE.get(), manager -> new ItemProjectileRenderer<>(manager, itemRenderer));
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.ENERGY_BALL.get(), EnergyBallRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.DART.get(), DartRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.FNG_ARROW.get(), BowArrowRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(FnGEntities.MINION.get(), MinionRenderer::new);

        // Item model overrides
        Item[] fishingRods = {FnGItems.CHAMPIONS_FISHING_ROD.get()};
        for (Item fishingRod : fishingRods) {
            ItemModelsProperties.register(fishingRod, new ResourceLocation("cast"), (stack, world, entity) -> {
                if (entity == null) {
                    return 0.0F;
                } else {
                    boolean flag = entity.getMainHandItem() == stack;
                    boolean flag1 = entity.getOffhandItem() == stack;
                    if (entity.getMainHandItem().getItem() instanceof FishingRodItem) {
                        flag1 = false;
                    }

                    return (flag || flag1) && entity instanceof PlayerEntity && ((PlayerEntity)entity).fishing != null ? 1.0F : 0.0F;
                }
            });
        }

        Item[] bows = {FnGItems.IRON_LONGBOW.get(), FnGItems.IRON_REPEATER.get(), FnGItems.ADMIN_REPEATER.get(),
                FnGItems.DUALITY.get(), FnGItems.COCYTUS.get()};
        for (Item bow : bows) {
            register(bow, new ResourceLocation("pull"), (stack, world, entity) -> {
                    if (entity == null) {
                        return 0.0F;
                    } else {
                        Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
                        int useTimeBonus = 0;
                        if (enchants.containsKey(Enchantments.QUICK_CHARGE)) {
                            useTimeBonus += enchants.get(Enchantments.QUICK_CHARGE) * 5;
                        }
                        return (entity.getUseItem().getItem() instanceof ShootableItem) ? (float) (stack.getUseDuration() - entity.getUseItemRemainingTicks()) / (20.0F - useTimeBonus) : 0.0F;
                    }
                }
            );
                register(bow, new ResourceLocation("pulling"), (stack, world, entity) -> {
                    return entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F;
                }
            );
        }

        Item[] crossbows = {FnGItems.MACHINE_CROSSBOW.get()};
        for (Item crossbow : crossbows) {
            register(crossbow, new ResourceLocation("pull"), (stack, world, entity) -> {
                if (entity == null) {
                    return 0.0F;
                } else {
                    return FnGCrossbowItem.isCharged(stack) ? 0.0F : (float)(stack.getUseDuration() - entity.getUseItemRemainingTicks()) / (float)FnGCrossbowItem.getChargeDuration(stack);
                }
            });
            register(crossbow, new ResourceLocation("pulling"), (stack, world, entity) -> {
                return entity != null && entity.isUsingItem() && entity.getUseItem() == stack && !FnGCrossbowItem.isCharged(stack) ? 1.0F : 0.0F;
            });
            register(crossbow, new ResourceLocation("charged"), (stack, world, entity) -> {
                return entity != null && FnGCrossbowItem.isCharged(stack) ? 1.0F : 0.0F;
            });
            register(crossbow, new ResourceLocation("firework"), (stack, world, entity) -> {
                return entity != null && FnGCrossbowItem.isCharged(stack) && CrossbowItem.containsChargedProjectile(stack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F;
            });
        }

        Item[] spears = {FnGItems.IRON_SPEAR.get()};
        for (Item spear : spears) {
            register(spear, new ResourceLocation("throwing"), (stack, world, entity) -> {
                    return entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F;
                }
            );
        }

        Item[] boomerangs = {FnGItems.IRON_BOOMERANG.get(), FnGItems.BLIZZARD_CUTTER.get()};
        for (Item boomerang : boomerangs) {
            register(boomerang, new ResourceLocation("throwing"), (stack, world, entity) -> {
                    return (stack.getItem() instanceof BoomerangItem) && stack.getTag().getBoolean("throwing") ? 1.0F : 0.0F;
                }
            );
        }

        // Block render types
        event.enqueueWork(() -> {
            RenderTypeLookup.setRenderLayer(FnGBlocks.VAULT_BOMB.get(), RenderType.cutout());
        });

        // Keybinds
        ClientRegistry.registerKeyBinding(KEY_ALT_ITEM_FUNCTION);
        ClientRegistry.registerKeyBinding(KEY_BADGE_1);
        ClientRegistry.registerKeyBinding(KEY_BADGE_2);
        ClientRegistry.registerKeyBinding(KEY_BADGE_3);
    }
}
