package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.tileentities.VaultBlockTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FnGTileEntities {

    public static final DeferredRegister<TileEntityType<?>> TILE_ENTITY_TYPES =
            DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, FortuneAndGlory.MODID);

    public static final RegistryObject<TileEntityType<VaultBlockTileEntity>> VAULT_BLOCK = TILE_ENTITY_TYPES.register(
            "vault_block_tile",
            () -> TileEntityType.Builder
                    .of(VaultBlockTileEntity::new, FnGBlocks.VAULT.get())
                    .build(null)
    );

    public static final RegistryObject<TileEntityType<VaultBlockTileEntity>> VAULT_BLOCK_SLAB = TILE_ENTITY_TYPES.register(
            "vault_block_slab_tile",
            () -> TileEntityType.Builder
                    .of(VaultBlockTileEntity::new, FnGBlocks.VAULT_SLAB.get())
                    .build(null)
    );

    public static final RegistryObject<TileEntityType<VaultBlockTileEntity>> VAULT_BLOCK_STAIRS = TILE_ENTITY_TYPES.register(
            "vault_block_stairs_tile",
            () -> TileEntityType.Builder
                    .of(VaultBlockTileEntity::new, FnGBlocks.VAULT_STAIRS.get())
                    .build(null)
    );

}
