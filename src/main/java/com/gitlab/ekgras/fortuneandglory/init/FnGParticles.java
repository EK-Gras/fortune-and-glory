package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.particle.FrostParticle;
import com.gitlab.ekgras.fortuneandglory.particle.SparkParticle;
import net.minecraft.client.Minecraft;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class FnGParticles {
    public static final DeferredRegister<ParticleType<?>> PARTICLE_TYPES = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, FortuneAndGlory.MODID);

    public static final RegistryObject<ParticleType<BasicParticleType>> FROST = PARTICLE_TYPES.register("frost",
            () -> new BasicParticleType(false));
    public static final RegistryObject<ParticleType<BasicParticleType>> SPARK = PARTICLE_TYPES.register("spark",
            () -> new BasicParticleType(false));

    @SubscribeEvent
    public static void onRegisterParticles(ParticleFactoryRegisterEvent event) {
        Minecraft.getInstance().particleEngine.register(FnGParticles.FROST.get(), FrostParticle.Factory::new);
        Minecraft.getInstance().particleEngine.register(FnGParticles.SPARK.get(), SparkParticle.Factory::new);
    }
}
