package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.block.*;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FnGBlocks {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, FortuneAndGlory.MODID);

    private static VaultBlock defaultVaultBlock = new VaultBlock(AbstractBlock.Properties.of(Material.METAL).strength(0.8F, 1200.0F));

    public static final RegistryObject<Block> VAULT = BLOCKS.register("vault_block",
            () -> new VaultBlock(AbstractBlock.Properties.of(Material.METAL).strength(0.8F, 1200.0F)));
    public static final RegistryObject<Block> VAULT_SLAB = BLOCKS.register("vault_block_slab",
            () -> new VaultBlockSlab(AbstractBlock.Properties.of(Material.METAL).strength(0.8F, 1200.0F)));
    public static final RegistryObject<Block> VAULT_STAIRS = BLOCKS.register("vault_block_stairs",
            () -> new VaultBlockStairs(()-> defaultVaultBlock.defaultBlockState(), AbstractBlock.Properties.of(Material.METAL).strength(0.8F, 1200.0F)));
    public static final RegistryObject<Block> VAULT_BOMB = BLOCKS.register("vault_bomb_block",
            () -> new VaultBombBlock(AbstractBlock.Properties.of(Material.METAL).strength(3.0F, 3.0F).noOcclusion()));
    public static final RegistryObject<Block> HOLLOW_STONE = BLOCKS.register("hollow_stone",
            () -> new HollowStoneBlock(AbstractBlock.Properties.of(Material.STONE).randomTicks().strength(1.5F, 6.0F).sound(SoundType.STONE).noOcclusion()));
}


