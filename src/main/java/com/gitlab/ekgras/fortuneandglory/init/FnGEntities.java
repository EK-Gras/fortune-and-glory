package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.*;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FnGEntities {
    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITIES, FortuneAndGlory.MODID);

    public static final RegistryObject<EntityType<WeaponSlashEntity>> WEAPON_SLASH = ENTITY_TYPES.register("weapon_slash",
            () -> EntityType.Builder.<WeaponSlashEntity>of(WeaponSlashEntity::new, EntityClassification.MISC).sized(0.5f,1.5f).build(new ResourceLocation(FortuneAndGlory.MODID, "weapon_slash").toString()));
    public static final RegistryObject<EntityType<FnGFishingBobberEntity>> CUSTOM_BOBBER = ENTITY_TYPES.register("custom_bobber",
            () -> EntityType.Builder.<FnGFishingBobberEntity>createNothing(EntityClassification.MISC).noSave().noSummon().sized(0.25F, 0.25F).clientTrackingRange(4).updateInterval(5).build(new ResourceLocation(FortuneAndGlory.MODID, "custom_bobber").toString()));
    public static final RegistryObject<EntityType<BulletEntity>> BULLET = ENTITY_TYPES.register("bullet",
            () -> EntityType.Builder.<BulletEntity>of(BulletEntity::new, EntityClassification.MISC).sized(0.1f,0.1f).build(new ResourceLocation(FortuneAndGlory.MODID, "bullet").toString()));
    public static final RegistryObject<EntityType<SpearEntity>> SPEAR = ENTITY_TYPES.register("spear",
            () -> EntityType.Builder.<SpearEntity>of(SpearEntity::new, EntityClassification.MISC).sized(0.5f,0.5f).build(new ResourceLocation(FortuneAndGlory.MODID, "spear").toString()));
    public static final RegistryObject<EntityType<BoomerangEntity>> BOOMERANG = ENTITY_TYPES.register("boomerang",
            () -> EntityType.Builder.<BoomerangEntity>of(BoomerangEntity::new, EntityClassification.MISC).sized(0.5f,0.5f).build(new ResourceLocation(FortuneAndGlory.MODID, "boomerang").toString()));
    public static final RegistryObject<EntityType<FnGProjectileItemEntity>> THROWABLE = ENTITY_TYPES.register("throwable",
            () -> EntityType.Builder.<FnGProjectileItemEntity>of(FnGProjectileItemEntity::new, EntityClassification.MISC).sized(0.5f,0.5f).build(new ResourceLocation(FortuneAndGlory.MODID, "throwable").toString()));
    public static final RegistryObject<EntityType<EnergyBallEntity>> ENERGY_BALL = ENTITY_TYPES.register("energy_ball",
            () -> EntityType.Builder.<EnergyBallEntity>of(EnergyBallEntity::new, EntityClassification.MISC).sized(0.25f,0.25f).build(new ResourceLocation(FortuneAndGlory.MODID, "energy_ball").toString()));
    public static final RegistryObject<EntityType<DartEntity>> DART = ENTITY_TYPES.register("dart",
            () -> EntityType.Builder.<DartEntity>of(DartEntity::new, EntityClassification.MISC).sized(0.2f,0.2f).build(new ResourceLocation(FortuneAndGlory.MODID, "dart").toString()));
    public static final RegistryObject<EntityType<BowArrowEntity>> FNG_ARROW = ENTITY_TYPES.register("fng_arrow",
            () -> EntityType.Builder.<BowArrowEntity>of(BowArrowEntity::new, EntityClassification.MISC).sized(0.5f,0.5f).build(new ResourceLocation(FortuneAndGlory.MODID, "fng_arrow").toString()));
    public static final RegistryObject<EntityType<MinionEntity>> MINION = ENTITY_TYPES.register("minion",
            () -> EntityType.Builder.<MinionEntity>of(MinionEntity::new, EntityClassification.MONSTER).sized(0.6F, 1.5F).clientTrackingRange(8).build(new ResourceLocation(FortuneAndGlory.MODID, "minion").toString()));
    public static final RegistryObject<EntityType<StaffAOEEntity>> STAFF_AOE_EFFECT = ENTITY_TYPES.register("staff_effect",
            () -> EntityType.Builder.<StaffAOEEntity>of(StaffAOEEntity::new, EntityClassification.MISC).build(new ResourceLocation(FortuneAndGlory.MODID, "staff_effect").toString()));
}
