package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import java.util.function.Supplier;

@MethodsReturnNonnullByDefault
public class FnGItemGroups {

    public static class ModItemGroup extends ItemGroup {

        private final Supplier<ItemStack> iconSupplier;

        public ModItemGroup(final String name, final Supplier<ItemStack> iconSupplier) {
            super(name);
            this.iconSupplier = iconSupplier;
        }

        @Override
        public ItemStack makeIcon() {
            return iconSupplier.get();
        }

    }

    public static final ItemGroup MAIN_ITEM_GROUP = new ModItemGroup(FortuneAndGlory.MODID,
            () -> new ItemStack(FnGItems.IRON_DAGGER.get()));

}
