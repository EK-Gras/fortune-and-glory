package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.enchants.*;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.potion.Effect;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class FnGEnchantments {

    public static final DeferredRegister<Enchantment> ENCHANTMENTS =
        DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, FortuneAndGlory.MODID);

    public static final RegistryObject<Enchantment> ICE_ASPECT =
        ENCHANTMENTS.register("ice_aspect", IceAspectEnchantment::new);

    public static final RegistryObject<Enchantment> CRYOSHOT =
        ENCHANTMENTS.register("cryoshot", CryoshotEnchantment::new);

    public static final RegistryObject<Enchantment> FROST_SHELL =
        ENCHANTMENTS.register("frost_shell", FrostShellEnchantment::new);

    public static final RegistryObject<Enchantment> STONEWALKER =
        ENCHANTMENTS.register("stone_walker", StoneWalkerEnchantment::new);

    public static final RegistryObject<Enchantment> ELECTRIC_ASPECT =
        ENCHANTMENTS.register("electric_aspect", ElectricAspectEnchantment::new);

    public static final RegistryObject<Enchantment> ELECTROSHOT =
        ENCHANTMENTS.register("electroshot", ElectroshotEnchantment::new);

    public static final RegistryObject<Enchantment> GROWTH =
        ENCHANTMENTS.register("growth", GrowthEnchantment::new);

    public static final RegistryObject<Enchantment> DRAINING_ASPECT =
        ENCHANTMENTS.register("draining_aspect", DrainingAspectEnchantment::new);

    public static final RegistryObject<Enchantment> DRAINSHOT =
        ENCHANTMENTS.register("drainshot", DrainshotEnchantment::new);

    public static final RegistryObject<Enchantment> FIRESTEP =
        ENCHANTMENTS.register("firestep", FirestepEnchantment::new);

    public static final RegistryObject<Enchantment> QUAKESHOT =
        ENCHANTMENTS.register("quakeshot", QuakeshotEnchantment::new);

    public static final List<Enchantment> MELEE_EFFECT_ENCHANTMENTS = new ArrayList<>();

    public static final List<Enchantment> PROJECTILE_EFFECT_ENCHANTMENTS = new ArrayList<>();

    public static final List<Enchantment> USER_EFFECT_ENCHANTMENTS = new ArrayList<>();

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    private static class RegisterEffectEnchantments {
        @SubscribeEvent
        public static void onRegisterEffects(RegistryEvent.Register<Enchantment> event) {
            ForgeRegistries.POTIONS.forEach(effect -> {
                USER_EFFECT_ENCHANTMENTS.add(
                    new UserEffectEnchantment(effect)
                        .setRegistryName(getEnchantId("user", effect)));
                PROJECTILE_EFFECT_ENCHANTMENTS.add(
                    new ProjectileEffectEnchantment(effect, 100)
                        .setRegistryName(getEnchantId("projectile", effect)));
                MELEE_EFFECT_ENCHANTMENTS.add(
                    new MeleeEffectEnchantment(effect, 100)
                        .setRegistryName(getEnchantId("melee", effect)));
            });

            USER_EFFECT_ENCHANTMENTS.forEach(event.getRegistry()::register);
            PROJECTILE_EFFECT_ENCHANTMENTS.forEach(event.getRegistry()::register);
            MELEE_EFFECT_ENCHANTMENTS.forEach(event.getRegistry()::register);
        }

        private static ResourceLocation getEnchantId(String prefix, Effect effect) {
            return new ResourceLocation(
                FortuneAndGlory.MODID,
                prefix + "_"
                    + effect.getRegistryName().getNamespace() + "_"
                    + effect.getRegistryName().getPath()
            );
        }
    }
}
