package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.abilities.*;
import com.gitlab.ekgras.fortuneandglory.enums.BoomerangHitResult;
import com.gitlab.ekgras.fortuneandglory.enums.UsageType;
import com.gitlab.ekgras.fortuneandglory.items.*;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.google.common.collect.ImmutableList;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import org.lwjgl.system.CallbackI;

import java.util.function.Supplier;

import static com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments.CRYOSHOT;
import static net.minecraft.enchantment.Enchantments.FIRE_ASPECT;
import static net.minecraft.enchantment.Enchantments.QUICK_CHARGE;
import static net.minecraft.item.ItemTier.IRON;

public class FnGItems {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, FortuneAndGlory.MODID);

    public static final RegistryObject<Item> IRON_DAGGER = ITEMS.register("iron_dagger",
            () -> new LightMeleeItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 1, 16F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_HAMMER = ITEMS.register("iron_hammer",
            () -> new MeleeItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 7, -3F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_GAUNTLET = ITEMS.register("iron_gauntlet",
            () -> new GauntletItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 0, 16F, 3, 750, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SCYTHE = ITEMS.register("iron_scythe",
            () -> new ComboItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 2, -2.4F, 3, 1000, 2,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_BATTLE_AXE = ITEMS.register("iron_battle_axe",
            () -> new ComboItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 6, -3F, 3, 1500, 3,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_RAPIER = ITEMS.register("iron_rapier",
            () -> new RapierItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 0, 16F, 8, new ZipAbility(2, SoundEvents.TRIDENT_RIPTIDE_1, 1, 1), SoundEvents.EXPERIENCE_ORB_PICKUP, 1, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SABER = ITEMS.register("iron_saber",
            () -> new ChargingItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 2, -2.4F, 5, new WeaponSlashAbility(6, 60, 1, SoundEvents.PLAYER_ATTACK_SWEEP, 1, 0.75f), SoundEvents.EXPERIENCE_ORB_PICKUP, 1, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_GREATSWORD = ITEMS.register("iron_greatsword",
            () -> new ChargingItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 6, -3F, 3, new ApplyEffectAbility(Effects.MOVEMENT_SPEED, 100, 4, true, SoundEvents.WITHER_BREAK_BLOCK, 0.25F, 1.4F), SoundEvents.EXPERIENCE_ORB_PICKUP, 1, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_LONGBOW = ITEMS.register("iron_longbow",
            () -> new LongBowItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 1.5,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_REPEATER = ITEMS.register("iron_repeater",
            () -> new RepeaterBowItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 1.2, 2, 5, 20, 10,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_PISTOL = ITEMS.register("iron_pistol",
            () -> new GunItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, 5, 3,20, 1, 1, 3, 2, 60, false, 0.2f, 0.2f, 0.2f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SHOTGUN = ITEMS.register("iron_shotgun",
            () -> new GunItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, 5, 20, 40, 10, 10, 1,8, 30, false, 0.2f, 0.2f, 0.2f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_RIFLE = ITEMS.register("iron_rifle",
            () -> new GunItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 4, 4, 20, 40, 0, 1, 8,8, 100, false, 0.2f, 0.2f, 0.2f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_MACHINE_GUN = ITEMS.register("iron_machine_gun",
            () -> new GunItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, 15, 2, 30, 2, 1, 1,1, 60, true, 0.2f, 0.2f, 0.2f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SPEAR = ITEMS.register("iron_spear",
            () -> new SpearItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 8, 8, -2.9f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_BOOMERANG = ITEMS.register("iron_boomerang",
            () -> new BoomerangItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 10, 10, 2, 8, 20, BoomerangHitResult.TELEPORT,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_CANNON = ITEMS.register("iron_cannon",
            () -> new CannonItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), (stack) -> {return stack.getItem() == Items.COBBLESTONE;}, 20, 8, 1.0F, 1.5F, 2.0F, SoundEvents.GENERIC_EXPLODE, 1, 1.5F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_WAND = ITEMS.register("iron_wand",
            () -> new WandItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 4, 5f, 0.25f, 20, 4, 1, 1, false, false, 200, ParticleTypes.END_ROD, false,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SCROLL = ITEMS.register("iron_scroll",
            () -> new ScrollItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 20, 20f, new WeaponSlashAbility(5, 40, 1, SoundEvents.PLAYER_ATTACK_SWEEP, 1, 0.75f),
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_DART = ITEMS.register("iron_dart",
            () -> new DartItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 20, 25f, 0.2f, 140, 2, 1, Effects.POISON, 0, 140,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_FLAMETHROWER = ITEMS.register("iron_flamethrower",
            () -> new FlamethrowerItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2f, 0.3f, 3, 1, 10, 0.3f, 2,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SPELLBOOK = ITEMS.register("iron_spellbook",
            () -> new SpellbookItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 4, 20f, 0.15f, 20, 12, 0.25f, 1.5f, true, true, 200, ParticleTypes.END_ROD, false,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_STAFF = ITEMS.register("iron_staff",
            () -> new StaffItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 60, 50f, 0.1f, 60, SoundEvents.WITHER_BREAK_BLOCK, 0.5f, 1,false, new BlankAbility(), 8, 3, 3,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SCEPTER = ITEMS.register("iron_scepter",
            () -> new ScepterItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 20, 33.3f, 0.2f, 40, SoundEvents.ZOMBIE_INFECT, 1, 1, 3, 2400,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SLEDGEHAMMER = ITEMS.register("iron_sledgehammer",
            () -> new SledgehammerItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 6, -2.8F, 6.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_LUMBERAXE = ITEMS.register("iron_lumberaxe",
            () -> new LumberaxeItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 6, -2.8F, 6.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_EXCAVATOR = ITEMS.register("iron_excavator",
            () -> new ExcavatorItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP),IRON, 3, -2.4F, 6.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SICKLE = ITEMS.register("iron_sickle",
            () -> new SickleItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP),IRON, 1, -1.6F, 6.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_PAXEL = ITEMS.register("iron_paxel",
            () -> new PaxelItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 3, -2.4F, 6.0F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_SPADE = ITEMS.register("iron_spade",
            () -> new SpadeItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 3, -2.0F, 6.0F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_PICKSAW = ITEMS.register("iron_picksaw",
            () -> new PicksawItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 3, -2.8F, 6.0F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_MATTOCK = ITEMS.register("iron_mattock",
            () -> new MattockItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 3, -2.4F, 6.0F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_COIN = ITEMS.register("iron_coin",
            () -> new CoinItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(384), Attributes.ARMOR, 2,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> IRON_BADGE = ITEMS.register("iron_badge",
            () -> new BadgeItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(384), 1, Attributes.MAX_HEALTH, 1, UsageType.TICK));
    public static final RegistryObject<Item> DIAMOND_PAXEL = ITEMS.register("diamond_paxel",
            () -> new PaxelItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 3, -2.4F,  6.0F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> MANTLE_BREAKER = ITEMS.register("mantle_breaker",
            () -> new FnGPickaxeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 1, -2.8F, 24.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> NETHERENDING_FURY = ITEMS.register("netherending_fury",
            () -> new FnGAxeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 11, -3.0F, 30.0f,
                    ImmutableList.of(new LeveledEnchantment(FIRE_ASPECT, 2)), ImmutableList.of()));
    public static final RegistryObject<Item> DUNE_DUSTER = ITEMS.register("dune_duster",
            () -> new FnGShovelItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 1, -2.8F, 24.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> SEADLAYER = ITEMS.register("seadlayer",
            () -> new FnGHoeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 1, -2.8F, 24.0f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> ENDER_OF_WORLDS = ITEMS.register("ender_of_worlds",
            () -> new MeleeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), ItemTier.DIAMOND, 6, -2.4F,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> DUALITY = ITEMS.register("duality",
            () -> new FnGBowItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(1200), 2.0,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> MACHINE_CROSSBOW = ITEMS.register("machine_crossbow",
            () -> new FnGCrossbowItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(1200), 2.0,
                    ImmutableList.of(new LeveledEnchantment(QUICK_CHARGE, 5)), ImmutableList.of()));
    public static final RegistryObject<Item> CHAMPIONS_FISHING_ROD = ITEMS.register("champions_fishing_rod",
            () -> new FnGFishingRodItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(1200), 0.4f, 2f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> VAULT_HELMET = ITEMS.register("vault_helmet",
            () -> new FnGArmorItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), FnGArmorMaterial.VAULT, EquipmentSlotType.HEAD,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> VAULT_CHESTPLATE = ITEMS.register("vault_chestplate",
            () -> new FnGArmorItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), FnGArmorMaterial.VAULT, EquipmentSlotType.CHEST,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> VAULT_LEGGINGS = ITEMS.register("vault_leggings",
            () -> new FnGArmorItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), FnGArmorMaterial.VAULT, EquipmentSlotType.LEGS,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> VAULT_BOOTS = ITEMS.register("vault_boots",
            () -> new FnGArmorItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), FnGArmorMaterial.VAULT, EquipmentSlotType.FEET,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> ADMIN_REPEATER = ITEMS.register("admin_repeater",
            () -> new RepeaterBowItem((new Item.Properties()).durability(999999).tab(FnGItemGroups.MAIN_ITEM_GROUP),2, 10, 2, 0, 2,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> ORB_SHOTGUN = ITEMS.register("orb_shotgun",
            () -> new GunItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, 10, 5, 10, 180, 99, 1, 10, 30, false, 0.2f, 0.2f, 1f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> INFIROCKET = ITEMS.register("infirocket",
            () -> new ElytraRocketItem((new Item.Properties()).stacksTo(1).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> RUBY = ITEMS.register("ruby",
            () -> new Item((new Item.Properties()).stacksTo(64).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> BIG_RUBY = ITEMS.register("big_ruby",
            () -> new Item((new Item.Properties()).stacksTo(64).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> RUBY_BAG = ITEMS.register("ruby_bag",
            () -> new Item((new Item.Properties()).stacksTo(64).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> BULLET = ITEMS.register("bullet",
            () -> new BulletItem((new Item.Properties()).stacksTo(64).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> BALL = ITEMS.register("ball",
            () -> new ThrowableItem((new Item.Properties()).stacksTo(64).tab(FnGItemGroups.MAIN_ITEM_GROUP), 4, 5,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> DODGE_BADGE = ITEMS.register("dodge_badge",
            () -> new AbilityBadgeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(600), 2, UsageType.USE, new DodgeAbility(1, 0.5f, 10), 60));
    public static final RegistryObject<Item> POTION_BADGE = ITEMS.register("potion_badge",
            () -> new AbilityBadgeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(600), 2, UsageType.USE, new ApplyEffectAbility(Effects.REGENERATION, 200, 0, true, SoundEvents.UI_BUTTON_CLICK, 0, 0), 100));
    public static final RegistryObject<Item> SLASH_BADGE = ITEMS.register("slash_badge",
            () -> new AbilityBadgeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(600), 2, UsageType.USE, new WeaponSlashAbility(6, 100,  1, SoundEvents.PLAYER_ATTACK_SWEEP, 1, 0.5f), 40));
    public static final RegistryObject<Item> VAULT_WRENCH = ITEMS.register("vault_wrench",
            () -> new Item((new Item.Properties()).stacksTo(1).tab(FnGItemGroups.MAIN_ITEM_GROUP)));
    public static final RegistryObject<Item> RAINBOW_SABER = ITEMS.register("rainbow_saber",
            () -> new ChargingItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 2, -2.4F,  1, new WeaponSlashAbility(6, 60, 1, SoundEvents.PLAYER_ATTACK_SWEEP, 1, 0.75f), SoundEvents.EXPERIENCE_ORB_PICKUP, 1, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> RAINBOW_RAPIER = ITEMS.register("rainbow_rapier",
            () -> new ChargingItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP), IRON, 0, 16F,  1, new DashAbility(1.0d, SoundEvents.SNOWBALL_THROW, 1.0f, 1.0f, true), SoundEvents.EXPERIENCE_ORB_PICKUP, 1, 1,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> RAINBOW_MACHINE_GUN = ITEMS.register("rainbow_machine_gun",
            () -> new GunItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, 20, 1, 1, 0, 1, 3,0, 60, true, 0.01f, 0.01f, 0.01f,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> RAINBOW_BADGE = ITEMS.register("rainbow_badge",
            () -> new BadgeItem((new Item.Properties()).durability(384).tab(FnGItemGroups.MAIN_ITEM_GROUP), 2, UsageType.TICK));
    public static final RegistryObject<Item> ADMIN_DODGE_BADGE = ITEMS.register("admin_dodge_badge",
            () -> new AbilityBadgeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(600), 1, UsageType.USE, new DodgeAbility(1, 0.5f, 10)));
    public static final RegistryObject<Item> ADMIN_ZIP_BADGE = ITEMS.register("admin_zip_badge",
            () -> new AbilityBadgeItem((new Item.Properties()).tab(FnGItemGroups.MAIN_ITEM_GROUP).durability(600), 1, UsageType.USE, new ZipAbility(1, SoundEvents.TRIDENT_RIPTIDE_1, 1, 1)));
    public static final RegistryObject<Item> ABILITY_TESTER = ITEMS.register("ability_tester",
            () -> new AbilityTesterItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).stacksTo(1), new DodgeAbility(2, 1,20)));
    public static final RegistryObject<Item> ZIP_ABILITY_TESTER = ITEMS.register("zip_ability_tester",
            () -> new AbilityTesterItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).stacksTo(1), new ZipAbility(2, SoundEvents.TRIDENT_RIPTIDE_1, 1, 1)));
    public static final RegistryObject<Item> DISRUPT_TESTER = ITEMS.register("disrupt_tester",
            () -> new BlockDisruptTesterItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).stacksTo(1)));

    // ICE SERIES
    public static final RegistryObject<Item> COCYTUS = ITEMS.register("cocytus",
            () -> new FnGBowItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).stacksTo(1).durability(3000), 5,
                    ImmutableList.of(), ImmutableList.of()));
    public static final RegistryObject<Item> BLIZZARD_CUTTER = ITEMS.register("blizzard_cutter",
            () -> new BoomerangItem(new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP).stacksTo(1).durability(3000), 5, 5, 3, 16, 20, BoomerangHitResult.TELEPORT,
                    ImmutableList.of(new LeveledEnchantment(CRYOSHOT, 2), new LeveledEnchantment(Enchantments.UNBREAKING, 1)), ImmutableList.of()));

}
