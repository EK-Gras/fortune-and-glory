package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class EntityAttributeCreationHandler{

    @SubscribeEvent
    public static void onEntityAttributeCreation (EntityAttributeCreationEvent event) {
        event.put(FnGEntities.MINION.get(), MinionEntity.createAttributes().build());
    }
}
