package com.gitlab.ekgras.fortuneandglory.init;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.particles.IParticleData;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.PotionEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class FnGEffects {

    public static final DeferredRegister<Effect> EFFECTS = DeferredRegister.create(ForgeRegistries.POTIONS, FortuneAndGlory.MODID);

    public static final RegistryObject<Effect> FREEZE =
        EFFECTS.register("freeze", () -> new EffectAccess(EffectType.HARMFUL, 0)
            .addAttributeModifier(Attributes.MOVEMENT_SPEED, "62AA65BC-ABE0-41F8-B22B-1F04A22BA3CB", -1, AttributeModifier.Operation.MULTIPLY_TOTAL));

    public static final RegistryObject<Effect> SHOCK =
        EFFECTS.register("shock", () -> new EffectAccess(EffectType.HARMFUL, 0));

    public static final RegistryObject<Effect> SUBMERGED =
        EFFECTS.register("submerged", () -> new EffectAccess(EffectType.HARMFUL, 0));

    public static final RegistryObject<Effect> FREEZE_RESISTANCE =
        EFFECTS.register("freeze_resistance", () -> new EffectAccess(EffectType.BENEFICIAL, 0));

    public static final RegistryObject<Effect> SHOCK_RESISTANCE =
        EFFECTS.register("shock_resistance", () -> new EffectAccess(EffectType.BENEFICIAL, 0));


    @SubscribeEvent
    public static void onAttack(LivingAttackEvent event) {
        Entity entity = event.getSource().getEntity();
        // Cancel the attacks of entities with Shock
        if (entity instanceof LivingEntity && ((LivingEntity) entity).hasEffect(SHOCK.get())) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void onUse(PlayerInteractEvent event) {
        LivingEntity entity = event.getEntityLiving();
        Item item = event.getItemStack().getItem();
        if (entity.hasEffect(SHOCK.get()) && event.isCancelable()
                && !(item.isEdible()) && !(item instanceof BlockItem)
                && !(event instanceof PlayerInteractEvent.LeftClickBlock)
            ) {
            event.setCanceled(true);
        }
    }

    private static BiMap<Effect, Effect> resistances;

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    private static class RegisterResistances {
        @SubscribeEvent
        public static void onRegistered(RegistryEvent.Register<Effect> event) {
            resistances = ImmutableBiMap.of(
                FREEZE.get(), FREEZE_RESISTANCE.get(),
                SHOCK.get(), SHOCK_RESISTANCE.get()
            );
        }
    }



    /**
     * Ensure that an effect is not applicable when it's resistance is applied
     *
     * e.g., freeze cannot be added to entity with freeze resistance
     */
    @SubscribeEvent
    public static void isEffectApplicable(PotionEvent.PotionApplicableEvent event) {
        Effect effect = event.getPotionEffect().getEffect();
        Effect resistanceEffect = resistances.get(effect);
        LivingEntity entity = event.getEntityLiving();
        if (entity.hasEffect(resistanceEffect)) {
            event.setResult(Event.Result.DENY);
        }
    }

    /**
     * Remove an effect when it's resistance is applied
     *
     * e.g., remove freeze when freeze resistance is applied
     *
     * Additionally, remove an effect when another elemental effect is applied
     */
    @SubscribeEvent
    public static void onEffectApplied(PotionEvent.PotionAddedEvent event) {
        Effect resistedEffect = resistances.inverse().get(event.getPotionEffect().getEffect());
        LivingEntity entity = event.getEntityLiving();
        if (entity.hasEffect(resistedEffect)) {
            entity.removeEffect(resistedEffect);
        }
        if ((event.getPotionEffect().getEffect() == FnGEffects.SHOCK.get())) {
            entity.setSecondsOnFire(0);
            entity.removeEffect(FnGEffects.FREEZE.get());
        }
        if ((event.getPotionEffect().getEffect() == FnGEffects.FREEZE.get())) {
            entity.setSecondsOnFire(0);
            entity.removeEffect(FnGEffects.SHOCK.get());
        }
    }

    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
        PlayerEntity player = event.player;
        if (player.hasEffect(FnGEffects.FREEZE.get()) || player.hasEffect(FnGEffects.SHOCK.get())) {
            player.setSecondsOnFire(0);
        }
    }

    @SubscribeEvent
    public static void onWorldTick(TickEvent.WorldTickEvent event) {
        ServerWorld world = (ServerWorld) event.world;
        for (Entity entity : world.getAllEntities()) {
            if (entity instanceof LivingEntity) {
                LivingEntity livingEntity = (LivingEntity) entity;
                if (livingEntity.hasEffect(FnGEffects.FREEZE.get())) {
                    if (livingEntity.getEffect(FnGEffects.FREEZE.get()).isVisible())
                        world.sendParticles((IParticleData) FnGParticles.FROST.get(), entity.getRandomX(0.5D), entity.getRandomY(), entity.getRandomZ(0.5D), 1, 0, 0, 0, 1);

                }
                if (livingEntity.hasEffect(FnGEffects.SHOCK.get())) {
                    if (livingEntity.getEffect(FnGEffects.SHOCK.get()).isVisible())
                        world.sendParticles((IParticleData) FnGParticles.SPARK.get(), entity.getX(), entity.getRandomY(), entity.getZ(), 1, 0, 0, 0, 1);
                }
            }
        }
    }

    private static class EffectAccess extends Effect {

        public EffectAccess(EffectType type, int color) {
            super(type, color);
        }
    }

}
