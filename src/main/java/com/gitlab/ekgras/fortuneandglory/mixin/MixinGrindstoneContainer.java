package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.inventory.container.GrindstoneContainer;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Mixin(GrindstoneContainer.class)
public class MixinGrindstoneContainer {

    @Redirect(
        method = "removeNonCurses",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/enchantment/EnchantmentHelper;setEnchantments(Ljava/util/Map;Lnet/minecraft/item/ItemStack;)V"
        )
    )
    public void onSetEnchantments(
        Map<Enchantment, Integer> enchants, ItemStack newStack, // setEnchantments arguments
        ItemStack originalStack, int p_217007_2_, int p_217007_3_ // removeNonCurses arguments
    ) {
        Map<Enchantment, Integer> enchantData = IDefaultEnchantmentData.defaultsForItem(originalStack.getItem());

        // ignore passed in enchants; it contains only curses (vanilla behaviour)
        // get all enchantments on the item
        enchants = EnchantmentHelper.getEnchantments(originalStack);
        // Instantiate a new map with the same set of enchantments as enchants, then remove all the enchantments we DON'T want removed
        Map<Enchantment, Integer> enchantsToRemove = new HashMap<>(enchants);
        enchantsToRemove.keySet().removeAll(enchantData.keySet());
        for (Enchantment enchant : enchants.keySet()) {
            if (enchant.isCurse())
                enchantsToRemove.remove(enchant);
        }
        // Remove all the enchantments that are not default enchantments
        enchants.keySet().removeAll(enchantsToRemove.keySet());

        EnchantmentHelper.setEnchantments(enchants, newStack);
    }

}
