package com.gitlab.ekgras.fortuneandglory.mixin.accessors;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.List;
import java.util.Random;

@Mixin(CrossbowItem.class)
public interface CrossbowItemAccessor {

    @Invoker("getChargedProjectiles")
    static List<ItemStack> getChargedProjectiles(ItemStack crossbowStack) {
        throw new AssertionError();
    }

    @Invoker("getShotPitches")
    static float[] getShotPitches(Random random) {
        throw new AssertionError();
    }

    @Invoker("onCrossbowShot")
    static void onCrossbowShot(World world, LivingEntity entity, ItemStack crossbowStack) {
        throw new AssertionError();
    }

    @Invoker("getArrow")
    static AbstractArrowEntity getArrow(World world, LivingEntity entity, ItemStack crossbowStack, ItemStack ammoStack) {
        throw  new AssertionError();
    }

    @Invoker("addChargedProjectile")
    static void addChargedProjectile(ItemStack crossbowStack, ItemStack ammoStack) {
        throw new AssertionError();
    }

    @Invoker("getPowerForTime")
    static float getPowerForTime(int useDuration, ItemStack crossbowStack) {
        throw new AssertionError();
    }

}
