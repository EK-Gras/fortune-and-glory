package com.gitlab.ekgras.fortuneandglory.mixin.accessors;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.List;

@Mixin(AbstractArrowEntity.class)
public interface ArrowEntityAccessor {

    @Accessor
    IntOpenHashSet getPiercingIgnoreEntityIds();

    @Accessor("piercingIgnoreEntityIds")
    void setPiercingIgnoreEntityIds(IntOpenHashSet set);

    @Accessor
    List<Entity> getPiercedAndKilledEntities();

    @Accessor("piercedAndKilledEntities")
    void setPiercedAndKilledEntities(List<Entity> list);

    @Accessor
    int getKnockback();
}
