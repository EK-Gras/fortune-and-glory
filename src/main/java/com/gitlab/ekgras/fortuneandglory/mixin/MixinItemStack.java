package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.IItemProvider;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

@Mixin(ItemStack.class)
public class MixinItemStack {

    @Inject(
        method = "<init>(Lnet/minecraft/nbt/CompoundNBT;)V",
        at = @At(
            value = "RETURN"
        )
    )
    public void onItemCreatedNBT(CompoundNBT nbt, CallbackInfo info) {
        ItemStack stack = ItemStack.class.cast(this);
        // Ensures that enchantments are not applied multiple times
        if (EnchantmentHelper.getEnchantments(stack).isEmpty()) {
            for (Map.Entry<Enchantment, Integer> enchant : IDefaultEnchantmentData.defaultsForItem(stack.getItem()).entrySet()) {
                stack.enchant(enchant.getKey(), enchant.getValue());
            }
        }
    }

    @Inject(
            method = "<init>(Lnet/minecraft/util/IItemProvider;ILnet/minecraft/nbt/CompoundNBT;)V",
            at = @At(
                    value = "RETURN"
            )
    )
    public void onItemCreatedFull(IItemProvider provider, int count, CompoundNBT nbt, CallbackInfo info) {
        ItemStack stack = ItemStack.class.cast(this);
        if (EnchantmentHelper.getEnchantments(stack).isEmpty()) {
            for (Map.Entry<Enchantment, Integer> enchant : IDefaultEnchantmentData.defaultsForItem(stack.getItem()).entrySet()) {
                stack.enchant(enchant.getKey(), enchant.getValue());
            }
        }
    }
}
