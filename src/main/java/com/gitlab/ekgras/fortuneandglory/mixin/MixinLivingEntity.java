package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.enchants.StoneWalkerEnchantment;
import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import com.gitlab.ekgras.fortuneandglory.network.FnGPacketHandler;
import com.gitlab.ekgras.fortuneandglory.network.packet.UpdateElementalPercentagesPacket;
import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.enchantment.FrostWalkerEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.tags.ITag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
public abstract class MixinLivingEntity extends Entity implements ElementalPercentages {
    float freezePercentage = 0;
    float shockPercentage = 0;

    public MixinLivingEntity(EntityType<?> pType, World pLevel) {
        super(pType, pLevel);
    }

    @Redirect(
        method = "baseTick",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/LivingEntity;isEyeInFluid(Lnet/minecraft/tags/ITag;)Z"
        )
    )
    public boolean onCheckEyeInFluid(LivingEntity instance, ITag<Fluid> tag) {
        if (instance.hasEffect(FnGEffects.SUBMERGED.get())) {
            return true;
        }

        return instance.isEyeInFluid(tag);
    }

    // todo: julian fix it
    @Redirect(
        method = "jumpFromGround",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/LivingEntity;setDeltaMovement(DDD)V"
        )
    )
    public void onJumpUp(LivingEntity instance, double x, double y, double z) {
        if (instance.hasEffect(FnGEffects.FREEZE.get())) {
            instance.setDeltaMovement(0, 0, 0);
            return;
        }

        instance.setDeltaMovement(x, y, z);
    }

    @Redirect(
        method = "jumpFromGround",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/LivingEntity;setDeltaMovement(Lnet/minecraft/util/math/vector/Vector3d;)V"
        )
    )
    public void onJumpUp2(LivingEntity instance, Vector3d vector3d) {
        if (instance.hasEffect(FnGEffects.FREEZE.get())) {
            return;
        }

        instance.setDeltaMovement(vector3d);
    }

    @Inject(
        method = "onChangedBlock",
        at = @At(
            value = "HEAD"
        )
    )
    public void onChangedBlock(BlockPos pPos, CallbackInfo info) {
        LivingEntity entity = LivingEntity.class.cast(this);
        int i = EnchantmentHelper.getEnchantmentLevel(FnGEnchantments.STONEWALKER.get(), entity);
        if (i > 0) {
            StoneWalkerEnchantment.onEntityMoved(entity, this.level, pPos, i);
        }
    }

    @Inject(
            method = "tick",
            at = @At(
                    value = "HEAD"
            )
    )
    public void onTick(CallbackInfo info) {
        if (this.getFreezePercentage() > 0)
            this.addFreezePercentage(-0.5f);
        if (this.getShockPercentage() > 0)
            this.addShockPercentage(-0.5f);
    }

    // ELEMENTAL PERCENTAGES !! this code runs on server !!

    @Override
    public float getFreezePercentage() {
        return this.freezePercentage;
    }

    @Override
    public float getRenderFreezePercentage() {
        LivingEntity castedEntity = ((LivingEntity) this.getEntity());
//        if (this.level.isClientSide) {
//            FortuneAndGlory.LOGGER.info("\nENTITY CHECK! " + castedEntity + "\nHAS FREEZE? " + castedEntity.hasEffect(FnGEffects.FREEZE.get()));
//        }
        return ((LivingEntity) this.getEntity()).hasEffect(FnGEffects.FREEZE.get()) ? 100f : this.getFreezePercentage();
    }

    @Override
    public void setFreezePercentage(float percentage) {
        this.freezePercentage = percentage;
        if (freezePercentage < 0) freezePercentage = 0;
        FortuneAndGlory.LOGGER.info("!SET " + (this.level.isClientSide ? "CLIENT" : "SERVER") + "side freeze percentage: " + this.getFreezePercentage());
        this.synchronize();
    }

    @Override
    public void addFreezePercentage(float percentage) {
        this.setFreezePercentage(this.getFreezePercentage() + percentage);
    }

    @Override
    public float getShockPercentage() {
        return this.shockPercentage;
    }

    @Override
    public float getRenderShockPercentage() {
        return ((LivingEntity) this.getEntity()).hasEffect(FnGEffects.SHOCK.get()) ? 100f : this.getShockPercentage();
    }

    @Override
    public void setShockPercentage(float percentage) {
        this.shockPercentage = percentage;
        if (shockPercentage < 0) shockPercentage = 0;
        this.synchronize();
    }

    @Override
    public void addShockPercentage(float percentage) {
        this.setShockPercentage(this.getShockPercentage() + percentage);
    }

    @Override
    public void synchronize() {
        // this code can only run on server, so do NOT send when on client
        if (this.level.isClientSide) return;

        // server sends back updated percentages
        FnGPacketHandler.sendToAllTrackingEntity(new UpdateElementalPercentagesPacket(this.getId(), this.freezePercentage, this.shockPercentage), () -> this);
    }
}