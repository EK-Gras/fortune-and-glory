package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.items.GunItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerController;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(Minecraft.class)
public class MixinMinecraft {

    @Redirect(
        method = "handleKeybinds",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/multiplayer/PlayerController;releaseUsingItem(Lnet/minecraft/entity/player/PlayerEntity;)V"
        )
    )
    public void releaseUsingItem(PlayerController instance, PlayerEntity player) {
        Minecraft self = Minecraft.class.cast(this);
        if (player.getItemInHand(Hand.MAIN_HAND).getItem() instanceof GunItem && player.getUseItem().getItem() instanceof GunItem && self.options.keyAttack.isDown()) {
            return;
        }
        else if (player.getItemInHand(Hand.MAIN_HAND).getItem() instanceof GunItem && player.getUseItem().getItem() instanceof GunItem) {
            instance.releaseUsingItem(player);
        }
        instance.releaseUsingItem(player);
    }

}
