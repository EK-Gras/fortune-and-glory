package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingRenderer.class)
public abstract class MixinLivingRenderer<T extends LivingEntity, M extends EntityModel<T>> {

    @Inject(method = "getWhiteOverlayProgress", at = @At("HEAD"), cancellable = true)
    protected void onGetWhiteOverlayProgress(T pLivingEntity, float pPartialTicks, CallbackInfoReturnable<Float> cir) {
        float freezePercentage = ((ElementalPercentages) pLivingEntity).getRenderFreezePercentage();
        if (freezePercentage > 0)
            cir.setReturnValue(freezePercentage / 200f);
    }

    @Redirect(
            method = "render*",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/client/renderer/entity/model/EntityModel;renderToBuffer(Lcom/mojang/blaze3d/matrix/MatrixStack;Lcom/mojang/blaze3d/vertex/IVertexBuilder;IIFFFF)V"
            )
    )
    public void onRenderToBuffer(
            EntityModel model,
            MatrixStack pMatrixStack, IVertexBuilder pBuffer, int pPackedLight, int pPackedOverlay, float pRed, float pGreen, float pBlue, float pAlpha,
            T pEntity, float pEntityYaw, float pPartialTicks, MatrixStack bMatrixStack, IRenderTypeBuffer bBuffer, int bPackedLight
    ) {
        pRed *= 1.0f - (((ElementalPercentages) pEntity).getRenderFreezePercentage() / 50f);

        model.renderToBuffer(pMatrixStack, pBuffer, pPackedLight, pPackedOverlay, pRed, pGreen, pBlue, pAlpha);
    }
}