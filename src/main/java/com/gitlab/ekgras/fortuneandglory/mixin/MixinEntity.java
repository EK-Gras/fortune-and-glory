package com.gitlab.ekgras.fortuneandglory.mixin;

import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Entity.class)
public class MixinEntity {
    @Inject(
        method = "setRemainingFireTicks",
        at = @At("HEAD"),
        cancellable = true
    )
    public void noFireIfFrozen(int pTicks, CallbackInfo ci) {
        Entity entity = Entity.class.cast(this);
        if (entity instanceof LivingEntity && ((LivingEntity) entity).hasEffect(FnGEffects.FREEZE.get())) {
            ci.cancel();
        }
    }
}
