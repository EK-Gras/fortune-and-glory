package com.gitlab.ekgras.fortuneandglory;

import com.gitlab.ekgras.fortuneandglory.init.FnGItemGroups;
import net.minecraft.block.Block;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent; 
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Objects;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class FnGEventSubscriber {

    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {
        // get all blocks
        for (Block block : ForgeRegistries.BLOCKS.getValues()) {
            // get name
            ResourceLocation resourceLocation = Objects.requireNonNull(block.getRegistryName());
            // if its a block from our mod
            if (resourceLocation.getNamespace().equals(FortuneAndGlory.MODID)) {
                // make a block item!
                final Item.Properties properties = new Item.Properties().tab(FnGItemGroups.MAIN_ITEM_GROUP);
                final BlockItem blockItem = new BlockItem(block, properties);
                blockItem.setRegistryName(resourceLocation);
                event.getRegistry().register(blockItem);
            }
        }
    }
}


