package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.*;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.ToolType;


import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Extension of a modifiable tool that also is capable of harvesting blocks
 */
public class AOEToolItem extends ToolItem implements IToolEffectivenessSets, IDefaultEnchantmentData {

    private final float speed;
    private static final Set<Block> DIGGABLES = IToolEffectivenessSets.PICKAXE;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public AOEToolItem(Item.Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed, ToolType toolType,
                       List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super((float)attackDamageBaseLine, attackSpeed, tier, DIGGABLES, properties.addToolType(toolType, tier.getLevel()));
        this.speed = destroySpeed;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public ToolHarvestLogic getToolHarvestLogic() {
        return RectangleAOEHarvestLogic.SMALL;
    }

    /* Mining */

    @Override
    public Set<ToolType> getToolTypes(ItemStack stack) {
        return super.getToolTypes(stack);
    }

    @Override
    public int getHarvestLevel(ItemStack stack, ToolType toolClass, @Nullable PlayerEntity player, @Nullable BlockState blockState) {
        // brokenness is calculated in by the toolTypes check
        if (this.getToolTypes(stack).contains(toolClass)) {
            return this.getTier().getLevel();
        }
        return -1;
    }

    /**
     * As far as I can tell this does nothing because all the mining logic is hijacked by ToolHarvestLogic (It isn't
     * even fired when the player breaks a block with this item).
     * Keeping it here in the even that I'm wrong.
     */
    @Override
    public boolean mineBlock(ItemStack stack, World worldIn, BlockState state, BlockPos pos, LivingEntity entityLiving) {
        if (!worldIn.isClientSide() && worldIn instanceof ServerWorld) {
            boolean isEffective = getToolHarvestLogic().isEffective(stack, state);
            ToolHarvestContext context = new ToolHarvestContext((ServerWorld) worldIn, entityLiving, state, pos, Direction.UP, true, isEffective);
        }
        return true;
    }

    @Override
    public final boolean canHarvestBlock(ItemStack stack, BlockState state) {
        return this.getToolHarvestLogic().isEffective(stack, state);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return material != Material.METAL && material != Material.HEAVY_METAL && material != Material.STONE ? super.getDestroySpeed(stack, state) : this.speed;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack stack, BlockPos pos, PlayerEntity player) {
        return getToolHarvestLogic().handleBlockBreak(stack, pos, player);
    }
}