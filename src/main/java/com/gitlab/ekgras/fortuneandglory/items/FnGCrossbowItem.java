package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BowArrowEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.mixin.accessors.CrossbowItemAccessor;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.ICrossbowUser;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.FireworkRocketEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class FnGCrossbowItem extends CrossbowItem implements IDefaultEnchantmentData {

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    // Set to 2 for default crossbow damage
    private final double damage;

    public FnGCrossbowItem(Item.Properties properties, double damage,
                           List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);
        this.damage = damage;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack itemstack = player.getItemInHand(hand);
        if (isCharged(itemstack)) {
            performShooting(world, player, hand, itemstack, getShootingPower(itemstack), 1.0F);
            setCharged(itemstack, false);
            return ActionResult.consume(itemstack);
        } else if (!player.getProjectile(itemstack).isEmpty()) {
            return super.use(world, player, hand);
        } else {
            return ActionResult.fail(itemstack);
        }
    }

    public static void performShooting(World pLevel, LivingEntity pShooter, Hand pUsedHand, ItemStack pCrossbowStack, float pVelocity, float pInaccuracy) {
        List<ItemStack> list = CrossbowItemAccessor.getChargedProjectiles(pCrossbowStack);
        float[] afloat = CrossbowItemAccessor.getShotPitches(pShooter.getRandom());

        for(int i = 0; i < list.size(); ++i) {
            ItemStack itemstack = list.get(i);
            boolean flag = pShooter instanceof PlayerEntity && ((PlayerEntity)pShooter).abilities.instabuild;
            if (!itemstack.isEmpty()) {
                if (i == 0) {
                    shootProjectile(pLevel, pShooter, pUsedHand, pCrossbowStack, itemstack, afloat[i], flag, pVelocity, pInaccuracy, 0.0F);
                } else if (i == 1) {
                    shootProjectile(pLevel, pShooter, pUsedHand, pCrossbowStack, itemstack, afloat[i], flag, pVelocity, pInaccuracy, -10.0F);
                } else if (i == 2) {
                    shootProjectile(pLevel, pShooter, pUsedHand, pCrossbowStack, itemstack, afloat[i], flag, pVelocity, pInaccuracy, 10.0F);
                }
            }
        }

        CrossbowItemAccessor.onCrossbowShot(pLevel, pShooter, pCrossbowStack);
    }

    public void releaseUsing(ItemStack pStack, World pLevel, LivingEntity pEntityLiving, int pTimeLeft) {
        int i = this.getUseDuration(pStack) - pTimeLeft;
        float f = CrossbowItemAccessor.getPowerForTime(i, pStack);
        if (f >= 1.0F && !isCharged(pStack) && tryLoadProjectiles(pEntityLiving, pStack)) {
            setCharged(pStack, true);
            SoundCategory soundcategory = pEntityLiving instanceof PlayerEntity ? SoundCategory.PLAYERS : SoundCategory.HOSTILE;
            pLevel.playSound((PlayerEntity)null, pEntityLiving.getX(), pEntityLiving.getY(), pEntityLiving.getZ(), SoundEvents.CROSSBOW_LOADING_END, soundcategory, 1.0F, 1.0F / (random.nextFloat() * 0.5F + 1.0F) + 0.2F);
        }
    }

    private static boolean tryLoadProjectiles(LivingEntity pShooter, ItemStack pCrossbowStack) {
        int i = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.MULTISHOT, pCrossbowStack);
        int j = i == 0 ? 1 : 3;
        boolean flag = pShooter instanceof PlayerEntity && ((PlayerEntity)pShooter).abilities.instabuild;
        ItemStack itemstack = pShooter.getProjectile(pCrossbowStack);
        ItemStack itemstack1 = itemstack.copy();

        for(int k = 0; k < j; ++k) {
            if (k > 0) {
                itemstack = itemstack1.copy();
            }

            if (itemstack.isEmpty() && flag) {
                itemstack = new ItemStack(Items.ARROW);
                itemstack1 = itemstack.copy();
            }

            if (!loadProjectile(pShooter, pCrossbowStack, itemstack, k > 0, flag)) {
                return false;
            }
        }

        return true;
    }

    private static boolean loadProjectile(LivingEntity pShooter, ItemStack pCrossbowStack, ItemStack pAmmoStack, boolean pHasAmmo, boolean pIsCreative) {
        if (pAmmoStack.isEmpty()) {
            return false;
        } else {
            boolean flag = pIsCreative && pAmmoStack.getItem() instanceof ArrowItem;
            ItemStack itemstack;
            if (!flag && !pIsCreative && !pHasAmmo) {
                if (!EnchantmentHelper.getEnchantments(pCrossbowStack).containsKey(Enchantments.INFINITY_ARROWS)) itemstack = pAmmoStack.split(1);
                else itemstack = pAmmoStack.copy().split(1);
                if (pAmmoStack.isEmpty() && pShooter instanceof PlayerEntity) {
                    ((PlayerEntity)pShooter).inventory.removeItem(pAmmoStack);
                }
            } else {
                itemstack = pAmmoStack.copy();
            }

            CrossbowItemAccessor.addChargedProjectile(pCrossbowStack, itemstack);
            return true;
        }
    }

    private static void shootProjectile(World pLevel, LivingEntity pShooter, Hand pHand, ItemStack pCrossbowStack, ItemStack pAmmoStack, float pSoundPitch, boolean pIsCreativeMode, float pVelocity, float pInaccuracy, float pProjectileAngle) {
        if (!pLevel.isClientSide) {
            boolean flag = pAmmoStack.getItem() == Items.FIREWORK_ROCKET;
            ProjectileEntity projectileEntity;
            if (flag) {
                projectileEntity = new FireworkRocketEntity(pLevel, pAmmoStack, pShooter, pShooter.getX(), pShooter.getEyeY() - (double)0.15F, pShooter.getZ(), true);
            } else {
                projectileEntity = CrossbowItemAccessor.getArrow(pLevel, pShooter, pCrossbowStack, pAmmoStack);
                if (projectileEntity instanceof ArrowEntity) {
                    projectileEntity = BowArrowEntity.fromArrow((ArrowEntity) projectileEntity, pAmmoStack);
                    BowArrowEntity arrowEntity = (BowArrowEntity) projectileEntity;
                    Map<Enchantment, Integer> enchantments = EnchantmentHelper.getEnchantments(pCrossbowStack);
                    arrowEntity.setBaseDamage(((FnGCrossbowItem)pCrossbowStack.getItem()).damage);
                    ((BowArrowEntity) projectileEntity).setItemEnchantments(enchantments);
                    if (enchantments.containsKey(Enchantments.FLAMING_ARROWS)) projectileEntity.setSecondsOnFire(100);
                    if (enchantments.containsKey(Enchantments.PUNCH_ARROWS)) arrowEntity.setKnockback(enchantments.get(Enchantments.PUNCH_ARROWS));
                }
                if (pIsCreativeMode || pProjectileAngle != 0.0F || EnchantmentHelper.getEnchantments(pCrossbowStack).containsKey(Enchantments.INFINITY_ARROWS)) {
                    ((AbstractArrowEntity)projectileEntity).pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
                }
            }

            if (pShooter instanceof ICrossbowUser) {
                ICrossbowUser icrossbowuser = (ICrossbowUser)pShooter;
                icrossbowuser.shootCrossbowProjectile(icrossbowuser.getTarget(), pCrossbowStack, projectileEntity, pProjectileAngle);
            } else {
                Vector3d vector3d1 = pShooter.getUpVector(1.0F);
                Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), pProjectileAngle, true);
                Vector3d vector3d = pShooter.getViewVector(1.0F);
                Vector3f vector3f = new Vector3f(vector3d);
                vector3f.transform(quaternion);
                projectileEntity.shoot((double)vector3f.x(), (double)vector3f.y(), (double)vector3f.z(), pVelocity, pInaccuracy);
            }

            pCrossbowStack.hurtAndBreak(flag ? 3 : 1, pShooter, (p_220017_1_) -> {
                p_220017_1_.broadcastBreakEvent(pHand);
            });
            pLevel.addFreshEntity(projectileEntity);
            pLevel.playSound((PlayerEntity)null, pShooter.getX(), pShooter.getY(), pShooter.getZ(), SoundEvents.CROSSBOW_SHOOT, SoundCategory.PLAYERS, 1.0F, pSoundPitch);
        }
    }

    private float getShootingPower(ItemStack stack) {
        return stack.getItem() == Items.CROSSBOW && containsChargedProjectile(stack, Items.FIREWORK_ROCKET) ? 1.6F : 3.15F;
    }
}
