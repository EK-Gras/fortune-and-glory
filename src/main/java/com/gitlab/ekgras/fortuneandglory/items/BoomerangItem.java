package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BoomerangEntity;
import com.gitlab.ekgras.fortuneandglory.enums.BoomerangHitResult;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import net.minecraft.command.arguments.EntityAnchorArgument;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

import java.util.List;

import static net.minecraft.util.Hand.MAIN_HAND;

public class BoomerangItem extends Item implements IDefaultEnchantmentData {

    private final int incrementLength;
    private final int betweenTicks;
    private final int maxIncrements;
    private final int throwDamage;
    private final int returnCooldown;
    private final BoomerangHitResult result;
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public BoomerangItem(Properties properties, int incrementLengthIn, int betweenTicksIn, int maxIncrementsIn, int throwDamageIn,
                         int returnCooldown, BoomerangHitResult resultIn,
                         List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);
        this.incrementLength = incrementLengthIn;
        this.betweenTicks = betweenTicksIn;
        this.maxIncrements = maxIncrementsIn;
        this.throwDamage = throwDamageIn;
        this.returnCooldown = returnCooldown;
        this.result = resultIn;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    @Override
    public boolean isFoil(ItemStack stack) {
        return false;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (player.getItemInHand(hand).getItem() instanceof BoomerangItem) {
            ItemStack stack = player.getItemInHand(hand);
            CompoundNBT tag = stack.getTag();
            Vector3d lookVector = player.getLookAngle();
            tag.putBoolean("hand", hand == MAIN_HAND);
            tag.putBoolean("throwing", true);

            // Spawn the boomerang, add effects from enchantments
            ItemStack mCopy = stack.copy();
            BoomerangEntity boomerang = new BoomerangEntity(world, player, lookVector.x, lookVector.y, lookVector.z, throwDamage, stack.split(1));
            boomerang.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
            boomerang.setPos(player.getX(), player.getEyeY(), player.getZ());
            boomerang.lookAt(EntityAnchorArgument.Type.EYES, new Vector3d(player.getX() + lookVector.x, player.getEyeY() + lookVector.y, player.getZ() + lookVector.z));
            if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.FLAMING_ARROWS)) boomerang.setSecondsOnFire(100);
            player.getCommandSenderWorld().playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundCategory.PLAYERS, 1, 1);
            world.addFreshEntity(boomerang);

            // Spawn additional boomerangs if the boomerang has multishot
            if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.MULTISHOT)) {
                BoomerangEntity prevBoomerang = boomerang;

                for (int i = 0; i <= EnchantmentHelper.getItemEnchantmentLevel(Enchantments.MULTISHOT, stack); i++){
                    BoomerangEntity mBoomerang = new BoomerangEntity(world, player, lookVector.x, lookVector.y, lookVector.z, throwDamage, mCopy);
                    mBoomerang.following = prevBoomerang;
                    mBoomerang.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                    mBoomerang.setPos(player.getX(), player.getEyeY(), player.getZ());
                    mBoomerang.lookAt(EntityAnchorArgument.Type.EYES, new Vector3d(player.getX() + lookVector.x, player.getEyeY() + lookVector.y, player.getZ() + lookVector.z));
                    if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.FLAMING_ARROWS))
                        mBoomerang.setSecondsOnFire(100);
                    player.getCommandSenderWorld().playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundCategory.PLAYERS, 1, 1);
                    world.addFreshEntity(mBoomerang);
                    prevBoomerang = mBoomerang;
                }
            }

            // Set 'throwing' on the original ItemStack to false (the original item stack is deleted in survival, but persists in creative)
            tag.putBoolean("throwing", false);
            return ActionResult.sidedSuccess(stack, world.isClientSide);
        }
        return ActionResult.fail(player.getItemInHand(hand));
    }

    public int getIncrementLength() {
        return incrementLength;
    }

    public int getBetweenTicks() {
        return betweenTicks;
    }

    public int getMaxIncrements() {
        return maxIncrements;
    }

    public int getReturnCooldown() {
        return returnCooldown;
    }

    public BoomerangHitResult getResult() {
        return result;
    }
}
