package com.gitlab.ekgras.fortuneandglory.items.util;

import com.gitlab.ekgras.fortuneandglory.tags.FnGTags;
import com.gitlab.ekgras.fortuneandglory.util.StoreBlockFaceMinedHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;

import java.util.Collections;

public class SickleAOEHarvestLogic extends RectangleAOEHarvestLogic {

    public SickleAOEHarvestLogic(int extraWidth, int extraHeight, int extraDepth) {
        super(extraWidth, extraHeight, extraDepth);
    }


    @Override
    public boolean isEffective(ItemStack stack, BlockState state) {

        // harvest level too low -> not effective
        if (state.requiresCorrectToolForDrops() && stack.getHarvestLevel(state.getHarvestTool(), null, state) < state.getHarvestLevel()) {
            return false;
        }

        else if (FnGTags.PLANTS.contains(state.getBlock())) {
            return true;
        }

        // find a matching tool type
        return isEffectiveAgainst(stack, state);
    }

    @Override
    public boolean handleBlockBreak(ItemStack stack, BlockPos pos, PlayerEntity player) {
        // client can run normal block breaking
        if (player.getCommandSenderWorld().isClientSide() || !(player instanceof ServerPlayerEntity)) {
            return false;
        }

        // create contexts
        ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
        ServerWorld world = serverPlayer.getLevel();
        BlockState state = world.getBlockState(pos);
        Direction sideHit = StoreBlockFaceMinedHandler.getSideHit(player);

        // if broken, clear the item stack temporarily then break

        // add in harvest info
        ToolHarvestContext context = new ToolHarvestContext(world, serverPlayer, state, pos, sideHit,
                !player.isCreative() && state.canHarvestBlock(world, pos, player),
                stack.getItem().canHarvestBlock(stack, state));

        // This is the difference between this and RectangleAOEHarvest
        // If the block is a crop then the direction will always be up
        Direction sideHitNew = FnGTags.PLANTS.contains(world.getBlockState(context.getPos()).getBlock()) ? Direction.UP : sideHit;
        // need to calculate the iterator before we break the block, as we need the reference hardness from the center
        Iterable<BlockPos> extraBlocks = context.isEffective() ? getAOEBlocks(stack, player, state, world, pos, sideHitNew, AOEMatchType.BREAKING) : Collections.emptyList();

        // actually break the block, run AOE if successful
        if (breakBlock(stack, context)) {
            stack.hurtAndBreak(1, player, (entity) -> {
                entity.broadcastBreakEvent(EquipmentSlotType.MAINHAND);
            });
            for (BlockPos extraPos : extraBlocks) {
                BlockState extraState = world.getBlockState(extraPos);
                // prevent calling that stuff for air blocks, could lead to unexpected behaviour since it fires events
                // this should never actually happen, but just in case some AOE is odd
                if (!extraState.isAir(world, extraPos)) {
                    // prevent mutable position leak, breakBlock has a few places wanting immutable
                    breakExtraBlock(stack, context.forPosition(extraPos.immutable(), extraState));
                }
            }
        }

        return true;
    }
}
