package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.FnGProjectileItemEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class ThrowableItem extends Item implements IDefaultEnchantmentData {

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    private final int damage;
    private final int cooldown;

    public ThrowableItem(Properties properties, int damageIn, int cooldown,
                         List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);
        this.damage = damageIn;
        this.cooldown = cooldown;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        world.playSound((PlayerEntity)null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
        if (!world.isClientSide) {
            FnGProjectileItemEntity entity = new FnGProjectileItemEntity(world, player, stack, 1.0f, damage);
            entity.setItem(stack);
            entity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
            entity.shootFromRotation(player, player.xRot, player.yRot, 0.0F, 1.5F, 1.0F);
            if (entity.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS)) entity.setSecondsOnFire(100);
            world.addFreshEntity(entity);
            if (entity.getItemEnchantments().containsKey(Enchantments.MULTISHOT)) {
                for (int i = 0; i < 2; i++){
                    FnGProjectileItemEntity mEntity = new FnGProjectileItemEntity(world, player, stack, 1.0f, damage);
                    mEntity.setItem(stack);
                    mEntity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                    Vector3d vector3d1 = player.getUpVector(1.0F);
                    Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i == 0 ? 10F : -10F, true);
                    Vector3d vector3d = player.getViewVector(1.0F);
                    Vector3f vector3f = new Vector3f(vector3d);
                    vector3f.transform(quaternion);
                    mEntity.shoot(vector3f.x(), vector3f.y(), vector3f.z(), 0.75F, 1.0F);
                    Vector3d projectileVector = mEntity.getDeltaMovement();
                    mEntity.setDeltaMovement(mEntity.getDeltaMovement().add(projectileVector.x, mEntity.isOnGround() ? 0.0D : projectileVector.y, projectileVector.z));
                    if (mEntity.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                        mEntity.setSecondsOnFire(100);
                    world.addFreshEntity(mEntity);
                }
            }
        }

        player.awardStat(Stats.ITEM_USED.get(this));
        if (!player.abilities.instabuild && !EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.INFINITY_ARROWS)) {
            stack.shrink(1);
        }

        player.getCooldowns().addCooldown(this, cooldown);

        return ActionResult.sidedSuccess(stack, world.isClientSide());
    }
}
