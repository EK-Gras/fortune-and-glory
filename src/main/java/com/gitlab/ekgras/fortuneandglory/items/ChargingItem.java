package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;

public class ChargingItem extends MeleeItem {

    private final float attackDamage;
    private final float attackSpeed;
    private final int maxCharge;
    private final Ability ability;
    private final SoundEvent sound;
    private final float volume;
    private final float pitch;


    public ChargingItem(Item.Properties builder, IItemTier tier, int attackDamageIn, float attackSpeedIn, int charge, Ability abilityIn, SoundEvent soundIn, float volumeIn, float pitchIn,
                        List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder, tier, attackDamageIn, attackSpeedIn, defaultEnchantments, variantEnchantments);
        this.attackSpeed = attackSpeedIn;
        this.attackDamage = (float)attackDamageIn + tier.getAttackDamageBonus();
        this.maxCharge = charge;
        this.ability = abilityIn;
        this.sound = soundIn;
        this.volume = volumeIn;
        this.pitch = pitchIn;
    }


    public ActionResult<ItemStack> use(World worldIn, PlayerEntity player, Hand handIn) {
        if (player.getItemInHand(handIn).getItem() instanceof ChargingItem) {
            CompoundNBT tag = player.getItemBySlot(EquipmentSlotType.MAINHAND).getTag();
            if (tag.getBoolean("charged")) {
                if (ability.trigger(player)) {
                    tag.putBoolean("charged", false);
                    tag.putInt("hits", 0);
                    return ActionResult.success(player.getItemInHand(handIn));
                }
            }
            return ActionResult.fail(player.getItemInHand(handIn));
        }
        return ActionResult.fail(player.getItemInHand(handIn));
    }

    public int getMaxCharge() {
        return this.maxCharge;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class ChargingItemHandler {

        @SubscribeEvent
        public static void onLivingHurt(LivingHurtEvent event) {

            //If the source of the damage is a Living Entity
            if (event.getSource().getDirectEntity() instanceof LivingEntity) {

                //Gets the entity who attacked.
                LivingEntity source = (LivingEntity) event.getSource().getEntity();

                //Get the held item of the source
                ItemStack stack = source.getItemBySlot(EquipmentSlotType.MAINHAND);

                //If the entity is holding a Charging item
                if (stack.getItem() instanceof ChargingItem) {

                    //Get the item being used. This is used to get the attack damage and max combo of the item later.
                    ChargingItem ci = (ChargingItem) stack.getItem();

                    //Get the NBT of the item
                    CompoundNBT tag = stack.getTag();

                    //Give hits a value if it doesn't already have one. This is to prevent a possible NullPointerException when using getInt later on
                    if (tag.get("hits") == null) {
                        tag.putInt("hits", 0);
                    }

                    //If the item has not hit its max number of hits, is not yet charged, and the damage dealt was greater than or equal to the attack damage of the weapon...
                    if (tag.getInt("hits") < ci.maxCharge && !tag.getBoolean("charged") && event.getAmount() >= source.getAttributeValue(Attributes.ATTACK_DAMAGE)) {
                        //Increment hits by 1
                        tag.putInt("hits", tag.getInt("hits") + 1);
                    }

                    //If the item has hit its max number of hits and is not yet charged...
                    if (tag.getInt("hits") >= ci.maxCharge && !tag.getBoolean("charged")) {
                        source.getCommandSenderWorld().playSound(null, source.getX(), source.getY(), source.getZ(), ci.sound, SoundCategory.PLAYERS, ci.volume, ci.pitch);
                        //Set charged to true
                        tag.putBoolean("charged", true);
                    }
                }
            }
        }
    }
}
