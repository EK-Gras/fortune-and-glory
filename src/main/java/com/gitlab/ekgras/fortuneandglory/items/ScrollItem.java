package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.EnergyBallEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class ScrollItem extends EnergyItem {
    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Ability performed by the item
    private final Ability ability;

    public ScrollItem(Properties properties, int fireSpeedIn, float usageRateIn, Ability abilityIn,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, fireSpeedIn, usageRateIn, 0, 0, defaultEnchantments, variantEnchantments);
        this.fireSpeed = fireSpeedIn;
        this.usageRate = usageRateIn;
        this.ability = abilityIn;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        if (stack.getItem() instanceof ScrollItem) {
            CompoundNBT tag = stack.getTag();
            float energy = tag.getFloat("energy");
            if (energy >= this.usageRate) {
                if (ability.trigger(player)) {
                    tag.putFloat("energy", energy - this.usageRate);
                    player.getCooldowns().addCooldown(this, this.fireSpeed);
                    if (tag.getFloat("energy") <= 0) {
                        stack.shrink(1);
                        return ActionResult.consume(stack);
                    }
                    return ActionResult.success(stack);
                } else {
                    return ActionResult.fail(stack);
                }
            }
        }
        return ActionResult.fail(stack);
    }
}
