package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BulletEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BulletItem extends Item {
    public BulletItem (Item.Properties builder) {
        super(builder);
    }

//    public BulletEntity createBullet(World worldIn, ItemStack stack, LivingEntity shooter) {
//        BulletEntity bulletentity = new BulletEntity(worldIn, shooter);
//        return bulletentity;
//    }

    public boolean isInfinite(ItemStack stack, ItemStack gun, net.minecraft.entity.player.PlayerEntity player) {
        int enchant = net.minecraft.enchantment.EnchantmentHelper.getItemEnchantmentLevel(net.minecraft.enchantment.Enchantments.INFINITY_ARROWS, gun);
        return enchant > 0 && this.getClass() == BulletItem.class;
    }

    public boolean isAmmo() {
        return true;
    }

}
