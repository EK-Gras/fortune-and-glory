//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gitlab.ekgras.fortuneandglory.items;


import net.minecraft.entity.projectile.FireworkRocketEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.world.World;


import net.minecraft.item.Item.Properties;

public class ElytraRocketItem extends FireworkRocketItem {
    public ElytraRocketItem(Properties p_i48498_1_) {
        super(p_i48498_1_);
    }

    @Override
    public ActionResultType useOn(ItemUseContext context) {
        return ActionResultType.FAIL;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (player.isFallFlying()) {
            ItemStack stack = player.getItemInHand(hand);
            if (!world.isClientSide) {
                world.addFreshEntity(new FireworkRocketEntity(world, stack, player));
            }

            return ActionResult.success(player.getItemInHand(hand));
        } else {
            return ActionResult.pass(player.getItemInHand(hand));
        }
    }

}
