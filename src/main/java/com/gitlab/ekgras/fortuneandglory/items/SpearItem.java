package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.SpearEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class SpearItem extends TridentItem implements IDefaultEnchantmentData {

    private final Multimap<Attribute, AttributeModifier> defaultModifiers;
    private final int throwDamage;
    private final int attackDamage;
    private final float attackSpeed;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public SpearItem(Item.Properties builder, int throwDamageIn, int attackDamageIn, float attackSpeedIn,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder);
        this.throwDamage = throwDamageIn;
        this.attackDamage = attackDamageIn;
        this.attackSpeed = attackSpeedIn;
        ImmutableMultimap.Builder<Attribute, AttributeModifier> defaultModifiersIn = ImmutableMultimap.builder();
        defaultModifiersIn.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Tool modifier", attackDamageIn, AttributeModifier.Operation.ADDITION));
        defaultModifiersIn.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", attackSpeedIn, AttributeModifier.Operation.ADDITION));
        this.defaultModifiers = defaultModifiersIn.build();
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(player.getItemInHand(hand));
        return super.use(world, player, hand);
    }

    @Override
    public void releaseUsing(ItemStack stack, World world, LivingEntity entity, int timeLeft) {
        if (entity instanceof PlayerEntity && stack.getItem() instanceof SpearItem) {
            PlayerEntity player = (PlayerEntity)entity;
            int i = this.getUseDuration(stack) - timeLeft;
            Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
            int time = (
                enchants.containsKey(Enchantments.QUICK_CHARGE)
                    ? (int)(10D - 2.5D * (double)enchants.get(Enchantments.QUICK_CHARGE))
                    : 10
            );
            if (i >= time) {
                int riptide = EnchantmentHelper.getRiptide(stack);
                if (riptide <= 0 || player.isInWaterOrRain()) {
                    if (!world.isClientSide) {
                        stack.hurtAndBreak(1, entity, (livingEntity) -> {
                            livingEntity.broadcastBreakEvent(entity.getUsedItemHand());
                        });
                        if (riptide == 0) {
                            SpearEntity spear = new SpearEntity(world, player, stack);
                            spear.setItemEnchantments(enchants);
                            spear.shootFromRotation(player, player.xRot, player.yRot, 0.0F, 2.5F + (float)riptide * 0.5F, 1.0F);
                            spear.setBaseDamage(this.throwDamage);
                            if (player.abilities.instabuild || enchants.containsKey(Enchantments.INFINITY_ARROWS)) {
                                spear.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
                            }
                            Map<Enchantment, Integer> entityEnchants = spear.getItemEnchantments();
                            if (entityEnchants.containsKey(Enchantments.FLAMING_ARROWS)) spear.setSecondsOnFire(100);
                            if (entityEnchants.containsKey(Enchantments.PUNCH_ARROWS))
                                spear.setKnockback(entityEnchants.get(Enchantments.PUNCH_ARROWS));

                            world.addFreshEntity(spear);
                            if (enchants.containsKey(Enchantments.MULTISHOT)) {
                                for (int iF = 0; iF < 2; iF++) {
                                    SpearEntity multiSpear = new SpearEntity(world, player, stack);
                                    multiSpear.setItemEnchantments(enchants);
                                    Vector3d vector3d1 = entity.getUpVector(1.0F);
                                    Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), iF == 0 ? 10f : -10f, true);
                                    Vector3d vector3d = entity.getViewVector(1.0F);
                                    Vector3f vector3f = new Vector3f(vector3d);
                                    vector3f.transform(quaternion);
                                    multiSpear.shoot(vector3f.x(), vector3f.y(), vector3f.z(), 2.5F + (float) riptide * 0.5F, 1.0F);
                                    Vector3d playerVector = entity.getDeltaMovement();
                                    multiSpear.setDeltaMovement(multiSpear.getDeltaMovement().add(playerVector.x, multiSpear.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
                                    multiSpear.setBaseDamage(this.throwDamage);
                                    multiSpear.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
                                    if (spear.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                                        spear.setSecondsOnFire(100);
                                    world.addFreshEntity(multiSpear);
                                }
                            }
                            world.playSound(null, spear, SoundEvents.TRIDENT_THROW, SoundCategory.PLAYERS, 1.0F, 1.0F);
                            if (!player.abilities.instabuild && !enchants.containsKey(Enchantments.INFINITY_ARROWS)) {
                                player.inventory.removeItem(stack);
                            }
                        }
                    }

                    player.awardStat(Stats.ITEM_USED.get(this));
                    if (riptide > 0) {
                        float yRot = player.yRot;
                        float xRot = player.xRot;
                        float angle1 = -MathHelper.sin(yRot * 0.017453292F) * MathHelper.cos(xRot * 0.017453292F);
                        float angle2 = -MathHelper.sin(xRot * 0.017453292F);
                        float angle3 = MathHelper.cos(yRot * 0.017453292F) * MathHelper.cos(xRot * 0.017453292F);
                        float angle4 = MathHelper.sqrt(angle1 * angle1 + angle2 * angle2 + angle3 * angle3);
                        float speedModifier = 3.0F * ((1.0F + (float)riptide) / 4.0F);
                        angle1 *= speedModifier / angle4;
                        angle2 *= speedModifier / angle4;
                        angle3 *= speedModifier / angle4;
                        player.push(angle1, angle2, angle3);
                        player.startAutoSpinAttack(20);
                        if (player.isOnGround()) {
                            float lvt_15_1_ = 1.1999999F;
                            player.move(MoverType.SELF, new Vector3d(0.0D, 1.1999999284744263D, 0.0D));
                        }

                        SoundEvent sound;
                        if (riptide >= 3) {
                            sound = SoundEvents.TRIDENT_RIPTIDE_3;
                        } else if (riptide == 2) {
                            sound = SoundEvents.TRIDENT_RIPTIDE_2;
                        } else {
                            sound = SoundEvents.TRIDENT_RIPTIDE_1;
                        }

                        world.playSound(null, player, sound, SoundCategory.PLAYERS, 1.0F, 1.0F);
                    }
                }
            }
        }
    }

    public int getThrowDamage() {
        return this.throwDamage;
    }
}
