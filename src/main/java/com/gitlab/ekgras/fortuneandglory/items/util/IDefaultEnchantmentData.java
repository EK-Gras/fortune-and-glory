package com.gitlab.ekgras.fortuneandglory.items.util;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
This is the interface for indicating an Item's default enchantments
 (Nothing here right now. Will code later)
 */

public interface IDefaultEnchantmentData {

    static Map<Enchantment, Integer> defaultsForItem(Item item) {
        if (item instanceof IDefaultEnchantmentData) {
            Map<Enchantment, Integer> enchants = new HashMap<>();
            IDefaultEnchantmentData data = (IDefaultEnchantmentData) item;
            data.getDefaultEnchantments().forEach(
                    de -> enchants.put(de.getSupplier().get(), de.getLevel())
                    );
            return enchants;
        } else {
            return Collections.emptyMap();
        }
    }

    static Map<Enchantment, Integer> variantsForItem(Item item) {
        if (item instanceof IDefaultEnchantmentData) {
            Map<Enchantment, Integer> enchants = new HashMap<>();
            IDefaultEnchantmentData data = (IDefaultEnchantmentData) item;
            data.getDefaultEnchantments().forEach(
                    de -> enchants.put(de.getSupplier().get(), de.getLevel())
            );
            return enchants;
        } else {
            return Collections.emptyMap();
        }
    }

    public List<LeveledEnchantment> getDefaultEnchantments();

    public List<LeveledEnchantment> getVariantEnchantments();

}
