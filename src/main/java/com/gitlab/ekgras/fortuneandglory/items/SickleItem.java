package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.*;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SickleItem extends AOEToolItem implements IToolEffectivenessSets {

    private static final SickleAOEHarvestLogic HARVEST_LOGIC = new SickleAOEHarvestLogic(1, 1, 0);
    private final float speed;
    private static final Set<Block> DIGGABLES = IToolEffectivenessSets.HOE;

    public SickleItem(Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, tier, attackDamageBaseLine, attackSpeed, destroySpeed, ToolType.HOE,
                defaultEnchantments, variantEnchantments);
        this.speed = destroySpeed;
    }

    @Override
    public ToolHarvestLogic getToolHarvestLogic() {
        return HARVEST_LOGIC;
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state) {
        if (getToolTypes(stack).stream().anyMatch(e -> state.isToolEffective(e))) return speed;
        return DIGGABLES.contains(state.getBlock()) ? this.speed : 1.0F;
    }

    public ActionResultType useOn(ItemUseContext context) {
        World world = context.getLevel();
        BlockPos blockpos = context.getClickedPos();
        int hook = net.minecraftforge.event.ForgeEventFactory.onHoeUse(context);
        if (hook != 0) return hook > 0 ? ActionResultType.SUCCESS : ActionResultType.FAIL;
        if (context.getClickedFace() != Direction.DOWN && world.isEmptyBlock(blockpos.above())) {
            BlockState blockstate = world.getBlockState(blockpos).getToolModifiedState(world, blockpos, context.getPlayer(), context.getItemInHand(), net.minecraftforge.common.ToolType.HOE);
            if (blockstate != null) {
                PlayerEntity playerentity = context.getPlayer();
                world.playSound(playerentity, blockpos, SoundEvents.HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
                if (!world.isClientSide) {
                    world.setBlock(blockpos, blockstate, 11);
                    if (playerentity != null) {
                        context.getItemInHand().hurtAndBreak(1, playerentity, (p_220043_1_) -> {
                            p_220043_1_.broadcastBreakEvent(context.getHand());
                        });
                    }
                }

                return ActionResultType.sidedSuccess(world.isClientSide);
            }
        }

        return ActionResultType.PASS;
    }

}
