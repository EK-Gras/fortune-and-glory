package com.gitlab.ekgras.fortuneandglory.items.util;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;

import java.util.Map;

public class EnchantmentProjectileHelper {

    public static void applyEnchantmentsFromItem(ItemStack stack, ProjectileEntity entity) {
        Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
        if (entity instanceof IEnchantmentProjectileData) {
            ((IEnchantmentProjectileData) entity).setItemEnchantments(enchants);
        }
        else {
            if (enchants.containsKey(FnGEnchantments.DRAINING_ASPECT.get())) {
                entity.addTag("FnGLifesteal");
            }
        }
    }

}
