package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class RepeaterBowItem extends FnGBowItem {

    // Base damage of the arrow (before multiplier for crits & other enchantments)
    private final double damage;

    // Fields used for determining the number of times the bow shoots per draw, and the delay between shots.
    private final int shots;
    private final long shotDelay;

    // Time before the bow starts drawing extra arrows
    private final int startDelay;

    // Time between each arrow being drawn
    private final int drawDelay;

    public RepeaterBowItem(Item.Properties builder, double damage, int shots, long delay, int startDelay, int drawDelay,
                           List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder, damage, defaultEnchantments, variantEnchantments);
        this.damage = damage;
        this.shots = shots;
        this.shotDelay = delay;
        this.startDelay = startDelay;
        this.drawDelay = drawDelay;
    }

    @Override
    public void onUseTick(World world, LivingEntity entity, ItemStack stack, int timeLeft) {
        int enchStartDelay = startDelay / (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.QUICK_CHARGE, stack) + 1);
        int enchDrawDelay = drawDelay / (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.QUICK_CHARGE, stack) + 1);
        if (this.getUseDuration(stack) - timeLeft >= enchStartDelay) {
            CompoundNBT tag = stack.getTag();
            if (tag.getInt("arrowsLoaded") < this.shots) {
                if (tag.getInt("drawCooldown") <= 0) {
                    tag.putInt("drawCooldown", this.drawDelay);
                    world.playSound(null, entity.getX(), entity.getY(), entity.getZ(), SoundEvents.DISPENSER_FAIL, SoundCategory.PLAYERS, 1.2F, (float) (1.0F + (tag.getInt("arrowsLoaded") * 0.1)));
                    tag.putInt("arrowsLoaded", tag.getInt("arrowsLoaded") + 1);
                }
                tag.putInt("drawCooldown", tag.getInt("drawCooldown") - 1);
            }
        }
    }

    @Override
    public void releaseUsing(ItemStack stack, World world, LivingEntity entity, int timeLeft) {
        if (entity instanceof PlayerEntity) {

            Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);
            int useTime = this.getUseDuration(stack) - timeLeft;
            PlayerEntity playerentity = (PlayerEntity) entity;
            // The itemStack and shouldConsumeAmmo variables no longer work outside the lambda, so this is a workaround
            ItemStack ammo = playerentity.getProjectile(stack);
            boolean shouldConsumeAmmo = playerentity.abilities.instabuild || (ammo.getItem() instanceof ArrowItem && ((ArrowItem) ammo.getItem()).isInfinite(ammo, stack, playerentity));
            CompoundNBT tag = stack.getTag();

            stack.getTag().putBoolean("firstArrowFired", true);

            stack.hurtAndBreak(1, entity, (livingEntity) -> {
                livingEntity.broadcastBreakEvent(playerentity.getUsedItemHand());
            });

            int useTimeBonus = 0;
            if (enchants.containsKey(Enchantments.QUICK_CHARGE)) {
                useTimeBonus += enchants.get(Enchantments.QUICK_CHARGE) * 5;
            }
            float power = this.getArrowVelocity(useTime + useTimeBonus);

            // This for loop shoots as many arrows as there are shots OR, if the bow was not drawn back all the way it defaults to 1.
            for (int z = 0; z < (this.getUseDuration(stack) - timeLeft >= 20 && tag.getInt("arrowsLoaded") > 0 ? tag.getInt("arrowsLoaded") : 1); z++) {
                // This schedules the arrow to be shot (Delay) ticks times the current number of arrows shot
                TickScheduler.schedule(z * this.shotDelay, world.isClientSide, () -> {
                    this.fireArrow(world, stack, ammo, playerentity, shouldConsumeAmmo, power, 0);

                    if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.MULTISHOT) && power >= 1.0F) {
                        fireArrow(world, stack, ammo, playerentity, shouldConsumeAmmo, power, 10);
                        fireArrow(world, stack, ammo, playerentity, shouldConsumeAmmo, power, -10);
                    }
                });
            }

            tag.putInt("arrowsLoaded", 0);
            tag.putInt("drawCooldown", 0);

            if (!shouldConsumeAmmo && !playerentity.abilities.instabuild) {
                ammo.shrink(1);
                if (ammo.isEmpty()) {
                    playerentity.inventory.removeItem(ammo);
                }
            }

            playerentity.awardStat(Stats.ITEM_USED.get(this));

        }
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return slotChanged;
    }
}
