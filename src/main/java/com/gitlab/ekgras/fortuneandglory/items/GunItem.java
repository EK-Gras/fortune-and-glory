package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import com.gitlab.ekgras.fortuneandglory.init.FnGSounds;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.network.FnGPacketHandler;
import com.gitlab.ekgras.fortuneandglory.network.packet.FireBulletPacket;
import com.gitlab.ekgras.fortuneandglory.network.packet.StopUsingItemPacket;
import com.gitlab.ekgras.fortuneandglory.network.packet.UpdateElementalPercentagesPacket;
import com.gitlab.ekgras.fortuneandglory.network.packet.UseGunPacket;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeItem;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Predicate;

import static net.minecraft.util.Hand.MAIN_HAND;
import static net.minecraft.util.Hand.OFF_HAND;

public class GunItem extends ShootableItem implements IForgeItem, IDefaultEnchantmentData {

    public final Predicate<ItemStack> BULLETS = (stack) -> {
        return stack.getItem() instanceof BulletItem;
    };

    // Velocity of the gun in meters (blocks) per tick.
    private final float velocity;

    // Capacity of the gun I.E. the amount of bullets a gun shoots before reloading.
    private final int capacity;

    // Number of ticks between shots UNLESS the gun needs to reload.
    private final int cooldown;

    // Number of ticks the gun needs to reload.
    private final int reloadSpeed;

    // Inaccuracy of the gun. Calculated in the same way as bows.
    private final float inaccuracy;

    // Number of bullets fired per trigger pull (left click).
    private final int shots;

    // Amount of damage the bow deals.
    private final int damage;

    // Amount of recoil in degrees I.E. how far up the player's vision is moved after firing.
    private final int recoil;

    // How far the gun's bullets travel in meters (blocks).
    private final float range;

    // Whether or not the gun fires automatically (Guns which are automatic fire as long as the fire button is held down).
    private final boolean automatic;

    // Color values to be passed in to the bullet entity
    private final float red;
    private final float green;
    private final float blue;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public GunItem(Item.Properties builder, float velocity, int capacity, int cooldown, int reloadSpeed, float inaccuracy, int shots, int damage, int recoil, float range, boolean automatic, float red, float green, float blue,
                   List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder);
        this.velocity = velocity;
        this.capacity = capacity;
        this.cooldown = cooldown;
        this.reloadSpeed = reloadSpeed;
        this.inaccuracy = inaccuracy;
        this.shots = shots;
        this.damage = damage;
        this.recoil = recoil;
        this.range = range;
        this.automatic = automatic;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    @Override
    public boolean canAttackBlock(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
        return false;
    }

    public float getVelocity() {
        return this.velocity;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public int getCooldown() {
        return this.cooldown;
    }

    public int getReloadSpeed() {
        return this.reloadSpeed;
    }

    public float getInaccuracy() {
        return this.inaccuracy;
    }

    public float getShots() {
        return this.shots;
    }

    public float getDamage() { return this.damage; }

    public int getRecoil() {return this.recoil;}

    public float getRange() {
        return this.range;
    }

    public boolean isAutomatic() {
        return this.automatic;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }

    public Random getRandom () {
        return random;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (hand == MAIN_HAND && player.getItemInHand(OFF_HAND).isEmpty() && player.getItemInHand(MAIN_HAND).getItem() instanceof GunItem) {
            return ActionResult.pass(player.getItemBySlot(EquipmentSlotType.MAINHAND));
        }
        else {
            return ActionResult.fail(player.getItemBySlot(hand == MAIN_HAND ? EquipmentSlotType.MAINHAND : EquipmentSlotType.OFFHAND));
        }
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.BOW;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 72000;
    }

    @Override
    public Predicate<ItemStack> getAllSupportedProjectiles() {
        return BULLETS;
    }

    @Override
    public int getDefaultProjectileRange() {
        return (int)this.range;
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean isSelected) {
        CompoundNBT tag = stack.getOrCreateTag();
        if (isSelected && !tag.getBoolean("selectUpdated")) {
            tag.putBoolean("selectUpdated", true);
            tag.putBoolean("isCooldown", true);
            tag.putInt("cooldown", ((GunItem) stack.getItem()).cooldown);
        }
        else if (!isSelected) {
            tag.putBoolean("selectUpdated", false);
        }
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class GunItemHandler {

        @SubscribeEvent
        public static void onPlayerTick (TickEvent.PlayerTickEvent event) {
            PlayerEntity player = event.player;
            ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            World world = player.getCommandSenderWorld();
            TickEvent.Phase phase = event.phase;
            if (stack.getItem() instanceof GunItem && phase == TickEvent.Phase.START) {
                GunItem gun = (GunItem) stack.getItem();
                CompoundNBT tag = stack.getOrCreateTag();
                ItemStack ammo = player.getProjectile(stack).getItem() == Items.ARROW ? new ItemStack(FnGItems.BULLET.get()) : player.getProjectile(stack);

                if (tag.getBoolean("isReloading") && player.getUseItem() == stack && !world.isClientSide) {
                    FnGPacketHandler.sendToAllTrackingEntity(new StopUsingItemPacket(player.getId()), () -> player);
                    player.stopUsingItem();
                }

                // If the gun is reloading, and the player is in creative mode and/or has bullets in their inventory...
                if (tag.getBoolean("isReloading") && (player.isCreative() || !(ammo == ItemStack.EMPTY))) {
                    //Subtract 1 from the reloadCooldown
                    tag.putInt("reloadCooldown", tag.getInt("reloadCooldown") - 1);
                    // If that decrement set the reloadCooldown to zero...
                    if (tag.getInt("reloadCooldown") == 0) {
                        // Set isReloading back to false, and put bullets in the gun.
                        tag.putBoolean("isReloading", false);
                        int count = 0;
                        /* getProjectile is designed for bows which consume one arrow at a time, so it can't find multiple stacks in the event that the gun
                        needs to reload more bullets than what is in the current stack. So, this for loop searches for ammo every time and shrinks it one
                        by one */
                        if (!player.isCreative() && !(EnchantmentHelper.getItemEnchantmentLevel(Enchantments.INFINITY_ARROWS, stack) > 0)) {
                            for (int i = 0; i < gun.getCapacity(); i++) {
                                ItemStack s = player.getProjectile(stack);
                                if (s.getItem() instanceof BulletItem) {
                                    s.shrink(1);
                                    count++;
                                    if (count >= gun.capacity - tag.getInt("bulletsLoaded")) break;
                                } else break;
                            }
                        }
                        else if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.INFINITY_ARROWS, stack) > 0) {
                            if (player.getProjectile(stack).getItem() instanceof BulletItem) count = gun.capacity - tag.getInt("bulletsLoaded");
                        }
                        else {
                            count = gun.capacity - tag.getInt("bulletsLoaded");
                        }
                        //Put either as many bullets in the gun as it can hold, or as many bullets as the player has left in their inventory, if they don't have enough to fill the gun.
                        tag.putInt("bulletsLoaded", tag.getInt("bulletsLoaded") + count);
                        //Play the sound for when a gun finishes reloading.
                        world.playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ITEM_GUN_FINISH_RELOAD.get(), SoundCategory.PLAYERS, 2.0F, 1.0F);
                    }
                }

                if (tag.getInt("bulletsLoaded") != 0 && tag.getBoolean("isCooldown") && tag.getInt("cooldown") == 0) {
                    tag.putInt("cooldown", gun.getCooldown());
                }

                if (tag.getInt("bulletsLoaded") != 0 && tag.getBoolean("isCooldown") && tag.getInt("cooldown") > 0) {
                    long ticks = world.getGameTime();
                    tag.putInt("cooldown", tag.getInt("cooldown") - 1);
                    if (tag.getInt("cooldown") == 0) {
                        tag.putBoolean("isCooldown", false);
                    }
                }

                if (world.isClientSide() && !player.hasEffect(FnGEffects.SHOCK.get())) {
                    if (Minecraft.getInstance().options.keyAttack.isDown() && !tag.getBoolean("isReloading") && (player.getUseItem().getItem() instanceof GunItem || player.getUseItem() == ItemStack.EMPTY)) {
                        player.startUsingItem(MAIN_HAND);
                    }

                    if (Minecraft.getInstance().options.keyAttack.isDown() && gun.automatic && !tag.getBoolean("isReloading") && !tag.getBoolean("isCooldown")
                            && (player.getUseItem().getItem() instanceof GunItem || player.getUseItem() == ItemStack.EMPTY)) {
                        // ClickInputEvent only fires on the logical client, so in order to fire the bullet a packet must be sent to the server.
                        for (int i = 0; i < gun.shots; i++) {
                            FireBulletPacket fMsg = FireBulletPacket.create(gun, true, player.getViewVector(1.0F));
                            FnGPacketHandler.sendToServer(fMsg);
                            if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.MULTISHOT)) {
                                for (int i1 = 0; i1 < 2; i1++) {
                                    Vector3d vector3d1 = player.getUpVector(1.0F);
                                    Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i1 == 0 ? -10 : 10, true);
                                    Vector3d vector3d = player.getViewVector(1.0F);
                                    Vector3f vector3f = new Vector3f(vector3d);
                                    vector3f.transform(quaternion);
                                    FireBulletPacket fmMsg = FireBulletPacket.create(gun, true, new Vector3d(vector3f));
                                    FnGPacketHandler.sendToServer(fmMsg);
                                }
                            }
                        }
                        UseGunPacket uMsg = new UseGunPacket(true);
                        FnGPacketHandler.sendToServer(uMsg);
                        player.absMoveTo(player.getX(), player.getY(), player.getZ(), player.yRot, player.xRot - (gun.recoil));


                        // If the gun still has bullets loaded, then put it in cooldown
                        if (tag.getInt("bulletsLoaded") > 0) {
                            tag.putBoolean("isCooldown", true);
                            tag.putInt("cooldown", gun.cooldown);
                        }

                        // If the gun has no bullets loaded but is not currently reloaded, start reloading it.
                        // isReloading and reloadCooldown need to be separate flags so there is a difference between when the gun has not yet started reloading and when the gun is finished reloading.
                        if (tag.getInt("bulletsLoaded") <= 0 && !tag.getBoolean("isReloading")) {
                            tag.putBoolean("isReloading", true);
                            tag.putInt("reloadCooldown", gun.getReloadSpeed());
                            world.playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ITEM_GUN_START_RELOAD.get(), SoundCategory.PLAYERS, 2.0F, 1.0F);
                        }
                    }
                }
            }
        }

        @SubscribeEvent
        public static void onLivingAttack(AttackEntityEvent event) {
            if (event.getPlayer().getItemInHand(MAIN_HAND).getItem() instanceof GunItem) {
                event.setCanceled(true);
            }
        }

        @SubscribeEvent
        public static void onPlayerKnockEntity(LivingKnockBackEvent event) {
            if (event.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntity();
                if (player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof GunItem) {
                    event.setCanceled(true);
                }
            }
        }

        @SubscribeEvent
        public static void onPlayerSelectItem (LivingEquipmentChangeEvent event) {
            if (event.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntity();
                if (player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof GunItem) {
                    ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
                    GunItem gun = (GunItem) stack.getItem();
                    CompoundNBT tag = stack.getTag();
                    if (tag.getInt("bulletsLoaded") <= 0 && !tag.getBoolean("isReloading")) {
                        tag.putBoolean("isReloading", true);
                        tag.putInt("reloadCooldown", ((GunItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem()).getReloadSpeed());
                        player.getCommandSenderWorld().playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ITEM_GUN_START_RELOAD.get(), SoundCategory.PLAYERS, 2.0F, 1.0F);
                    }
                }
            }
        }
    }
}