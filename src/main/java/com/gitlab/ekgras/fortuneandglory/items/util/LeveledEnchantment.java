package com.gitlab.ekgras.fortuneandglory.items.util;

import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.enchantment.Enchantment;

import java.util.Objects;
import java.util.function.Supplier;

public class LeveledEnchantment {

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeveledEnchantment that = (LeveledEnchantment) o;
        return Objects.equals(enchant, that.enchant) && Objects.equals(level, that.level);
    }

    public int hashCode() {
        return Objects.hash(enchant, level);
    }

    public Supplier<Enchantment> getSupplier() {
        return enchant;
    }

    public Enchantment getEnchant() {
        return enchant.get();
    }

    public void setEnchant(Supplier<Enchantment> enchant) {
        this.enchant = enchant;
    }

    public void setEnchant(Enchantment enchant) {
        this.enchant = () -> enchant;
    }

    private Supplier<Enchantment> enchant;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    private Integer level;

    public LeveledEnchantment(Supplier<Enchantment> enchant, Integer level) {
        this.enchant = enchant;
        this.level = level;
    }

    public LeveledEnchantment(Enchantment enchant, Integer level) {
        this.enchant = () -> enchant;
        this.level = level;
    }

}
