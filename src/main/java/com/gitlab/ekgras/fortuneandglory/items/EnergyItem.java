package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import com.gitlab.ekgras.fortuneandglory.init.FnGSounds;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;

import static java.lang.String.valueOf;

public class EnergyItem extends Item implements IDefaultEnchantmentData {

    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // Parameters for the sound the staff should make when used
    public final SoundEvent sound;
    public final float volume;
    public final float pitch;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public EnergyItem(Properties properties, int fireSpeed, float usageRate, float rechargeRate, int rechargeCooldown,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        this(properties, fireSpeed, usageRate, rechargeRate, rechargeCooldown, null, 0, 0, defaultEnchantments, variantEnchantments);
    }

    public EnergyItem(Properties properties, int fireSpeed, float usageRate, float rechargeRate, int rechargeCooldown,
                      SoundEvent sound, float volume, float pitch,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);
        this.fireSpeed = fireSpeed;
        this.usageRate = usageRate;
        this.rechargeRate = rechargeRate;
        this.rechargeCooldown = rechargeCooldown;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (player.getItemInHand(hand).getItem() instanceof EnergyItem) {
            ItemStack stack = player.getItemInHand(hand);
            CompoundNBT tag = stack.getTag();
            float energy = tag.getFloat("energy");
            if (energy >= this.usageRate) {
                tag.putFloat("energy", energy - this.usageRate);
                player.getCooldowns().addCooldown(this, this.fireSpeed);
                tag.putInt("rechargeCooldown", this.rechargeCooldown);
                return ActionResult.success(stack);
            } else {
                return ActionResult.fail(stack);
            }
        }
        return ActionResult.fail(player.getItemInHand(hand));
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return slotChanged;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class EnergyItemHandler {

        @SubscribeEvent
        public static void onPlayerTick (TickEvent.PlayerTickEvent event) {
            PlayerEntity player = event.player;
            World world = player.getCommandSenderWorld();
            ItemStack[] stacks = {player.getItemBySlot(EquipmentSlotType.MAINHAND), player.getItemBySlot(EquipmentSlotType.OFFHAND)};
            for (ItemStack stack : stacks) {
                if (stack.getItem() instanceof EnergyItem) {
                    EnergyItem item = (EnergyItem) stack.getItem();
                    CompoundNBT tag = stack.getTag();
                    if (tag.getFloat("energy") < 100 && tag.getFloat("rechargeCooldown") <= 0) {
                        tag.putFloat("energy", tag.getFloat("energy") + item.rechargeRate);
                    }
                    if (tag.getInt("rechargeCooldown") > 0) {
                        tag.putInt("rechargeCooldown", tag.getInt("rechargeCooldown") - 1);
                    }
                    if (tag.getFloat("energy") > 100) {
                        tag.putFloat("energy", 100);
                    }
                    if (tag.getFloat("energy") < 0) {
                        tag.putFloat("energy", 0);
                    }
                }
            }
        }

        @SubscribeEvent
        public static void onPlayerSelectItem (LivingEquipmentChangeEvent event) {
            if (event.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntity();
                ItemStack stack = player.getItemBySlot(event.getSlot());
                CompoundNBT tag = stack.getTag();
                if (stack.getItem() instanceof EnergyItem && !tag.getBoolean("shouldNotFillEnergy")) {
                    tag.putFloat("energy", 100);
                    tag.putBoolean("shouldNotFillEnergy", true);
                }
            }
        }
    }
}
