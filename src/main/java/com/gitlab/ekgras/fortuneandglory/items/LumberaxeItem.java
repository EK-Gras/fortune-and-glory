package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.items.util.ToolHarvestLogic;
import com.gitlab.ekgras.fortuneandglory.items.util.TreeAOEHarvestLogic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class LumberaxeItem extends AOEToolItem implements IToolEffectivenessSets {

    private static final TreeAOEHarvestLogic HARVEST_LOGIC = new TreeAOEHarvestLogic(0, 0, 5);
    private final float speed;
    private static final Set<Material> DIGGABLE_MATERIALS = IToolEffectivenessSets.AXE;
    private static final Set<Block> DIGGABLE_BLOCKS = IToolEffectivenessSets.AXE_OTHER;

    public LumberaxeItem(Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                         List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, tier, attackDamageBaseLine, attackSpeed, destroySpeed, ToolType.AXE,
                defaultEnchantments, variantEnchantments);
        this.speed = destroySpeed;
    }

    @Override
    public ToolHarvestLogic getToolHarvestLogic() {
        return HARVEST_LOGIC;
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return (DIGGABLE_MATERIALS.contains(material) || DIGGABLE_BLOCKS.contains(state.getBlock())) ? this.speed : super.getDestroySpeed(stack, state);
    }

    public ActionResultType useOn(ItemUseContext context) {
        World world = context.getLevel();
        BlockPos blockpos = context.getClickedPos();
        BlockState blockstate = world.getBlockState(blockpos);
        BlockState block = blockstate.getToolModifiedState(world, blockpos, context.getPlayer(), context.getItemInHand(), net.minecraftforge.common.ToolType.AXE);
        if (block != null) {
            PlayerEntity playerentity = context.getPlayer();
            world.playSound(playerentity, blockpos, SoundEvents.AXE_STRIP, SoundCategory.BLOCKS, 1.0F, 1.0F);
            if (!world.isClientSide) {
                world.setBlock(blockpos, block, 11);
                if (playerentity != null) {
                    context.getItemInHand().hurtAndBreak(1, playerentity, (entity) -> {
                        entity.broadcastBreakEvent(context.getHand());
                    });

                }
            }

            return ActionResultType.sidedSuccess(world.isClientSide);
        } else {
            return ActionResultType.PASS;
        }
    }
}
