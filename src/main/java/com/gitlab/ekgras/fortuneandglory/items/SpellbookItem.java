package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.EnergyBallEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class SpellbookItem extends EnergyItem {

    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // Amount of damage the spellbook deals
    private final double damage;

    // Speed of the fired projectile (blocks/tick)
    private final float velocity;

    // The level of inaccuracy of the fired projectile
    private final float inaccuracy;

    // Various properties for the fired Energy Ball projectile. These are rather self-explanatory.
    private final boolean noGravity;
    private final boolean constantMomentum;
    private final int life;
    private final BasicParticleType particle;
    private final boolean invisible;


    public SpellbookItem(Properties properties, int fireSpeedIn, float usageRateIn, float rechargeRateIn, int rechargeCooldownIn,
                         double damage, float inaccuracy, float velocity, boolean noGravity, boolean constantMomentum, int life, BasicParticleType particle, boolean invisible,
                         List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, fireSpeedIn, usageRateIn, rechargeRateIn, rechargeCooldownIn, defaultEnchantments, variantEnchantments);
        this.fireSpeed = fireSpeedIn;
        this.usageRate = usageRateIn;
        this.rechargeRate = rechargeRateIn;
        this.rechargeCooldown = rechargeCooldownIn;
        this.damage = damage;
        this.velocity = velocity;
        this.inaccuracy = inaccuracy;
        this.noGravity = noGravity;
        this.constantMomentum = constantMomentum;
        this.life = life;
        this.particle = particle;
        this.invisible = invisible;
    }

    public void releaseUsing(ItemStack stack, World world, LivingEntity entity, int timeLeft) {
        if (entity instanceof PlayerEntity && this.getUseDuration(stack) - timeLeft >= 20 && stack.getItem() instanceof SpellbookItem) {
            CompoundNBT tag = stack.getTag();
            PlayerEntity player = (PlayerEntity) entity;
            Float energy = tag.getFloat("energy");
            tag.putFloat("energy", energy - this.usageRate);
            player.getCooldowns().addCooldown(this, this.fireSpeed);
            tag.putInt("rechargeCooldown", this.rechargeCooldown);
            stack.hurtAndBreak(1, player, (lHand) -> {
                lHand.broadcastBreakEvent(player.getUsedItemHand());
            });
            EnergyBallEntity ball = new EnergyBallEntity(FnGEntities.ENERGY_BALL.get(), player, player.getCommandSenderWorld(), noGravity, false, life, particle, invisible, true);
            ball.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
            ball.shoot(player, player.xRot, player.yRot, velocity, inaccuracy, damage, SoundEvents.SHULKER_BULLET_HIT);
            if (ball.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS)) ball.setSecondsOnFire(100);
            world.addFreshEntity(ball);
            if (ball.getItemEnchantments().containsKey(Enchantments.MULTISHOT)) {
                for (int i = 0; i < 2; i++){
                    EnergyBallEntity mBall = new EnergyBallEntity(FnGEntities.ENERGY_BALL.get(), player, player.getCommandSenderWorld(), noGravity, false, life, particle, invisible, true);
                    mBall.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                    Vector3d vector3d1 = player.getUpVector(1.0F);
                    Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i == 0 ? 10F : -10F, true);
                    Vector3d vector3d = player.getViewVector(1.0F);
                    Vector3f vector3f = new Vector3f(vector3d);
                    vector3f.transform(quaternion);
                    mBall.shoot(vector3f.x(), vector3f.y(), vector3f.z(), velocity, inaccuracy, damage, SoundEvents.SHULKER_BULLET_HIT);
                    Vector3d playerVector = player.getDeltaMovement();
                    mBall.setDeltaMovement(mBall.getDeltaMovement().add(playerVector.x, player.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
                    if (mBall.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                        mBall.setSecondsOnFire(100);
                    world.addFreshEntity(mBall);
                }
            }
        }
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        CompoundNBT tag = stack.getTag();
        if (tag.getFloat("energy") > usageRate) {
            player.startUsingItem(hand);
            return ActionResult.consume(stack);
        }
        else {
            return ActionResult.fail(stack);
        }
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 72000;
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.BOW;
    }

}
