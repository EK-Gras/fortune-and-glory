package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.enums.UsageType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.lwjgl.system.CallbackI;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;

public class AbilityBadgeItem extends BadgeItem {

    private final Ability ability;
    private final int cooldown;
    private final UsageType usageType;

    public AbilityBadgeItem(Item.Properties properties, int size, UsageType usageType, Ability ability) {
        this(properties, size, usageType, ability, 0);
    }

    public AbilityBadgeItem(Item.Properties properties, int size, UsageType usageType,  Ability ability, int cooldown) {
        super(properties, size, usageType);
        this.usageType = usageType;
        this.ability = ability;
        this.cooldown = cooldown;
    }

    @Override
    public boolean hasKeyBind() {
        return true;
    }

    @Override
    public void useBadge(PlayerEntity player, ItemStack stack) {
        if (!player.getCooldowns().isOnCooldown(this)) {
            if (this.usageType == UsageType.USE) {
                stack.hurtAndBreak(1, player, (entity) -> {
                        entity.broadcastBreakEvent(player.getUsedItemHand());
                    }
                );
            }
            ability.trigger(player);
            player.getCooldowns().addCooldown(this, cooldown);
        }
    }
}
