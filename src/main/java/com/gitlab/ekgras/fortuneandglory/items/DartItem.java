package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.DartEntity;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.EnergyBallEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.Effect;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;

public class DartItem extends EnergyItem {

    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // The type, strength, and duration of the effect that the dart
    private final Effect effect;
    private final int strength;
    private final int duration;

    // Speed of the fired projectile (blocks/tick)
    private final float velocity;

    // The level of inaccuracy of the fired projectile
    private final float inaccuracy;

    public DartItem(Properties properties, int fireSpeedIn, float usageRateIn, float rechargeRateIn, int rechargeCooldownIn,
                    int velocityIn, int inaccuracyIn, Effect effectIn, int strengthIn, int durationIn,
                    List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, fireSpeedIn, usageRateIn, rechargeRateIn, rechargeCooldownIn, defaultEnchantments, variantEnchantments);
        this.fireSpeed = fireSpeedIn;
        this.usageRate = usageRateIn;
        this.rechargeRate = rechargeRateIn;
        this.rechargeCooldown = rechargeCooldownIn;
        this.velocity = velocityIn;
        this.inaccuracy = inaccuracyIn;
        this.effect = effectIn;
        this.strength = strengthIn;
        this.duration = durationIn;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (player.getItemInHand(hand).getItem() instanceof DartItem) {
            ItemStack stack = player.getItemInHand(hand);
            CompoundNBT tag = stack.getTag();
            float energy = tag.getFloat("energy");
            if (energy >= this.usageRate) {
                tag.putFloat("energy", energy - this.usageRate);
                player.getCooldowns().addCooldown(this, this.fireSpeed);
                tag.putInt("rechargeCooldown", this.rechargeCooldown);
                stack.hurtAndBreak(1, player, (lHand) -> {
                    lHand.broadcastBreakEvent(player.getUsedItemHand());
                });
                DartEntity dart = new DartEntity(player, world, effect, strength, duration, stack);
                dart.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                dart.shoot(player, player.xRot, player.yRot, velocity, inaccuracy);
                if (dart.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS)) dart.setSecondsOnFire(100);
                world.addFreshEntity(dart);
                if (dart.getItemEnchantments().containsKey(Enchantments.MULTISHOT)) {
                    for (int i = 0; i < 2; i++) {
                        DartEntity mDart = new DartEntity(player, world, effect, strength, duration, stack);
                        mDart.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                        Vector3d vector3d1 = player.getUpVector(1.0F);
                        Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i == 0 ? 10F : -10F, true);
                        Vector3d vector3d = player.getViewVector(1.0F);
                        Vector3f vector3f = new Vector3f(vector3d);
                        vector3f.transform(quaternion);
                        mDart.shoot(vector3f.x(), vector3f.y(), vector3f.z(), velocity, inaccuracy);
                        Vector3d playerVector = player.getDeltaMovement();
                        mDart.setDeltaMovement(mDart.getDeltaMovement().add(playerVector.x, player.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
                        if (mDart.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS)) mDart.setSecondsOnFire(100);
                        world.addFreshEntity(mDart);
                    }
                }
                return ActionResult.success(stack);
            }
        }
        return ActionResult.fail(player.getItemInHand(hand));
    }
}
