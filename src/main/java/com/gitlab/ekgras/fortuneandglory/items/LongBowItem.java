
package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BowArrowEntity;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.FnGArrowEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.*;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class LongBowItem extends FnGBowItem {

    public LongBowItem(Item.Properties builder, double damage,
                       List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder, damage, defaultEnchantments, variantEnchantments);
    }

    @Override
    public void fireArrow(World world, ItemStack stack, ItemStack ammo, PlayerEntity playerentity, boolean consumeAmmo, float power, int yawModifier) {
        if (!world.isClientSide) {
            ArrowItem arrowitem = (ArrowItem) (ammo.getItem() instanceof ArrowItem ? ammo.getItem() : Items.ARROW);
            AbstractArrowEntity arrowEntity = arrowitem.createArrow(world, ammo, playerentity);
            arrowEntity = customArrow(arrowEntity);

            if (arrowEntity instanceof ArrowEntity) {
                arrowEntity = BowArrowEntity.fromArrow((ArrowEntity) arrowEntity, stack);
            }

            if (arrowEntity instanceof BowArrowEntity) {
                ((BowArrowEntity) arrowEntity).setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
            }

            if (yawModifier == 0.0f) arrowEntity.shootFromRotation(playerentity, playerentity.xRot, playerentity.yRot, 0.0F, power * 3.0F, -1.0F * ((power - 5.F)/2.5F));
            else {
                Vector3d vector3d1 = playerentity.getUpVector(1.0F);
                Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), yawModifier, true);
                Vector3d vector3d = playerentity.getViewVector(1.0F);
                Vector3f vector3f = new Vector3f(vector3d);
                vector3f.transform(quaternion);
                arrowEntity.shoot(vector3f.x(), vector3f.y(), vector3f.z(), power * 3.0F, -1.0F * ((power - 5.F)/2.5F));
                Vector3d playerVector = playerentity.getDeltaMovement();
                arrowEntity.setDeltaMovement(arrowEntity.getDeltaMovement().add(playerVector.x, playerentity.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
            }

            if (power == 5.0F) {
                arrowEntity.setCritArrow(true);
            }

            //This was added for the longbow class. It sets the damage so that when velocity is factored into the damage of the arrow, it doesn't multiply to a ludicrous amount of damage.
            arrowEntity.setBaseDamage(0.4D);

            int j = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, stack);
            if (j > 0) {
                arrowEntity.setBaseDamage(arrowEntity.getBaseDamage() + (double) j * 0.5D + 0.5D);
            }

            int k = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, stack);
            if (k > 0) {
                arrowEntity.setKnockback(k);
            }

            if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, stack) > 0) {
                arrowEntity.setSecondsOnFire(100);
            }

            stack.hurtAndBreak(1, playerentity, (p_220009_1_) -> {
                p_220009_1_.broadcastBreakEvent(playerentity.getUsedItemHand());
            });
            if (consumeAmmo || playerentity.abilities.instabuild && (ammo.getItem() == Items.SPECTRAL_ARROW || ammo.getItem() == Items.TIPPED_ARROW)) {
                arrowEntity.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
            }

            world.addFreshEntity(arrowEntity);
        }

        world.playSound((PlayerEntity) null, playerentity.getX(), playerentity.getY(), playerentity.getZ(), SoundEvents.ARROW_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.0F / (random.nextFloat() * 0.4F + 1.2F) + power * 0.5F);
    }

    public float getArrowVelocity(int useTime) {
        float f = (float) useTime / 20.0F;
        f = (0.01F * (float) Math.pow(500, f / 1.5F));
        if (f > 5.0F) {
            f = 5.0F;
        }

        return f;
    }

}


