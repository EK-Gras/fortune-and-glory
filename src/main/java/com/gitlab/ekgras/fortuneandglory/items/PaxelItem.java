package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class PaxelItem extends ToolItem implements IToolEffectivenessSets, IDefaultEnchantmentData {

    private final float speed;

    private static final Set<Block> DIGGABLE_BLOCKS = ImmutableSet.<Block>builder().addAll(PICKAXE).addAll(SHOVEL).addAll(AXE_OTHER).addAll(HOE).build();
    private static final Set<Material> DIGGABLE_MATERIALS = IToolEffectivenessSets.AXE;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public PaxelItem(Item.Properties builder, IItemTier tier, int attackDamageIn, float attackSpeedIn, float destroySpeed,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super((float)attackDamageIn, attackSpeedIn, tier, DIGGABLE_BLOCKS, builder.addToolType(net.minecraftforge.common.ToolType.PICKAXE, tier.getLevel()));
        this.speed = destroySpeed;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    /**
     * Check whether this Item can harvest the given Block
     */
    public boolean isCorrectToolForDrops(BlockState blockIn) {
        Block block = blockIn.getBlock();
        int i = this.getTier().getLevel();
        if (blockIn.getHarvestTool() == net.minecraftforge.common.ToolType.PICKAXE) {
            return i >= blockIn.getHarvestLevel();
        }
        Material material = blockIn.getMaterial();
        return material == Material.STONE || material == Material.METAL || material == Material.HEAVY_METAL || material == Material.WOOD || material == Material.PLANT || material == Material.REPLACEABLE_PLANT || material == Material.BAMBOO ;
    }

    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return (DIGGABLE_MATERIALS.contains(material) || DIGGABLE_BLOCKS.contains(state.getBlock())) ? this.speed : super.getDestroySpeed(stack, state);
    }
}
