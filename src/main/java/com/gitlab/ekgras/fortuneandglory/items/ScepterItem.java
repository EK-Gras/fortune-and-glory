package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;

public class ScepterItem extends EnergyItem {
    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // Parameters for the sound the staff should make when used
    private final SoundEvent sound;
    private final float volume;
    private final float pitch;

    // Maximum limit of minions that can be summoned
    private final int summoningLimit;

    // Number of ticks each minion lives for
    private final int life;

    public ScepterItem(Properties properties, int fireSpeed, float usageRate, float rechargeRate, int rechargeCooldown,
                       SoundEvent sound, float volume, float pitch, int summoningLimit, int life,
                       List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, fireSpeed, usageRate, rechargeRate, rechargeCooldown, defaultEnchantments, variantEnchantments);
        this.fireSpeed = fireSpeed;
        this.usageRate = usageRate;
        this.rechargeRate = rechargeRate;
        this.rechargeCooldown = rechargeCooldown;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.summoningLimit = summoningLimit;
        this.life = life;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        return ActionResult.fail(player.getItemInHand(hand));
    }

    public ActionResultType useOn(ItemUseContext context) {
        PlayerEntity player = context.getPlayer();
        Hand hand = context.getHand();
        ItemStack stack = player.getItemInHand(hand);
        // The world must be server-side because only ServerWorld has a getEntities function including all entities
        if (!context.getLevel().isClientSide && player.getItemInHand(hand).getItem() instanceof ScepterItem) {
            CompoundNBT tag = stack.getTag();
            float energy = tag.getFloat("energy");
            ServerWorld world = (ServerWorld) context.getLevel();
            List<Entity> entityList = world.getEntities(FnGEntities.MINION.get(), (entity) -> {return true;});
            int minionsSummoned = 0;
            for (Entity entity : entityList) {
                if (entity instanceof MinionEntity) {
                    MinionEntity minion = (MinionEntity) entity;
                    if (minion.getOwner() == player.getUUID()) {
                        minionsSummoned++;
                    }
                }
            }
            if (energy >= this.usageRate && minionsSummoned < summoningLimit) {
                BlockPos summonPos = context.getClickedPos().relative(context.getClickedFace());
                MinionEntity entity = new MinionEntity(world, player.getUUID(), player, stack, life);
                entity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                entity.setPos(summonPos.getX() + 0.5f, summonPos.getY(), summonPos.getZ() + 0.5f);
                world.sendParticles(ParticleTypes.POOF, entity.getRandomX(1.0D), entity.getRandomY(), entity.getRandomZ(1.0D),10,
                        entity.getRandom().nextGaussian() * 0.02D, entity.getRandom().nextGaussian() * 0.02D, entity.getRandom().nextGaussian() * 0.02D,0.02d);
                world.addFreshEntity(entity);
                tag.putFloat("energy", energy - this.usageRate);
                player.getCooldowns().addCooldown(this, this.fireSpeed);
                tag.putInt("rechargeCooldown", this.rechargeCooldown);
                stack.hurtAndBreak(1, player, (lHand) -> {
                    lHand.broadcastBreakEvent(player.getUsedItemHand());
                });
                return ActionResultType.SUCCESS;
            } else {
                return ActionResultType.FAIL;
            }
        }
        return ActionResultType.PASS;
    }

    public ActionResultType interactLivingEntity(ItemStack stack, PlayerEntity player, LivingEntity target, Hand hand) {
        if (!player.getCommandSenderWorld().isClientSide && player.getItemInHand(hand).getItem() instanceof ScepterItem) {
            ServerWorld world = (ServerWorld) player.getCommandSenderWorld();
            List<Entity> entityList = world.getEntities(FnGEntities.MINION.get(), (entity) -> {return true;});
            if (target instanceof MinionEntity) {
                if (((MinionEntity) target).getOwner() != player.getUUID()) {
                    double d0 = target.getRandom().nextGaussian() * 0.02D;
                    double d1 = target.getRandom().nextGaussian() * 0.02D;
                    double d2 = target.getRandom().nextGaussian() * 0.02D;
                    for (int i = 10; i > 0; i--) {
                        world.sendParticles(ParticleTypes.ANGRY_VILLAGER, target.getRandomX(1.0D), target.getRandomY(), target.getRandomZ(1.0D), 1, d0, d1, d2, 0.02d);
                    }
                    for (Entity entity : entityList) {
                        if (entity instanceof MinionEntity) {
                            MinionEntity minion = (MinionEntity) entity;
                            if (minion.getOwner() == player.getUUID()) {
                                minion.setTarget(target);
                            }
                        }
                    }
                    return ActionResultType.SUCCESS;
                }
                return ActionResultType.FAIL;
            }
            else {
                double d0 = target.getRandom().nextGaussian() * 0.02D;
                double d1 = target.getRandom().nextGaussian() * 0.02D;
                double d2 = target.getRandom().nextGaussian() * 0.02D;
                for (int i = 5; i > 0; i--) {
                    world.sendParticles(ParticleTypes.ANGRY_VILLAGER, target.getRandomX(1.0D), target.getRandomY(), target.getRandomZ(1.0D), 1, d0, d1, d2, 0.02d);
                }
                for (Entity entity : entityList) {
                    if (entity instanceof MinionEntity) {
                        MinionEntity minion = (MinionEntity) entity;
                        if (minion.getOwner() == player.getUUID()) {
                            minion.setTarget(target);
                        }
                    }
                }
                return ActionResultType.SUCCESS;
            }
        }
        return ActionResultType.PASS;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class ScepterItemHandler {

        @SubscribeEvent
        public static void onPlayerSelectItem (LivingEquipmentChangeEvent event) {
            if (event.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntity();
                ItemStack from = event.getFrom();
                ItemStack to = event.getTo();
                // LivingEquipmentChangeEvent only fires on the server, so it is safe to cast World to ServerWorld
                ServerWorld world = (ServerWorld) player.getCommandSenderWorld();
                EquipmentSlotType slot = event.getSlot();
                if (from.getItem() != event.getTo().getItem() && from.getItem() instanceof ScepterItem && !player.isHolding(from.getItem())) {
                    List<Entity> entityList = world.getEntities(FnGEntities.MINION.get(), (entity) -> {
                        return true;
                    });
                    for (Entity entity : entityList) {
                        if (entity instanceof MinionEntity) {
                            MinionEntity minion = (MinionEntity) entity;
                            if (minion.getOwner() == player.getUUID()) {
                                minion.remove();
                            }
                        }
                    }
                }
            }
        }
    }
}
