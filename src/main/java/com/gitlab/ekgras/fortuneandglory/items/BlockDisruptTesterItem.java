package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.util.FallingBlockUtils;
import net.minecraft.item.Item;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

public class BlockDisruptTesterItem extends Item {

    public BlockDisruptTesterItem(Item.Properties properties) {
        super(properties);
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class DisruptTesterHandler {
        @SubscribeEvent
        public static void onPlayerRightClickBlock(PlayerInteractEvent.RightClickBlock event) {
            if (!event.getWorld().isClientSide &&
                    event.getPlayer().getItemInHand(Hand.MAIN_HAND).getItem() instanceof BlockDisruptTesterItem) {
                BlockPos pos = event.getHitVec().getBlockPos();
                ServerWorld world = (ServerWorld) event.getWorld();
                FallingBlockUtils.disruptCascade(world, pos, 5);
            }
        }
    }
}
