package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.EnergyBallEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;
import net.minecraftforge.event.world.NoteBlockEvent;

import java.util.List;
import java.util.Map;

public class FlamethrowerItem extends EnergyItem {

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // Speed of the fired projectile (blocks/tick)
    private final float velocity;

    // The level of inaccuracy of the fired projectile
    private final float inaccuracy;

    // Amount of damage the wand deals
    private final double damage;

    // Number of projectiles fired
    private final int shots;

    public FlamethrowerItem(Properties properties, float usageRateIn, float rechargeRateIn, int rechargeCooldownIn,
                            double damageIn, float inaccuracyIn, float velocityIn, int shotsIn,
                            List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, 0, usageRateIn, rechargeRateIn, rechargeCooldownIn, defaultEnchantments, variantEnchantments);
        this.usageRate = usageRateIn;
        this.rechargeRate = rechargeRateIn;
        this.rechargeCooldown = rechargeCooldownIn;
        this.damage = damageIn;
        this.inaccuracy = inaccuracyIn;
        this.velocity = velocityIn;
        this.shots = shotsIn;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        if (stack.getItem() instanceof FlamethrowerItem) {
            CompoundNBT tag = stack.getTag();
            float energy = tag.getFloat("energy");
            if (energy >= this.usageRate) {
                player.startUsingItem(hand);
                return ActionResult.pass(stack);
            }
        }
        return ActionResult.fail(stack);
    }

    public void onUseTick(World world, LivingEntity entity, ItemStack stack, int useTime) {
        CompoundNBT tag = stack.getTag();
        float energy = tag.getFloat("energy");
        if (entity instanceof PlayerEntity && energy >= this.usageRate) {
            PlayerEntity player = (PlayerEntity) entity;
            tag.putFloat("energy", energy - this.usageRate);
            tag.putInt("rechargeCooldown", this.rechargeCooldown);
            if (tag.getInt("durabilityTimer") <= 0) {
                stack.hurtAndBreak(1, player, (lHand) -> {
                    lHand.broadcastBreakEvent(player.getUsedItemHand());
                });
                tag.putInt("durabilityTimer", 20);
            }
            tag.putInt("durabilityTimer", tag.getInt("durabilityTimer") - 1);
            for (int i = shots; i > 0; i--) {
                EnergyBallEntity ball = new EnergyBallEntity(FnGEntities.ENERGY_BALL.get(), player, player.getCommandSenderWorld(), true, false, 60, ParticleTypes.FLAME, true, false);
                ball.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                ball.shoot(player, player.xRot, player.yRot, velocity, inaccuracy, damage, SoundEvents.FIRE_AMBIENT);
                ball.setShouldKnockback(false);
                if (ball.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                    ball.setSecondsOnFire(100);
                world.addFreshEntity(ball);
                if (ball.getItemEnchantments().containsKey(Enchantments.MULTISHOT)) {
                    for (int i1 = 0; i1 < 2; i1++) {
                        EnergyBallEntity mBall = new EnergyBallEntity(FnGEntities.ENERGY_BALL.get(), player, player.getCommandSenderWorld(), true, false, 60, ParticleTypes.FLAME, true, false);
                        mBall.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                        Vector3d vector3d1 = player.getUpVector(1.0F);
                        Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i == 0 ? 10F : -10F, true);
                        Vector3d vector3d = player.getViewVector(1.0F);
                        Vector3f vector3f = new Vector3f(vector3d);
                        vector3f.transform(quaternion);
                        mBall.shoot(vector3f.x(), vector3f.y(), vector3f.z(), velocity, inaccuracy, damage, SoundEvents.SHULKER_BULLET_HIT);
                        Vector3d playerVector = player.getDeltaMovement();
                        mBall.setDeltaMovement(mBall.getDeltaMovement().add(playerVector.x, player.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
                        ball.setShouldKnockback(false);
                        if (mBall.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                            mBall.setSecondsOnFire(100);
                        world.addFreshEntity(mBall);
                    }
                }
            }
        }
        else {
            entity.stopUsingItem();
        }
    }

    public int getUseDuration(ItemStack stack) {
        return 72000;
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.BOW;
    }
}
