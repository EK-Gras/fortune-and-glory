package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;

public class AbilityTesterItem extends Item {

    private final Ability ability;

    public AbilityTesterItem(Item.Properties builder, Ability abilityIn) {
        super(builder);
        this.ability = abilityIn;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player , Hand hand) {
        if (player.getItemInHand(hand).getItem() instanceof AbilityTesterItem) {
            ability.trigger(player);
            return ActionResult.success(player.getItemInHand(hand));
        }
        return ActionResult.fail(player.getItemInHand(hand));
    }
}
