package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.AxeItem;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class FnGAxeItem extends AxeItem implements IToolEffectivenessSets, IDefaultEnchantmentData {

    private final float speed;
    // Why the hell does Mojang use a set of Materials (and not blocks) for the Axe and NONE OF THE OTHER TOOLS????
    private static final Set<Material> DIGGABLE_MATERIALS = IToolEffectivenessSets.AXE;
    private static final Set<Block> DIGGABLE_BLOCKS = IToolEffectivenessSets.AXE_OTHER;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public FnGAxeItem(Item.Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(tier, attackDamageBaseLine, attackSpeed, properties);
        this.speed = destroySpeed;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return (DIGGABLE_MATERIALS.contains(material) || DIGGABLE_BLOCKS.contains(state.getBlock())) ? this.speed : super.getDestroySpeed(stack, state);
    }
}
