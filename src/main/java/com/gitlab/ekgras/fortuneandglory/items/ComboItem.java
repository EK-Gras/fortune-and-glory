package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;

import static java.lang.String.valueOf;

public class ComboItem extends MeleeItem implements IDefaultEnchantmentData {
    private final float attackDamage;
    private final float attackSpeed;

    // Maximum number that the combo can increase to.
    private final int maxCombo;

    // Amount of time (in ticks) until the combo wears off.
    private final int comboTime;

    // Amount of additional damage that each hit deals.
    private final int comboDamage;

    public ComboItem(Item.Properties builder, IItemTier tier, int attackDamageIn, float attackSpeedIn, int maximumCombo, int time, int cDamage,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder, tier, attackDamageIn, attackSpeedIn, defaultEnchantments, variantEnchantments);
        this.attackSpeed = attackSpeedIn;
        this.attackDamage = (float)attackDamageIn + tier.getAttackDamageBonus();
        this.maxCombo = maximumCombo;
        this.comboTime = time;
        this.comboDamage = cDamage;
    }

    public int getMaxCombo() {
        return this.maxCombo;
    }

    public int getComboTime() {
        return this.comboTime;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class ComboItemHandler {

        @SubscribeEvent
        public static void onLivingHurt(LivingHurtEvent event) {

            //If the source of the damage is a Living Entity
            if (event.getSource().getDirectEntity() instanceof LivingEntity) {

                //Gets the entity who attacked.
                LivingEntity source = (LivingEntity) event.getSource().getEntity();

                //Get the held item of the source
                ItemStack stack = source.getItemBySlot(EquipmentSlotType.MAINHAND);

                //If the entity is holding a Combo-type item
                if (stack.getItem() instanceof ComboItem) {

                    //Get the item being used. This is used to get the attack damage and max combo of the item later.
                    ComboItem ci = (ComboItem) stack.getItem();

                    //Get the NBT of the item
                    CompoundNBT tag = stack.getTag();

                    //Give currentCombo a value if it doesn't already have one. This is to prevent a possible NullPointerException when using getInt later on
                    if (tag.get("currentCombo") == null) {
                        tag.putInt("currentCombo", 0);
                    }

                    //If the entity did not attack before attack cooldown wore off (their attack dealt at least the full attack damage of the item)...
                    if (event.getAmount() >= source.getAttributeValue(Attributes.ATTACK_DAMAGE) && tag.getInt("currentCombo") <= ci.getMaxCombo()) {
                        //If the entity has not finished their combo...
                        if (tag.getInt("currentCombo") <= ci.getMaxCombo()) {
                            //Increase the damage of the attack by currentCombo * comboDamage and increment the currentCombo
                            event.setAmount(event.getAmount() + (tag.getInt("currentCombo") * ci.comboDamage));
                            if (tag.getInt("currentCombo") < ci.maxCombo) tag.putInt("currentCombo", tag.getInt("currentCombo") + 1);
                            else tag.putInt("currentCombo", ci.maxCombo);
                        }
                    }
                    //Store the time that the entity attacked.
                    tag.putInt("lastHit", (int) System.currentTimeMillis());
                }
            }
        }

        @SubscribeEvent
        public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
            PlayerEntity player = event.player;
            ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            // If the player is holding a ComboItem...
            if (stack.getItem() instanceof ComboItem) {
                ComboItem ci = (ComboItem) stack.getItem();
                CompoundNBT tag = stack.getTag();

                // Check if the time passed since the last hit is higher than the comboTime of the item.
                if ((int) System.currentTimeMillis() - tag.getInt("lastHit") > ci.comboTime) {
                    //If it was, set the combo back to 0.
                    tag.putInt("currentCombo", 0);
                }
            }
        }
    }
}