package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.enums.UsageType;
import com.gitlab.ekgras.fortuneandglory.network.FnGPacketHandler;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.items.IItemHandlerModifiable;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurioItem;
import top.theillusivec4.curios.api.type.capability.ICuriosItemHandler;
import top.theillusivec4.curios.api.type.inventory.IDynamicStackHandler;

import java.util.HashMap;
import java.util.UUID;

public class BadgeItem extends Item implements ICurioItem {

    private final int size;
    private final UsageType usageType;

    // Attribute Modifiers

    private final UUID[] attributeUUIDs = {UUID.fromString("3a0f87dd-1b35-4ef3-9823-7852208d8ed0"), UUID.fromString("e40cb0bf-7ae0-4dcf-9e4d-4b5f30530f28"),
            UUID.fromString("f2e932b8-c162-401c-9b6b-471d20742575"), UUID.fromString("2c2f34e3-06dd-4271-9b6e-654500bfb8f5"),
            UUID.fromString("9a048dc4-ed1a-4137-8cb4-c1e753f0ab7c"), UUID.fromString("195c66d1-6c4e-41ee-a1e9-03befa8c0104"),
            UUID.fromString("bf2b31ae-04fb-41d1-8812-895286f4a24f"), UUID.fromString("573d290e-4432-45bf-ad23-045a2e99b08e")
    };
    private final Multimap<Attribute, AttributeModifier> modifiers;

    public BadgeItem(Item.Properties properties, int size, UsageType usageType) {
        super(properties);
        this.size = size;
        this.modifiers = ImmutableMultimap.of();
        this.usageType = usageType;
    }

    public BadgeItem(Item.Properties properties, int size, UsageType usageType,
                     double maxHealth, double knockbackResistance, double movementSpeed,
                     double attackKnockback, double attackSpeed, double armor, double armorToughness, double luck) {
        super(properties);
        this.size = size;
        this.usageType = usageType;
        ImmutableMultimap.Builder<Attribute, AttributeModifier> mainBuilder = ImmutableMultimap.builder();
        mainBuilder.put(Attributes.MAX_HEALTH, new AttributeModifier(attributeUUIDs[0], "Badge modifier", maxHealth, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(attributeUUIDs[1], "Badge modifier", knockbackResistance, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.MOVEMENT_SPEED, new AttributeModifier(attributeUUIDs[2], "Badge modifier", movementSpeed, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ATTACK_KNOCKBACK, new AttributeModifier(attributeUUIDs[3], "Badge modifier", attackKnockback, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ATTACK_SPEED, new AttributeModifier(attributeUUIDs[4], "Badge modifier", attackSpeed, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ARMOR, new AttributeModifier(attributeUUIDs[5], "Badge modifier", armor, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(attributeUUIDs[6], "Badge modifier", armorToughness, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.LUCK, new AttributeModifier(attributeUUIDs[7], "Badge modifier", luck, AttributeModifier.Operation.ADDITION));
        this.modifiers = mainBuilder.build();
    }

    public BadgeItem(Item.Properties properties, int size, Attribute attribute, double value, UsageType usageType) {
        super(properties);
        this.size = size;
        this.usageType = usageType;
        ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
        builder.put(attribute, new AttributeModifier(attributeUUIDs[0], "Badge modifier", value, AttributeModifier.Operation.ADDITION));
        this.modifiers = builder.build();

    }

    public boolean hasKeyBind() {
        return false;
    }

    public void useBadge(PlayerEntity player, ItemStack stack) {
        if (this.usageType == UsageType.USE) {
            stack.hurtAndBreak(1, player, (entity) -> {
                        entity.broadcastBreakEvent(player.getUsedItemHand());
                    }
            );
        }
    }

    public void curioTick(String identifier, int index, LivingEntity livingEntity, ItemStack stack) {
        if (identifier.matches("badge")) {
            if (stack.getItem() instanceof BadgeItem) {
                BadgeItem item = (BadgeItem) stack.getItem();
                if (item.usageType == UsageType.TICK) {
                    CompoundNBT tag = stack.getTag();
                    tag.putInt("useTime", tag.getInt("useTime") - 1);
                    int useTime = tag.getInt("useTime");
                    if (useTime <= 0) {
                        tag.putInt("useTime", 20);
                        stack.hurtAndBreak(1, livingEntity, (entity) -> {
                                    entity.broadcastBreakEvent(livingEntity.getUsedItemHand());
                                }
                        );
                    }
                }
            }
        }
    }

    public void onEquip(SlotContext slotContext, ItemStack prevStack, ItemStack stack) {
        LivingEntity entity = slotContext.getWearer();
        if (stack.getItem() instanceof BadgeItem) {
            stack.getTag().putBoolean("equipped", true);
            BadgeItem item = (BadgeItem) stack.getItem();
            CuriosApi.getSlotHelper().shrinkSlotType("badge", item.size - 1, entity);
        }

    }

    public void onUnequip(SlotContext slotContext, ItemStack newStack, ItemStack stack) {
        LivingEntity entity = slotContext.getWearer();
        if (stack.getItem() instanceof BadgeItem && stack.getTag().getBoolean("equipped")) {
            stack.getTag().putBoolean("equipped", false);
            BadgeItem item = (BadgeItem) stack.getItem();
            CuriosApi.getSlotHelper().growSlotType("badge", item.size - 1, entity);
        }
    }

    public boolean canEquip(String identifier, LivingEntity entity, ItemStack stack) {
        if (identifier.matches("badge") && stack.getItem() instanceof BadgeItem) {
            IDynamicStackHandler badgeHandler = CuriosApi.getCuriosHelper().getCuriosHandler(entity).orElseThrow(NullPointerException::new).getCurios().get("badge").getStacks();
            BadgeItem item = (BadgeItem) stack.getItem();
            int slotsAvailable = badgeHandler.getSlots();
            for (int i = 0; i < badgeHandler.getSlots(); i++) {
                if (badgeHandler.getStackInSlot(i) != ItemStack.EMPTY) slotsAvailable--;
            }
            return item.size <= slotsAvailable;
        }
        return ICurioItem.defaultInstance.canEquip(identifier, entity);
    }

    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(SlotContext slotContext, UUID uuid, ItemStack stack) {
        return this.modifiers;
    }

    public int getSize() {
        return this.size;
    }
}
