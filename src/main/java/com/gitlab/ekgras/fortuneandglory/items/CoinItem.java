package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeItem;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CoinItem extends Item implements IForgeItem, IDefaultEnchantmentData {

    private final UUID[] mainHandUUIDs = {UUID.fromString("17b48357-7bd9-416c-b44d-e9e084a34846"), UUID.fromString("01b14c1f-21af-49d7-a743-13aae80b52dc"),
            UUID.fromString("aef017b7-4140-4a4f-9c82-f4df25ccbb11"), UUID.fromString("cd730c70-3cc5-4200-98d5-81dceabfe868"),
            UUID.fromString("a4fed275-2dae-401b-8eab-d0476e1b6181"), UUID.fromString("f64f1eaa-cc1e-4e96-b779-166d3250074b"),
            UUID.fromString("d772d755-4a23-4fed-8e8b-89b6cc596549"), UUID.fromString("5aa22db8-35da-4665-a8c4-3e2979c845a5")
    };
    private final UUID[] offHandUUIDs = {UUID.fromString("6d6770ef-3fc2-48f2-8b65-28947e18319c"), UUID.fromString("3ef65898-3efe-4d3a-ae9e-ba20eda36b8c"),
            UUID.fromString("4873070b-c82e-4abe-9ec3-6d9491edd6b4"), UUID.fromString("a3ca2b0b-4159-4706-a0a0-027a7d85a8b1"),
            UUID.fromString("b1130c4b-2ddd-4148-982f-0587d179142a"), UUID.fromString("7ce49a9-348d-4586-be23-e5ac78b269d5"),
            UUID.fromString("b066bcf4-81ad-4631-8662-1821cd036d20"), UUID.fromString("53456360-9e39-4554-8b9a-c2b0f8d4936d")};
    private final Multimap<Attribute, AttributeModifier> mainHandModifiers;
    private final Multimap<Attribute, AttributeModifier> offHandModifiers;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public CoinItem(Item.Properties properties, double maxHealth, double knockbackResistance, double movementSpeed,
                     double attackKnockback, double attackSpeed, double armor, double armorToughness, double luck,
                    List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);

        ImmutableMultimap.Builder<Attribute, AttributeModifier> mainBuilder = ImmutableMultimap.builder();
        mainBuilder.put(Attributes.MAX_HEALTH, new AttributeModifier(mainHandUUIDs[0], "Coin modifier", maxHealth, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(mainHandUUIDs[1], "Coin modifier", knockbackResistance, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.MOVEMENT_SPEED, new AttributeModifier(mainHandUUIDs[2], "Coin modifier", movementSpeed, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ATTACK_KNOCKBACK, new AttributeModifier(mainHandUUIDs[3], "Coin modifier", attackKnockback, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ATTACK_SPEED, new AttributeModifier(mainHandUUIDs[4], "Coin modifier", attackSpeed, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ARMOR, new AttributeModifier(mainHandUUIDs[5], "Coin modifier", armor, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(mainHandUUIDs[6], "Coin modifier", armorToughness, AttributeModifier.Operation.ADDITION));
        mainBuilder.put(Attributes.LUCK, new AttributeModifier(mainHandUUIDs[7], "Coin modifier", luck, AttributeModifier.Operation.ADDITION));
        this.mainHandModifiers = mainBuilder.build();

        ImmutableMultimap.Builder<Attribute, AttributeModifier> offBuilder = ImmutableMultimap.builder();
        offBuilder.put(Attributes.MAX_HEALTH, new AttributeModifier(offHandUUIDs[0], "Coin modifier", maxHealth, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(offHandUUIDs[1], "Coin modifier", knockbackResistance, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.MOVEMENT_SPEED, new AttributeModifier(offHandUUIDs[2], "Coin modifier", movementSpeed, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.ATTACK_KNOCKBACK, new AttributeModifier(offHandUUIDs[3], "Coin modifier", attackKnockback, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.ATTACK_SPEED, new AttributeModifier(offHandUUIDs[4], "Coin modifier", attackSpeed, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.ARMOR, new AttributeModifier(offHandUUIDs[5], "Coin modifier", armor, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(offHandUUIDs[6], "Coin modifier", armorToughness, AttributeModifier.Operation.ADDITION));
        offBuilder.put(Attributes.LUCK, new AttributeModifier(offHandUUIDs[7], "Coin modifier", luck, AttributeModifier.Operation.ADDITION));
        this.offHandModifiers = offBuilder.build();

        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public CoinItem(Item.Properties properties, Attribute attribute, double value,
                    List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);

        ImmutableMultimap.Builder<Attribute, AttributeModifier> mainBuilder = ImmutableMultimap.builder();
        mainBuilder.put(attribute, new AttributeModifier(mainHandUUIDs[0], "Coin modifier", value, AttributeModifier.Operation.ADDITION));
        this.mainHandModifiers = mainBuilder.build();

        ImmutableMultimap.Builder<Attribute, AttributeModifier> offBuilder = ImmutableMultimap.builder();
        offBuilder.put(attribute, new AttributeModifier(offHandUUIDs[0], "Coin modifier", value, AttributeModifier.Operation.ADDITION));
        this.offHandModifiers = offBuilder.build();

        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    @Override
    @MethodsReturnNonnullByDefault
    public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlotType slot) {
        if (slot == EquipmentSlotType.MAINHAND) {
            return mainHandModifiers;
        }
        else if (slot == EquipmentSlotType.OFFHAND) {
            return offHandModifiers;
        }
        return super.getDefaultAttributeModifiers(slot);
    }

    public EquipmentSlotType getSlot() {
        return EquipmentSlotType.OFFHAND;
    }

    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return slotChanged;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class EnergyItemHandler {

        @SubscribeEvent
        public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
            if (event.phase == TickEvent.Phase.START) {
                PlayerEntity player = event.player;
                World world = player.getCommandSenderWorld();
                ItemStack[] stacks = {player.getItemBySlot(EquipmentSlotType.MAINHAND), player.getItemBySlot(EquipmentSlotType.OFFHAND)};
                for (ItemStack stack : stacks) {
                    if (stack.getItem() instanceof CoinItem) {
                        CompoundNBT tag = stack.getTag();
                        tag.putInt("useTime", tag.getInt("useTime") - 1);
                        int useTime = tag.getInt("useTime");
                        if (useTime <= 0) {
                            tag.putInt("useTime", 20);
                            stack.hurtAndBreak(1, player, (entity) -> {
                                        entity.broadcastBreakEvent(player.getUsedItemHand());
                                    }
                            );
                        }
                    }
                }
            }
        }
    }
}
