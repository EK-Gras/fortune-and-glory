package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.SwordItem;

import java.util.List;
import java.util.Map;

public class MeleeItem extends SwordItem implements IDefaultEnchantmentData {

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public MeleeItem(Item.Properties builder, IItemTier tier, int attackDamage, float attackSpeed,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(tier, attackDamage, attackSpeed, builder);
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }
}
