package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.abilities.Ability;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.StaffAOEEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;

public class StaffItem extends EnergyItem {

    // Ticks of delay between each use of the item
    private final int fireSpeed;

    // Amount of energy consumed per use
    private final float usageRate;

    // Amount of energy recharged per tick
    private final float rechargeRate;

    // Number of ticks between last use of the item and when it begins to recharge
    private final int rechargeCooldown;

    // Whether or not the staff should also affect its user
    private final boolean includeUser;

    // Ability triggered on all the entities affected by the staff
    private final Ability ability;

    // Additional damage dealt by staff
    private final int damage;

    // Size of the bounding box in which entities are affected by the staff.
    // Why you would want different x and z values, I have no idea, but the option is there.
    private final double attackRangeX;
    private final double attackRangeZ;
    private final double attackRangeY;

    // Parameters for the sound the staff should make when used
    public final SoundEvent sound;
    public final float volume;
    public final float pitch;

    public StaffItem(Properties properties, int fireSpeed, float usageRate, float rechargeRate, int rechargeCooldown,
                     SoundEvent sound, float volume, float pitch, boolean includeUser, Ability ability, int damage,
                     double attackRangeHorizontal, double attackRangeVertical,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        this(properties, fireSpeed, usageRate, rechargeRate, rechargeCooldown,
                sound, volume, pitch, includeUser, ability, damage,
                attackRangeHorizontal, attackRangeVertical, attackRangeHorizontal,
                defaultEnchantments, variantEnchantments);
    }

    public StaffItem(Properties properties, int fireSpeed, float usageRate, float rechargeRate, int rechargeCooldown,
                     SoundEvent sound, float volume, float pitch, boolean includeUser, Ability ability, int damage,
                     double attackRangeX, double attackRangeY, double attackRangeZ,
                     List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, fireSpeed, usageRate, rechargeRate, rechargeCooldown, defaultEnchantments, variantEnchantments);
        this.fireSpeed = fireSpeed;
        this.usageRate = usageRate;
        this.rechargeRate = rechargeRate;
        this.rechargeCooldown = rechargeCooldown;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.includeUser = includeUser;
        this.ability = ability;
        this.damage = damage;
        this.attackRangeX = attackRangeX;
        this.attackRangeZ = attackRangeZ;
        this.attackRangeY = attackRangeY;
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        CompoundNBT tag = stack.getTag();
        float energy = tag.getFloat("energy");
        if (energy >= this.usageRate) {
            AxisAlignedBB axisalignedbb = player.getBoundingBox().inflate(attackRangeX, attackRangeY, attackRangeZ);
            List<LivingEntity> list = world.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            if (world.isClientSide) {
                Minecraft.getInstance().particleEngine.createTrackingEmitter(player, ParticleTypes.ENCHANTED_HIT);
            }
            world.playSound(null, player.getX(), player.getY(), player.getZ(), sound, SoundCategory.PLAYERS, volume, pitch);
            if (!includeUser) {
                list.remove(player);
            }
            if (!list.isEmpty()) {
                StaffAOEEntity staffEntity = new StaffAOEEntity(world);
                staffEntity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                DamageSource source = DamageSource.thrown(staffEntity, player);
                source.setMagic();
                Map<Enchantment, Integer> enchants = staffEntity.getItemEnchantments();
                for (LivingEntity entity : list) {
                    if (damage > 0) {
                        entity.hurt(source, damage);
                        if (enchants.containsKey(Enchantments.FLAMING_ARROWS)) entity.setSecondsOnFire(5);
                        if (enchants.containsKey(Enchantments.PUNCH_ARROWS)) {
                            Vector3d vector3d = entity.getPosition(1.0f).subtract(player.position()).multiply(1.0D, 0.0D, 1.0D).normalize().scale(enchants.get(Enchantments.PUNCH_ARROWS) * 0.6D);
                            if (vector3d.lengthSqr() > 0.0D) {
                                entity.push(vector3d.x, 0.1D, vector3d.z);
                            }
                        }
                    }
                    ability.trigger(entity);
                }
            }
            tag.putFloat("energy", energy - this.usageRate);
            player.getCooldowns().addCooldown(this, this.fireSpeed);
            tag.putInt("rechargeCooldown", this.rechargeCooldown);
            stack.hurtAndBreak(1, player, (entity) -> {
                entity.broadcastBreakEvent(player.getUsedItemHand());
            });
            return ActionResult.success(stack);
        }
        else {
            return ActionResult.fail(stack);
        }
    }
}
