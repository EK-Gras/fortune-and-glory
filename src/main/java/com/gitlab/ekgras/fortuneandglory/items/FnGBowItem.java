package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BowArrowEntity;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.FnGArrowEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.*;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class FnGBowItem extends BowItem implements IDefaultEnchantmentData {

    // Base damage of the arrow (before multiplier for crits & other enchantments)
    // Set to 2.0 for default bow damage
    private final double damage;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public FnGBowItem(Item.Properties builder, double damage,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(builder);
        this.damage = damage;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public void releaseUsing(ItemStack stack, World world, LivingEntity entity, int timeLeft) {
        if (entity instanceof PlayerEntity) {
            PlayerEntity playerentity = (PlayerEntity)entity;
            boolean ammoNotNeeded = playerentity.abilities.instabuild || EnchantmentHelper.getItemEnchantmentLevel(Enchantments.INFINITY_ARROWS, stack) > 0;
            ItemStack ammo = playerentity.getProjectile(stack);

            int useTime = this.getUseDuration(stack) - timeLeft;
            useTime = net.minecraftforge.event.ForgeEventFactory.onArrowLoose(stack, world, playerentity, useTime, !ammo.isEmpty() || ammoNotNeeded);
            if (useTime < 0) return;

            stack.getTag().putBoolean("firstArrowFired", true);

            if (!ammo.isEmpty() || ammoNotNeeded) {

                Map<Enchantment, Integer> enchants = EnchantmentHelper.getEnchantments(stack);

                if (ammo.isEmpty()) {
                    ammo = new ItemStack(Items.ARROW);
                }
                int useTimeBonus = 0;
                if (enchants.containsKey(Enchantments.QUICK_CHARGE)) {
                    useTimeBonus += enchants.get(Enchantments.QUICK_CHARGE) * 5;
                }
                float power = this.getArrowVelocity(useTime + useTimeBonus);
                if (!((double)power < 0.1D)) {
                    boolean consumeAmmo = playerentity.abilities.instabuild || (ammo.getItem() instanceof ArrowItem && ((ArrowItem)ammo.getItem()).isInfinite(ammo, stack, playerentity));

                    stack.hurtAndBreak(1, entity, (livingEntity) -> {
                        livingEntity.broadcastBreakEvent(playerentity.getUsedItemHand());
                    });

                    fireArrow(world, stack, ammo, playerentity, consumeAmmo, power, 0);

                    if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.MULTISHOT) && power >= 1.0F) {
                        fireArrow(world, stack, ammo, playerentity, consumeAmmo, power, 10);
                        fireArrow(world, stack, ammo, playerentity, consumeAmmo, power, -10);
                    }

                    if (!consumeAmmo && !playerentity.abilities.instabuild) {
                        ammo.shrink(1);
                        if (ammo.isEmpty()) {
                            playerentity.inventory.removeItem(ammo);
                        }
                    }

                    playerentity.awardStat(Stats.ITEM_USED.get(this));
                }
            }
        }
    }

    public void fireArrow(World world, ItemStack stack, ItemStack ammo, PlayerEntity playerentity, boolean consumeAmmo, float power, int yawModifier) {
        if (!world.isClientSide) {
            ArrowItem arrowitem = (ArrowItem)(ammo.getItem() instanceof ArrowItem ? ammo.getItem() : Items.ARROW);
            AbstractArrowEntity arrowEntity = arrowitem.createArrow(world, ammo, playerentity);
            arrowEntity = customArrow(arrowEntity);

            if (arrowEntity instanceof ArrowEntity) {
                arrowEntity = BowArrowEntity.fromArrow((ArrowEntity) arrowEntity, stack);
            }

            if (arrowEntity instanceof BowArrowEntity) {
                ((BowArrowEntity) arrowEntity).setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
            }

            if (yawModifier == 0.0f) arrowEntity.shootFromRotation(playerentity, playerentity.xRot, playerentity.yRot, 0.0F, power * 3.0F, 1.0F);
            else {
                Vector3d vector3d1 = playerentity.getUpVector(1.0F);
                Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), yawModifier, true);
                Vector3d vector3d = playerentity.getViewVector(1.0F);
                Vector3f vector3f = new Vector3f(vector3d);
                vector3f.transform(quaternion);
                arrowEntity.shoot(vector3f.x(), vector3f.y(), vector3f.z(), power * 3.0F, 1.0F);
                Vector3d playerVector = playerentity.getDeltaMovement();
                arrowEntity.setDeltaMovement(arrowEntity.getDeltaMovement().add(playerVector.x, playerentity.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
            }

            if (power >= 1.0F) {
                arrowEntity.setCritArrow(true);
            }

            arrowEntity.setBaseDamage(this.damage);

            int j = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, stack);
            if (j > 0) {
                arrowEntity.setBaseDamage(arrowEntity.getBaseDamage() + (double)j * 0.5D + 0.5D);
            }

            int k = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, stack);
            if (k > 0) {
                arrowEntity.setKnockback(k);
            }

            if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, stack) > 0) {
                arrowEntity.setSecondsOnFire(100);
            }

            if (consumeAmmo || playerentity.abilities.instabuild && (ammo.getItem() == Items.SPECTRAL_ARROW || ammo.getItem() == Items.TIPPED_ARROW)
            || !stack.getTag().getBoolean("firstArrowFired")) {
                arrowEntity.pickup = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
            }

            stack.getTag().putBoolean("firstArrowFired", false);

            world.addFreshEntity(arrowEntity);
        }

        world.playSound((PlayerEntity) null, playerentity.getX(), playerentity.getY(), playerentity.getZ(), SoundEvents.ARROW_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.0F / (random.nextFloat() * 0.4F + 1.2F) + power * 0.5F);
    }

    public float getArrowVelocity(int useTime) {
        float f = (float)useTime / 20.0F;
        f = (f * f + f * 2.0F) / 3.0F;
        if (f > 1.0F) {
            f = 1.0F;
        }

        return f;
    }

}
