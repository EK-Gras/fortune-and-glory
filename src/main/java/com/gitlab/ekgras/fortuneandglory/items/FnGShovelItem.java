package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShovelItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class FnGShovelItem extends ShovelItem implements IToolEffectivenessSets, IDefaultEnchantmentData {

    public final float speed;
    private static final Set<Block> DIGGABLES = IToolEffectivenessSets.SHOVEL;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public FnGShovelItem(Item.Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                         List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(tier, attackDamageBaseLine, attackSpeed, properties);
        this.speed = destroySpeed;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public float getDestroySpeed(ItemStack stack, BlockState state) {
        if (getToolTypes(stack).stream().anyMatch(e -> state.isToolEffective(e))) return speed;
        return this.DIGGABLES.contains(state.getBlock()) ? this.speed : 1.0F;
    }

}
