package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.FnGProjectileItemEntity;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShootableItem;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.World;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class CannonItem extends ShootableItem {
    public final Predicate<ItemStack> ammo;

    // Time (in ticks) for the item to be usable again after being used
    public final int cooldown;

    // Amount of damage the fired projectile deals
    public final int damage;

    // Inaccuracy of the fired projectile (calculated the same way as default projectiles)
    public final float inaccuracy;

    // Velocity of the fired projectile (in blocks per tick)
    public final float velocity;

    // Scale of the fired projectile
    public final float scale;

    // Parameters for the sound the cannon should make when fired
    public final SoundEvent sound;
    public final float volume;
    public final float pitch;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public CannonItem(Properties properties, Predicate<ItemStack> ammo, int cooldown, int damage, float inaccuracy,
                      float velocity, float scale, SoundEvent sound, float volume, float pitch,
                      List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties);
        this.ammo = ammo;
        this.cooldown = cooldown;
        this.damage = damage;
        this.inaccuracy = inaccuracy;
        this.velocity = velocity;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.scale = scale;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
        if (stack.getItem() instanceof CannonItem) {
            ItemStack ammo = player.getProjectile(stack);
            if (ammo != ItemStack.EMPTY) {
                world.playSound(null, player.getX(), player.getY(), player.getZ(), sound, SoundCategory.NEUTRAL, volume, pitch / (random.nextFloat() * 0.4F + 0.8F));
                if (!world.isClientSide) {
                    FnGProjectileItemEntity entity = new FnGProjectileItemEntity(world, player, ammo, scale, damage);
                    entity.setItem(ammo);
                    entity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                    entity.shootFromRotation(player, player.xRot, player.yRot, 0.0F, 1.5F, 1.0F);
                    world.addFreshEntity(entity);
                    if (entity.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS)) entity.setSecondsOnFire(100);
                    if (entity.getItemEnchantments().containsKey(Enchantments.MULTISHOT)) {
                        for (int i = 0; i < 2; i++){
                            FnGProjectileItemEntity mEntity = new FnGProjectileItemEntity(world, player, ammo, scale, damage);
                            mEntity.setItem(ammo.copy());
                            mEntity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));
                            Vector3d vector3d1 = player.getUpVector(1.0F);
                            Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i == 0 ? 10F : -10F, true);
                            Vector3d vector3d = player.getViewVector(1.0F);
                            Vector3f vector3f = new Vector3f(vector3d);
                            vector3f.transform(quaternion);
                            mEntity.shoot(vector3f.x(), vector3f.y(), vector3f.z(), 0.75F, 1.0F);
                            Vector3d playerVector = mEntity.getDeltaMovement();
                            mEntity.setDeltaMovement(mEntity.getDeltaMovement().add(playerVector.x, mEntity.isOnGround() ? 0.0D : playerVector.y, playerVector.z));
                            if (mEntity.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
                                mEntity.setSecondsOnFire(100);
                            world.addFreshEntity(mEntity);
                        }
                    }
                    player.getCooldowns().addCooldown(this, this.cooldown);
                }
                stack.hurtAndBreak(1, player, (lHand) -> {
                    lHand.broadcastBreakEvent(player.getUsedItemHand());
                });
                player.awardStat(Stats.ITEM_USED.get(this));
                if (!player.abilities.instabuild && !EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.INFINITY_ARROWS)) {
                    ammo.shrink(1);
                }
                return ActionResult.sidedSuccess(stack, world.isClientSide());
            }
        }
        return ActionResult.fail(stack);
    }

    public Predicate<ItemStack> getAllSupportedProjectiles() {
        return this.ammo;
    }

    public int getDefaultProjectileRange() {
        return (int)(15 * this.velocity);
    }
}
