package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import com.gitlab.ekgras.fortuneandglory.items.util.RectangleAOEHarvestLogic;
import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.ToolHarvestLogic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ToolType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SledgehammerItem extends AOEToolItem implements IToolEffectivenessSets {

    private static final RectangleAOEHarvestLogic HARVEST_LOGIC = new RectangleAOEHarvestLogic(1, 1, 0);
    private final float speed;
    private static final Set<Block> DIGGABLES = IToolEffectivenessSets.PICKAXE;

    public SledgehammerItem(Item.Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                            List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(properties, tier, attackDamageBaseLine, attackSpeed, destroySpeed, ToolType.PICKAXE,
                defaultEnchantments, variantEnchantments);
        this.speed = destroySpeed;
    }

    @Override
    public ToolHarvestLogic getToolHarvestLogic() {
        return HARVEST_LOGIC;
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return material != Material.METAL && material != Material.HEAVY_METAL && material != Material.STONE ? super.getDestroySpeed(stack, state) : this.speed;
    }
}
