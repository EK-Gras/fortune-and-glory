package com.gitlab.ekgras.fortuneandglory.items;

import com.gitlab.ekgras.fortuneandglory.items.util.IDefaultEnchantmentData;
import com.gitlab.ekgras.fortuneandglory.items.util.IToolEffectivenessSets;
import com.gitlab.ekgras.fortuneandglory.items.util.LeveledEnchantment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PickaxeItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class FnGPickaxeItem extends PickaxeItem implements IToolEffectivenessSets, IDefaultEnchantmentData {

    private final float speed;
    private static final Set<Block> DIGGABLES = IToolEffectivenessSets.PICKAXE;

    // The item's default and variant enchantments
    private final List<LeveledEnchantment> defaultEnchantments;
    private final List<LeveledEnchantment> variantEnchantments;

    public FnGPickaxeItem(Item.Properties properties, IItemTier tier, int attackDamageBaseLine, float attackSpeed, float destroySpeed,
                          List<LeveledEnchantment> defaultEnchantments, List<LeveledEnchantment> variantEnchantments) {
        super(tier, attackDamageBaseLine, attackSpeed, properties);
        this.speed = destroySpeed;
        this.defaultEnchantments = defaultEnchantments;
        this.variantEnchantments = variantEnchantments;
    }

    public List<LeveledEnchantment> getDefaultEnchantments() {
        return this.defaultEnchantments;
    }

    public List<LeveledEnchantment> getVariantEnchantments() {
        return this.variantEnchantments;
    }

    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
        return material != Material.METAL && material != Material.HEAVY_METAL && material != Material.STONE ? super.getDestroySpeed(stack, state) : this.speed;
    }
}
