package com.gitlab.ekgras.fortuneandglory.block;

import com.gitlab.ekgras.fortuneandglory.util.IterationUtils;
import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

// Doesn't actually work yet, be warned
import net.minecraft.block.AbstractBlock.Properties;

@MethodsReturnNonnullByDefault
public class VaultBombBlock extends Block {

    public static final Duration ACTIVATE_TIME = Duration.ofSeconds(15);

    public VaultBombBlock(Properties properties) {
        super(properties);
    }

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos,
                                             PlayerEntity player, Hand hand, BlockRayTraceResult ray) {
        List<BlockPos> adjacentVaultBlocks = new ArrayList<>(6);

        // Iterate the adjacent blocks
        IterationUtils.iterateAdjacent3d(pos.getX(), pos.getY(), pos.getZ(), (x, y, z) -> {
            BlockPos adjacent = new BlockPos(x, y, z);
            if (world.getBlockState(adjacent).getBlock() instanceof VaultBlock) {
                adjacentVaultBlocks.add(adjacent);
            }
        });

        // If there are no adjacent vault blocks, fail to activate
        if (adjacentVaultBlocks.isEmpty()) {
            player.displayClientMessage(new StringTextComponent("There are no adjacent vault blocks!"), true);
            return ActionResultType.FAIL;
        }

        // Wait ACTIVATE_TIME_SECONDS and then execute on the following tick
        TickScheduler.schedule(ACTIVATE_TIME, false, () -> {
            if (world.getBlockState(pos) != state) {
                // This vault block is no longer there
                return;
            }
            // Clear all the blocks
            for (BlockPos vaultBlock : adjacentVaultBlocks) {
                world.setBlockAndUpdate(vaultBlock, Blocks.AIR.defaultBlockState());
            }
        });

        return ActionResultType.SUCCESS;
    }


}
