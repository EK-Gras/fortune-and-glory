package com.gitlab.ekgras.fortuneandglory.block;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class FnGBlockStateProvider extends BlockStateProvider {
    public FnGBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, FortuneAndGlory.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {

    }
}
