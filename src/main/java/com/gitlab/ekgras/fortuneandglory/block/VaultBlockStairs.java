package com.gitlab.ekgras.fortuneandglory.block;

import com.gitlab.ekgras.fortuneandglory.init.FnGTileEntities;
import net.minecraft.block.*;

import net.minecraft.state.properties.Half;
import net.minecraft.state.properties.StairsShape;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.IBlockReader;

import javax.annotation.Nullable;
import java.util.UUID;

public class VaultBlockStairs extends StairsBlock {

    /**
     * UUID of the owner of this block
     */
    @Nullable
    private UUID owner = null;

    public VaultBlockStairs(java.util.function.Supplier<BlockState> state, AbstractBlock.Properties properties) {
        super(state, properties);
        this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(HALF, Half.BOTTOM).setValue(SHAPE, StairsShape.STRAIGHT).setValue(WATERLOGGED, Boolean.valueOf(false)));
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return FnGTileEntities.VAULT_BLOCK_STAIRS.get().create();
    }

}