package com.gitlab.ekgras.fortuneandglory.block;

import com.gitlab.ekgras.fortuneandglory.init.FnGTileEntities;
import net.minecraft.block.BlockState;
import net.minecraft.block.SlabBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class VaultBlockSlab extends SlabBlock {

    public VaultBlockSlab(Properties properties) {
        super(properties);
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return FnGTileEntities.VAULT_BLOCK_SLAB.get().create();
    }

}
