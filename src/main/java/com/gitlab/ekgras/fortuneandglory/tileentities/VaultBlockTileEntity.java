package com.gitlab.ekgras.fortuneandglory.tileentities;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import com.gitlab.ekgras.fortuneandglory.init.FnGTileEntities;
import com.gitlab.ekgras.fortuneandglory.block.VaultBlockSlab;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.PistonEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.EnumSet;
import java.util.Objects;
import java.util.UUID;

@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
public class VaultBlockTileEntity extends TileEntity {

    /**
     * UUID of the owner of this block
     */
    @Nullable
    private UUID owner = null;

    private EnumSet<Direction> pushableDirections = EnumSet.noneOf(Direction.class);

    @Nullable
    public UUID getOwner() {
        return owner;
    }

    public void setOwner(@Nullable UUID owner) {
        this.owner = owner;
        this.setChanged();
    }

    public boolean isPushableDirection(Direction direction) {
        return pushableDirections.contains(direction);
    }

    public void setPushableDirection(Direction direction, boolean isPushable) {
        if (isPushable) {
            pushableDirections.add(direction);
        } else {
            pushableDirections.remove(direction);
        }
    }

    public VaultBlockTileEntity() {
        super(FnGTileEntities.VAULT_BLOCK.get());
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        CompoundNBT nbt = save(new CompoundNBT());
        //Write your data into the nbtTag
        return new SUpdateTileEntityPacket(getBlockPos(), 1, nbt);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        CompoundNBT tag = packet.getTag();
        BlockState state = this.getBlockState();
        load(state, tag);
    }

    @Override
    public void load(BlockState state, CompoundNBT compound) {
        super.load(state, compound);
        if (compound.hasUUID("owner")) {
            this.setOwner(compound.getUUID("owner"));
        }
    }

    @Override
    public CompoundNBT save(CompoundNBT compound) {
        super.save(compound);
        if (owner != null) {
            compound.putUUID("owner", owner);
        } else {
            compound.remove("owner");
        }
        return compound;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class VaultBlockTileEntityEvents {
        /**
         * Listen for when a player begins to mine this block...
         * DO NOTE: this is called from BOTH client AND server. don't learn this the hard way
         */
        @SubscribeEvent
        public static void onPlayerHarvest(PlayerEvent.BreakSpeed harvestEvent) {
            BlockState state = harvestEvent.getState();
            // Get the block being mined
            Block block = state.getBlock();
            // Get the player mining the block
            PlayerEntity player = harvestEvent.getPlayer();
            // If the block is or extends Vault Block...
            if (block.hasTileEntity(state)) {
                // Get the tile entity
                TileEntity te = harvestEvent.getEntity().getCommandSenderWorld()
                        .getBlockEntity(harvestEvent.getPos());

                // If the tile entity is a vault block tile entity
                if (te instanceof VaultBlockTileEntity) {
                    VaultBlockTileEntity vte = (VaultBlockTileEntity) te;
                    // If the block has an owner and the owner is not the player mining the block
                    if (!Objects.equals(vte.getOwner(), player.getUUID())) {
                        // Send a message to the player
                        player.displayClientMessage(new StringTextComponent("You do not own that vault block"), true);
                        // Prevent the player from mining the vault block
                        harvestEvent.setCanceled(true);
                    }
                }
            }
        }

        /**
         * Listen for when a player places a block
         */
        @SubscribeEvent
        public static void onBlockPlace(BlockEvent.EntityPlaceEvent blockPlaceEvent) {
            if (blockPlaceEvent.getEntity() instanceof PlayerEntity) {
                BlockState state = blockPlaceEvent.getState();
                // Get the block placed
                Block block = blockPlaceEvent.getPlacedBlock().getBlock();
                // Get the block that the placed block replaced
                Block replaced = blockPlaceEvent.getBlockSnapshot().getReplacedBlock().getBlock();
                // Get the player that placed the block
                PlayerEntity player = (PlayerEntity) blockPlaceEvent.getEntity();
                // If the block placed has a tile entity and the block was placed by an entity
                if (block.hasTileEntity(state) && blockPlaceEvent.getEntity() != null) {
                    TileEntity te = blockPlaceEvent.getEntity().getCommandSenderWorld()
                            .getBlockEntity(blockPlaceEvent.getPos());
                    // Check if both the replaced block and the placed block are slabs
                    if (block instanceof VaultBlockSlab && replaced instanceof VaultBlockSlab) {
                        // Get the tile entity of the replaced block
                        TileEntity rte = blockPlaceEvent.getBlockSnapshot().getTileEntity();
                        //If the replaced block's owner is not the same as the player placing the block
                        if (((VaultBlockTileEntity) rte).getOwner() == null || !((VaultBlockTileEntity) rte).getOwner().equals(player.getUUID())) {
                            // Cancel the blockPlaceEvent
                            blockPlaceEvent.setCanceled(true);
                            // Send the player a message
                            player.displayClientMessage(new StringTextComponent("You do not own that vault block"), true);
                        } else {
                            // Set the owner of the vault block to the entity's UUID
                            ((VaultBlockTileEntity) te).setOwner(blockPlaceEvent.getEntity().getUUID());
                        }
                    }
                    // If the tile entity is a vault block tile entity
                    else if (te instanceof VaultBlockTileEntity) {
                        // Set the owner of the vault block to the entity's UUID
                        ((VaultBlockTileEntity) te).setOwner(blockPlaceEvent.getEntity().getUUID());
                    }
                }
            }
        }

        @SubscribeEvent
        public static void onPistonPush(PistonEvent.Pre event) {
            Direction direction = event.getDirection();
            World world = (World) event.getWorld();
            BlockState blockPushedState =  world.getBlockState(event.getPos().relative(direction));
            if (world.getBlockEntity(event.getPos().relative(direction)) instanceof VaultBlockTileEntity) {
                VaultBlockTileEntity blockPushedEntity = (VaultBlockTileEntity) world.getBlockEntity(event.getPos().relative(direction));
                if (!blockPushedEntity.isPushableDirection(direction)) {
                    event.setCanceled(true);
                }
            }
        }

        @SubscribeEvent
        public static void onPlayerClickBlock(PlayerInteractEvent.RightClickBlock event) {
            Direction direction = event.getFace();
            BlockPos pos = event.getHitVec().getBlockPos();
            ItemStack stack = event.getItemStack();
            Item item = stack.getItem();
            event.setUseItem(Event.Result.ALLOW);
            if (item == FnGItems.VAULT_WRENCH.get() && event.getWorld().getBlockEntity(pos) instanceof VaultBlockTileEntity) {
                VaultBlockTileEntity te = (VaultBlockTileEntity) event.getWorld().getBlockEntity(pos);
                if (Objects.equals(te.owner, event.getEntity().getUUID())) {
                    if (te.isPushableDirection(direction)) {
                        te.setPushableDirection(direction.getOpposite(), false);
                    } else {
                        te.setPushableDirection(direction.getOpposite(), true);
                    }
                }
            }
        }
    }
}
