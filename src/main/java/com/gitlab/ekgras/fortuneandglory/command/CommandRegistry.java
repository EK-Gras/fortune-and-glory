package com.gitlab.ekgras.fortuneandglory.command;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.CommandSource;

public class CommandRegistry {

    public static void registerCommands(CommandDispatcher<CommandSource> dispatcher) {
        TestCommand.register(dispatcher);
    }

}
