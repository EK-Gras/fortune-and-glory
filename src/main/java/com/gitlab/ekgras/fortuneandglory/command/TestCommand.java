package com.gitlab.ekgras.fortuneandglory.command;

import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.text.StringTextComponent;

import java.time.Duration;

public class TestCommand {

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        dispatcher.register(Commands.literal("test").executes(ctx -> {
            for (int i = 0; i < 10; i++) {
                TickScheduler.schedule(Duration.ofSeconds(i), false, () -> {
                    ctx.getSource().sendSuccess(new StringTextComponent("Test"), true);
                });
            }
            return 1;
        }));
    }

}
