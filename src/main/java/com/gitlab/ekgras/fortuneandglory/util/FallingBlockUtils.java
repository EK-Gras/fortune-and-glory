package com.gitlab.ekgras.fortuneandglory.util;

import net.minecraft.block.BlockState;
import net.minecraft.block.PistonBlock;
import net.minecraft.block.material.PushReaction;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.item.TieredItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.server.ServerWorld;

import java.util.Random;
import java.util.Set;

public class FallingBlockUtils {
     public static void disrupt(ServerWorld level, BlockPos pos) {
         BlockState state = level.getBlockState(pos);
         FallingBlockEntity fallingblockentity = new FallingBlockEntity(level, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, level.getBlockState(pos));
         if (state.getBlock().isAir(state, level, pos) || state.hasTileEntity()) return;
         fallingblockentity.setDeltaMovement(0, 0.33d, 0);
         level.addFreshEntity(fallingblockentity);
    }

    public static void disruptCascade(ServerWorld level, BlockPos pos, int radius) {
         disruptCascade(level, pos, radius, 0);
    }

    public static void disruptCascade(ServerWorld level, BlockPos pos, int maxRadius, int minRadius) {
        for (int r = minRadius; r <= maxRadius; r++) {
            int cr = r;
            TickScheduler.schedule(r * 5, level.isClientSide, () -> {
                    int sr = cr - 1;

                    disrupt(level, pos.offset(new Vector3i(cr, 0, 0)));
                    disrupt(level, pos.offset(new Vector3i(-cr, 0, 0)));

                    for (int x = -sr; x <= sr; x++) {
                        disrupt(level, pos.offset(x, 0, cr - Math.abs(x)));
                        disrupt(level, pos.offset(x, 0, Math.abs(x) - cr));
                    }
                }
            );
        }
    }
}
