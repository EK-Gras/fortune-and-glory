package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SRemoveEntityEffectPacket;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.PotionEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.PacketDistributor;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class ElementEventHandler {
	@SubscribeEvent
	public static void onPlayerStartTracking(PlayerEvent.StartTracking event) {
		LivingEntity entity = event.getEntityLiving();
		((ElementalPercentages) entity).synchronize();
		// todo: doesn't include entity effects yet
	}

	@SubscribeEvent
	public static void onPotionAdd(PotionEvent.PotionAddedEvent event) {
		// todo: limit this to only fng effects
		LivingEntity entity = event.getEntityLiving();

		if (entity.level.isClientSide) return;

		EffectInstance e = event.getPotionEffect();
		PacketDistributor.TRACKING_ENTITY.with(() -> entity).send(new SPlayEntityEffectPacket(entity.getId(), e));
	}

	@SubscribeEvent
	public static void onPotionRemove(PotionEvent.PotionRemoveEvent event) {
		// todo: limit this to only fng effects
		LivingEntity entity = event.getEntityLiving();

		if (entity.level.isClientSide) return;

		PacketDistributor.TRACKING_ENTITY.with(() -> entity).send(new SRemoveEntityEffectPacket(entity.getId(), event.getPotion()));
	}

	@SubscribeEvent
	public static void onPotionExpiry(PotionEvent.PotionExpiryEvent event) {
		// todo: limit this to only fng effects
		LivingEntity entity = event.getEntityLiving();

		if (entity.level.isClientSide) return;

		PacketDistributor.TRACKING_ENTITY.with(() -> entity).send(new SRemoveEntityEffectPacket(entity.getId(), event.getPotionEffect().getEffect()));
	}
}
