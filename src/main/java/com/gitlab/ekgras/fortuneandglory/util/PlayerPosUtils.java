package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.block.AbstractFireBlock;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;
import java.util.Map;

import static net.minecraftforge.common.util.Constants.BlockFlags.DEFAULT_AND_RERENDER;

// Be careful not to access this on the client. This code only runs on the server so that each map isn't edited twice per tick (in singleplayer)
@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class PlayerPosUtils {

    // Position of the player on the previous tick
    private static final Map<PlayerEntity, BlockPos> previousTickBlockMap = new HashMap<>();

    // Position of the player before their current position
    private static final Map<PlayerEntity, BlockPos> lastBlockMap = new HashMap<>();

    // Position of the player before their current position and before their next most recent position
    private static final Map<PlayerEntity, BlockPos> lastLastBlockMap = new HashMap<>();

    @SubscribeEvent
    public static void onTick(TickEvent.PlayerTickEvent event) {
        PlayerEntity player = event.player;
        World world = player.getCommandSenderWorld();
        if (world.isClientSide) return;

        BlockPos pos = event.player.blockPosition();

        if (!previousTickBlockMap.containsKey(player))
            previousTickBlockMap.put(player, pos);

        if (!lastBlockMap.containsKey(player))
            lastBlockMap.put(player, pos);

        if (!lastLastBlockMap.containsKey(player))
            lastLastBlockMap.put(player, pos);

        if (!previousTickBlockMap.get(player).equals(pos)) {
            if (!lastBlockMap.get(player).equals(pos) && !lastBlockMap.get(player).equals(previousTickBlockMap.get(player))) {
                lastLastBlockMap.put(player, lastBlockMap.get(player));
            }
            lastBlockMap.put(player, previousTickBlockMap.get(player));
        }
        previousTickBlockMap.put(player, pos);
    }
}

