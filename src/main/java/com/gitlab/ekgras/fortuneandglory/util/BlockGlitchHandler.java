package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.block.VaultBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class BlockGlitchHandler {

    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
        PlayerEntity player = event.player;
        if (player.getCommandSenderWorld().getBlockState(player.blockPosition()).getBlock() instanceof VaultBlock && !player.isCreative()) {
            player.kill();
        }
    }
}
