package com.gitlab.ekgras.fortuneandglory.util;

public interface ElementalPercentages {
    float getFreezePercentage();
    float getRenderFreezePercentage();
    void setFreezePercentage(float percentage);
    void addFreezePercentage(float percentage);


    float getShockPercentage();
    float getRenderShockPercentage();
    void setShockPercentage(float percentage);
    void addShockPercentage(float percentage);

    void synchronize();

    void doFreezeCalc(float damage, int level);
    void doShockCalc(float damage, int level);
}
