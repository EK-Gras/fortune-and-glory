package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGClientRegistry;
import com.gitlab.ekgras.fortuneandglory.init.FnGEffects;
import com.gitlab.ekgras.fortuneandglory.items.BadgeItem;
import com.gitlab.ekgras.fortuneandglory.items.GunItem;
import com.gitlab.ekgras.fortuneandglory.items.ScepterItem;
import com.gitlab.ekgras.fortuneandglory.network.FnGPacketHandler;
import com.gitlab.ekgras.fortuneandglory.network.packet.*;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.lwjgl.glfw.GLFW;
import top.theillusivec4.curios.api.CuriosApi;

import static net.minecraft.util.Hand.MAIN_HAND;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class ClientInputEventHandler {

    // This listener listens for when the client left clicks. ClickInputEvent only detects game inputs, meaning it does not fire in menus or outside of game worlds.
    @SubscribeEvent
    public static void onPlayerLeftClick (InputEvent.ClickInputEvent event) {
        if (event.isAttack()) {
            PlayerEntity player = Minecraft.getInstance().player;
            ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            if (stack.getItem() instanceof GunItem && !player.hasEffect(FnGEffects.SHOCK.get())) {

                CompoundNBT tag = stack.getTag();
                GunItem gun = (GunItem)stack.getItem();
                event.setSwingHand(false);

                if (!gun.isAutomatic() && !tag.getBoolean("isReloading") && !tag.getBoolean("isCooldown")) {

                    // ClickInputEvent only fires on the logical client, so in order to fire the bullet a packet must be sent to the server.
                    for (int i = 0; i < gun.getShots(); i++) {
                        FireBulletPacket fMsg = FireBulletPacket.create(gun, false, player.getViewVector(1.0F));
                        FnGPacketHandler.sendToServer(fMsg);
                        if (EnchantmentHelper.getEnchantments(stack).containsKey(Enchantments.MULTISHOT)) {
                            for (int i1 = 0; i1 < 2; i1++) {
                                Vector3d vector3d1 = player.getUpVector(1.0F);
                                Quaternion quaternion = new Quaternion(new Vector3f(vector3d1), i1 == 0 ? -10 : 10, true);
                                Vector3d vector3d = player.getViewVector(1.0F);
                                Vector3f vector3f = new Vector3f(vector3d);
                                vector3f.transform(quaternion);
                                FireBulletPacket fmMsg = FireBulletPacket.create(gun, false, new Vector3d(vector3f));
                                FnGPacketHandler.sendToServer(fmMsg);
                            }
                        }
                    }
                    UseGunPacket uMsg = new UseGunPacket(false);
                    FnGPacketHandler.sendToServer(uMsg);
                    player.absMoveTo(player.getX(), player.getY(), player.getZ(), player.yRot, player.xRot - (gun.getRecoil()));

                    // If the gun still has bullets loaded, then put it in cooldown
                    if (tag.getInt("bulletsLoaded") > 0) {
                        tag.putBoolean("isCooldown", true);
                        tag.putInt("cooldown", gun.getCooldown());
                    }

                    // If the gun has no bullets loaded but is not currently reloaded, start reloading it.
                    // isReloading and reloadCooldown need to be separate flags so there is a difference between when the gun has not yet started reloading and when the gun is finished reloading.
                    if (tag.getInt("bulletsLoaded") <= 0 && !tag.getBoolean("isReloading")) {
                        tag.putBoolean("isReloading", true);
                        tag.putInt("reloadCooldown", gun.getReloadSpeed());
                    }
                }
            }
        }
    }

    // NOTE that this one DOES detect inputs outside of the game, so you need to use KeyBinding.isDown() to check if it's a game input.
    // Additionally you can check if Minecraft.screen is null, which will only be true if the player is in game.
    @SubscribeEvent
    public static void onKeyboardInput(InputEvent.KeyInputEvent event) {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (event.getAction() == GLFW.GLFW_PRESS) {
            if (FnGClientRegistry.KEY_ALT_ITEM_FUNCTION.getKey().getValue() == event.getKey() && FnGClientRegistry.KEY_ALT_ITEM_FUNCTION.isDown()) {
                if (player.getItemInHand(MAIN_HAND).getItem() instanceof GunItem) {
                    ReloadGunPacket msg = new ReloadGunPacket();
                    FnGPacketHandler.sendToServer(msg);
                }
                if (player.getItemInHand(Hand.MAIN_HAND).getItem() instanceof ScepterItem || player.getItemInHand(Hand.OFF_HAND).getItem() instanceof ScepterItem) {
                    RemoveMinionsOfUUIDPacket msg = new RemoveMinionsOfUUIDPacket();
                    FnGPacketHandler.sendToServer(msg);
                }
            }
        }

        if (event.getAction() == GLFW.GLFW_PRESS) {
            if (FnGClientRegistry.KEY_BADGE_1.getKey().getValue() == event.getKey() && FnGClientRegistry.KEY_BADGE_1.isDown()) {
                useSortedAbilityBadge(player, 1);
            }
            if (FnGClientRegistry.KEY_BADGE_2.getKey().getValue() == event.getKey() && FnGClientRegistry.KEY_BADGE_2.isDown()) {
                useSortedAbilityBadge(player, 2);
            }
            if (FnGClientRegistry.KEY_BADGE_3.getKey().getValue() == event.getKey() && FnGClientRegistry.KEY_BADGE_3.isDown()) {
                useSortedAbilityBadge(player, 3);
            }
        }
    }

    public static void useSortedAbilityBadge(PlayerEntity player, int order) {
        CuriosApi.getCuriosHelper()
            .getCuriosHandler(player)
            .resolve()
            .flatMap(handler -> handler.getStacksHandler("badge"))
            .ifPresent(handler -> {
                int o = 0;
                for (int i = 0; i < handler.getSlots(); i++) {
                    ItemStack s = handler.getStacks().getStackInSlot(i);
                    if (s.getItem() instanceof BadgeItem) {
                        BadgeItem badgeItem = (BadgeItem) s.getItem();
                        if (badgeItem.hasKeyBind()) {
                            o++;
                            if (o >= order) {
                                UseBadgePacket packet = new UseBadgePacket(s);
                                FnGPacketHandler.sendToServer(packet);
                                badgeItem.useBadge(player, s);
                                break;
                            }
                        }
                    }
                }
            });
    }
}
