package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.block.VaultBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.EnderPearlEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.ProjectileImpactEvent;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class EnderPearlHandler {

    @SubscribeEvent
    public static void onProjectileImpact(ProjectileImpactEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof EnderPearlEntity && event.getRayTraceResult().getType() == RayTraceResult.Type.BLOCK) {
            BlockRayTraceResult result = (BlockRayTraceResult) event.getRayTraceResult();
            Block block = event.getEntity().getCommandSenderWorld().getBlockState(result.getBlockPos()).getBlock();
            if (block instanceof VaultBlock) {
                event.setCanceled(true);
                // Old code that causes the ender pearl to bounce off the vault block instead of just being removed.
                // Decided against it because it's buggy and can cause the pearl to go through the block instead of bouncing.
                // Might come back to this some time
//                Vector3d motion = entity.getDeltaMovement();
//                switch (result.getDirection()) {
//                    case NORTH:
//                    case SOUTH: {
//                        entity.setDeltaMovement(motion.x, motion.y, -motion.z);
//                        break;
//                    }
//                    case EAST:
//                    case WEST: {
//                        entity.setDeltaMovement(-motion.x, motion.y, motion.z);
//                        break;
//                    }
//                    case UP:
//                    case DOWN: {
//                        entity.setDeltaMovement(motion.x, -motion.y, motion.z);
//                        break;
//                    }
//                }
                entity.remove();
            }
        }
    }
}
