package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.items.BadgeItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import top.theillusivec4.curios.api.CuriosApi;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class PlayerDeathHandler {

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event) {
        PlayerEntity player = event.getPlayer();
        CuriosApi.getCuriosHelper().getEquippedCurios(player).ifPresent(handler -> {
            int slots = CuriosApi.getSlotHelper().getSlotsForType(player, "badge");
            for (int i = 0; i < slots; i++) {
                ItemStack stack = handler.getStackInSlot(i);
                if (stack.getItem() instanceof BadgeItem) {
                    BadgeItem item = (BadgeItem) stack.getItem();
                    CuriosApi.getSlotHelper().growSlotType("badge", item.getSize() - 1, player);
                }
            }
        });
    }
}
