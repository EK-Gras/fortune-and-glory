package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.mixin.accessors.MinecraftAccessor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;

import javax.annotation.Nullable;
import java.util.Objects;

public class RaytraceUtils {

    @Nullable
    public static EntityRayTraceResult getCrosshairTargetEntity(float range, Vector3d lookVector) {
        Minecraft mc = Minecraft.getInstance();
        MinecraftAccessor mcAccess = (MinecraftAccessor) mc;
        LivingEntity entity = (LivingEntity) Objects.requireNonNull(mc.cameraEntity);
        Vector3d eyePosition = entity.getEyePosition(mcAccess.getTimer().partialTick);
        Vector3d traceEnd = eyePosition.add(lookVector.x * range, lookVector.y * range, lookVector.z * range);
        AxisAlignedBB boundingBox = entity.getBoundingBox().expandTowards(lookVector.scale(range))
            .inflate(1.0D, 1.0D, 1.0D);
        return ProjectileHelper.getEntityHitResult(entity, eyePosition, traceEnd, boundingBox,
            (e) -> !e.isSpectator() && e.isPickable(), range * range);
    }

}
