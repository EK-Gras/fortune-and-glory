package com.gitlab.ekgras.fortuneandglory.util;

public class IterationUtils {

    /**
     * Iterates every adjacent point to one in 3d space
     * @param consumer The consumer to call with each iteration
     */
    public static void iterateAdjacent3d(int originX, int originY, int originZ,
                                         IntTernaryConsumer consumer) {

        consumer.accept(originX + 1, originY, originZ);
        consumer.accept(originX - 1, originY, originZ);
        consumer.accept(originX, originY + 1, originZ);
        consumer.accept(originX, originY - 1, originZ);
        consumer.accept(originX, originY, originZ + 1);
        consumer.accept(originX, originY, originZ - 1);
    }

    /**
     * Iterates 3d space from the start position (inclusive) to the end position (inclusive). End
     * position must always be greater than start position, otherwise they will be swapped.
     *
     * @param consumer The consumer to call with each iteration
     */
    public static void iterate3d(int startX, int startY, int startZ, int endX, int endY, int endZ,
                                 IntTernaryConsumer consumer) {
        // Ensure that the start positions aren't greater than the end positions
        int temp;
        if (startX > endX) {
            temp = startX;
            startX = endX;
            endX = temp;
        }
        if (startY > endY) {
            temp = startY;
            startY = endY;
            endY = temp;
        }
        if (startZ > endZ) {
            temp = startZ;
            startZ = endZ;
            endZ = temp;
        }

        // Iterate
        for (int x = startX; x <= endX; x++) {
            for (int y = startY; y <= endY; y++) {
                for (int z = startZ; z <= endZ; z++) {
                    consumer.accept(x, y, z);
                }
            }
        }
    }

    /**
     * Iterate every point around one in 3d space.
     *
     * @param consumer The consumer to call with each iteration
     */
    public static void expand3d(int originX, int originY, int originZ,
                                IntTernaryConsumer consumer) {

        expand3d(originX, originY, originZ, 1, consumer);
    }

    /**
     * Iterate every point around one in 3d space.
     *
     * @param toExpand The amount to expand about the origin position
     * @param consumer The consumer to call with each iteration
     */
    public static void expand3d(int originX, int originY, int originZ, int toExpand,
                                IntTernaryConsumer consumer) {

        iterate3d(originX - toExpand, originY - toExpand, originZ - toExpand,
                originX + toExpand, originY + toExpand, originZ + toExpand, consumer);
    }

    /**
     * Iterates 2d space from the start position (inclusive) to the end position (inclusive). End
     * position must always be greater than start position, otherwise they will be swapped.
     *
     * @param consumer The consumer to call with each iteration
     */
    public static void iterate2d(int startX, int startY, int endX, int endY,
                                 IntBinaryConsumer consumer) {

        int temp;
        if (startX > endX) {
            temp = startX;
            startX = endX;
            endX = temp;
        }
        if (startY > endY) {
            temp = startY;
            startY = endY;
            endY = temp;
        }

        for (int x = startX; x <= endX; x++) {
            for (int y = startY; y <= endY; y++) {
                consumer.accept(x, y);
            }
        }
    }

    /**
     * Iterate every point around one in 2d space.
     *
     * @param consumer The consumer to call with each iteration
     */
    public static void expand2d(int originX, int originY, IntBinaryConsumer consumer) {
        expand2d(originX, originY, 1, consumer);
    }

    /**
     * Iterate every point around one in 2d space.
     *
     * @param toExpand The amount to expand about the origin position
     * @param consumer The consumer to call with each iteration
     */
    public static void expand2d(int originX, int originY, int toExpand,
                                IntBinaryConsumer consumer) {
        iterate2d(originX - toExpand, originY - toExpand, originX + toExpand,
                originY + toExpand, consumer);
    }

    @FunctionalInterface
    public interface IntTernaryConsumer {

        void accept(int x, int y, int z);

    }

    @FunctionalInterface
    public interface IntBinaryConsumer {

        void accept(int x, int y);

    }

}
