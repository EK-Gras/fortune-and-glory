package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

public class TickScheduler {
    /**
     * Ticks passed since this class was loaded. DO NOT MODIFY
     */
    private static volatile long serverTicksPassed = 0;
    private static volatile long clientTicksPassed = 0;

    // This data structure is important to ensure
    // A) Thread-safety
    // B) We only iterate the events that need to be processed, not all of them
    // events are sorted by the time they should be executed
    public static ConcurrentSkipListSet<TickScheduledEvent> serverEvents = new ConcurrentSkipListSet<>();
    public static ConcurrentSkipListSet<TickScheduledEvent> clientEvents = new ConcurrentSkipListSet<>();

    private static void scheduleInternal(long ticks, Runnable runnable, boolean clientSide) {
        TickScheduledEvent event = new TickScheduledEvent(runnable, serverTicksPassed + ticks);
        if (clientSide)
            clientEvents.add(event);
        else
            serverEvents.add(event);
    }

    /**
     * Schedule a runnable to execute on the server ticking thread
     *
     * @param runnable The runnable to execute
     * @return A CompletableFuture that completes when the specified duration has passed
     */
    public static CompletableFuture<Void> schedule(long ticks, boolean clientSide, Runnable runnable) {
        return CompletableFuture.runAsync(runnable, createServerExecutor(ticks, clientSide));
    }

    /**
     * Schedule a supplier to execute on the server ticking thread
     *
     * @param supplier The supplier to execute
     * @return A CompletableFuture that completes with the results of the supplier
     * when the specified duration has passed
     */
    public static <T> CompletableFuture<T> schedule(long ticks, boolean clientSide, Supplier<T> supplier) {
        return CompletableFuture.supplyAsync(supplier, createServerExecutor(ticks, clientSide));
    }

    public static <T> CompletableFuture<T> schedule(Duration duration, boolean clientSide, Supplier<T> supplier) {
        return schedule(durationToTicks(duration), clientSide, supplier);
    }

    public static CompletableFuture<Void> schedule(Duration duration, boolean clientSide, Runnable runnable) {
        return schedule(durationToTicks(duration), clientSide, runnable);
    }

    /**
     * Create an executor that executes any runnable on the server ticking thread
     */
    private static Executor createServerExecutor(long ticks, boolean clientSide) {
        return runnable -> scheduleInternal(ticks, runnable, clientSide);
    }

    private static long durationToTicks(Duration duration) {
        return duration.toMillis() / 50;
    }

    public static class TickScheduledEvent implements Comparable<TickScheduledEvent> {
        public final Runnable runnable;
        public final long tickNumber;

        public TickScheduledEvent(Runnable runnable, long tickNumber) {
            this.runnable = runnable;
            this.tickNumber = tickNumber;
        }

        // This means that TickScheduledEvents are sorted by the tick number
        @Override
        public int compareTo(TickScheduledEvent o) {
            return Long.compare(this.tickNumber, o.tickNumber);
        }
    }


    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class TickSchedulerEvents {

        @SubscribeEvent
        @SuppressWarnings("NonAtomicOperationOnVolatileField")
        public static void onTick(TickEvent.ClientTickEvent tickEvent) {
            // only trigger on the start of a tick
            if (tickEvent.phase == TickEvent.Phase.END) {
                return;
            }
            // non-atomic operation is safe as long is ticksPassed is never modified elsewhere
            clientTicksPassed++;
            runEventsBefore(clientEvents, clientTicksPassed);
        }

        @SubscribeEvent
        @SuppressWarnings("NonAtomicOperationOnVolatileField")
        public static void onTick(TickEvent.ServerTickEvent tickEvent) {
            // only trigger on the start of a tick
            if (tickEvent.phase == TickEvent.Phase.END) {
                return;
            }
            // non-atomic operation is safe as long is ticksPassed is never modified elsewhere
            serverTicksPassed++;
            runEventsBefore(serverEvents, serverTicksPassed);
        }

        private static void runEventsBefore(Set<TickScheduledEvent> events, long ticksPassed) {
            // iterate over events in queue starting from the earliest
            // going to the latest
            for (Iterator<TickScheduledEvent> iter = events.iterator(); iter.hasNext(); ) {
                TickScheduledEvent event = iter.next();
                // If the earliest event is scheduled to pass this tick
                if (ticksPassed >= event.tickNumber) {
                    // remove it
                    iter.remove();
                    // run it
                    event.runnable.run();
                } else {
                    // the earliest is scheduled to run later
                    break;
                }
            }
        }

    }

}
