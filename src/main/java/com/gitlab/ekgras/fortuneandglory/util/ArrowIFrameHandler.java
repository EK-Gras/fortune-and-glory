package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.BulletEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class ArrowIFrameHandler {

    @SubscribeEvent
    public static void onArrowHitEntity(LivingHurtEvent event) {
        // If the entity was hit by an arrow of any kind...
        if (event.getSource().getDirectEntity() instanceof AbstractArrowEntity) {
            // And the arrow was critical...
            if (((AbstractArrowEntity) event.getSource().getDirectEntity()).isCritArrow()) {
                // Set their invincibility frames to 0
                event.getEntityLiving().invulnerableTime = 0;
            }
        }
    }
}
