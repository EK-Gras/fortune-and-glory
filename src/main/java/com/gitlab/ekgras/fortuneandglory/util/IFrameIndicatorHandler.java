package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;

import static java.lang.String.valueOf;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, value = Dist.CLIENT)
public class IFrameIndicatorHandler {

    // Might want to come back to this later. Originally intended as a debug feature but may be interesting to implement
    // later as a meter or something.

    @SubscribeEvent
    public static void onRenderHotBar(RenderGameOverlayEvent.Post event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.CROSSHAIRS) {
            Minecraft mc = Minecraft.getInstance();
            PlayerEntity player = mc.player;
            int iFrames = player.invulnerableTime;
            String iFrameIndicator = (valueOf(player.invulnerableTime));
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            MatrixStack m = new MatrixStack();
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            int i = (width - renderer.width(iFrameIndicator)) / 2;
            if (iFrames != 0) {
                renderer.drawShadow(m, iFrameIndicator, i, height / 2f - 16, 5636095);
            }
        }
    }
}
