package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class StoreBlockFaceMinedHandler {
    private static final Map<UUID,Direction> HIT_FACE = new HashMap<>();

    /** Initializies this listener */
    public static void init() {
    }

    /** Called when the player left clicks a block to store the face */
    @SubscribeEvent
    public static void onLeftClickBlock(PlayerInteractEvent.LeftClickBlock event) {
        HIT_FACE.put(event.getPlayer().getUUID(), event.getFace());
    }

    /** Called when a player leaves the server to clear the face */
    @SubscribeEvent
    public static void onLeaveServer(PlayerEvent.PlayerLoggedOutEvent event) {
        HIT_FACE.remove(event.getPlayer().getUUID());
    }

    /**
     * Gets the side this player last hit, should return correct values in most modifier hooks related to block breaking
     * @param player  Player
     * @return  Side last hit
     */
    public static Direction getSideHit(PlayerEntity player) {
        return HIT_FACE.getOrDefault(player.getUUID(), Direction.UP);
    }
}
