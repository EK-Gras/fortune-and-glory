package com.gitlab.ekgras.fortuneandglory.util;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.init.FnGClientRegistry;
import com.gitlab.ekgras.fortuneandglory.items.*;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.CuriosCapability;
import top.theillusivec4.curios.api.type.capability.ICuriosItemHandler;
import top.theillusivec4.curios.api.type.inventory.ICurioStacksHandler;

import java.util.Locale;

import static java.lang.String.valueOf;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class ClientRenderEventHandler {

    @SubscribeEvent
    public static void renderGunOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof GunItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            GunItem gun = (GunItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
            CompoundNBT tag = itemStack.getTag();
            String bulletIndicator = (itemStack.getTag().getInt("bulletsLoaded") + "/" + gun.getCapacity());
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            int widthCentered = (width - renderer.width(bulletIndicator)) / 2;
            MatrixStack m = new MatrixStack();
            if ((!tag.getBoolean("isCooldown") && !tag.getBoolean("isReloading")) || (tag.getBoolean("isCooldown") && gun.isAutomatic() && !tag.getBoolean("isReloading"))) {
                renderer.drawShadow(m, bulletIndicator, widthCentered, height / 2f + 18, 5635925);
            }
            else if (tag.getBoolean("isReloading") || tag.getInt("reloadCooldown") > 0) {
                renderer.drawShadow(m, bulletIndicator, widthCentered, height / 2f + 18, 16733525);
            }
            else if (tag.getBoolean("isCooldown")) {
                renderer.drawShadow(m, bulletIndicator, widthCentered, height / 2f + 18, 16777045);
            }
        }
    }

    @SubscribeEvent
    public static void renderChargingOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof ChargingItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            ChargingItem item = (ChargingItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
            CompoundNBT tag = itemStack.getTag();
            String chargeIndicator = (itemStack.getTag().getInt("hits") + "/" + item.getMaxCharge());
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            MatrixStack m = new MatrixStack();
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            int centeredWidth = (width - renderer.width(chargeIndicator)) / 2;
            int color = tag.getBoolean("charged") ? 5636095 : 16777215;
            renderer.drawShadow(m, chargeIndicator, centeredWidth, height / 2f + 18, color);
        }
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.OFFHAND).getItem() instanceof ChargingItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.OFFHAND);
            ChargingItem item = (ChargingItem) player.getItemBySlot(EquipmentSlotType.OFFHAND).getItem();
            CompoundNBT tag = itemStack.getTag();
            String chargeIndicator = (itemStack.getTag().getInt("hits") + "/" + item.getMaxCharge());
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            MatrixStack m = new MatrixStack();
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            int centeredWidth = (width - renderer.width(chargeIndicator)) / 2;
            int color = tag.getBoolean("charged") ? 5636095 : 16777215;
            renderer.drawShadow(m, chargeIndicator, centeredWidth, height / 2f - 27, color);
        }
    }

    @SubscribeEvent
    public static void renderRapierOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof RapierItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            RapierItem item = (RapierItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
            CompoundNBT tag = itemStack.getTag();
            String chargeIndicator = (itemStack.getTag().getInt("hits") + "/" + item.getMaxCharge());
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            MatrixStack m = new MatrixStack();
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            int i = (width - renderer.width(chargeIndicator)) / 2;
            if (!tag.getBoolean("charged")) {
                renderer.drawShadow(m, chargeIndicator, i, height / 2f + 18, 16777215);
            }
            else if (tag.getBoolean("charged")) {
                renderer.drawShadow(m, chargeIndicator, i, height / 2f + 18, 5636095);
            }
        }
    }

    @SubscribeEvent
    public static void renderComboOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        // If this event is the one rendering the hotbar, and the player is holding a ComboItem in their mainhand...
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof ComboItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            CompoundNBT tag = itemStack.getTag();
            int currentCombo = tag.getInt("currentCombo");
            ComboItem ci = (ComboItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
            // If the ComboItem has an active combo (currentCombo is greater than 0)...
            if (currentCombo > 0 && currentCombo <= ci.getMaxCombo()) {
                // Convert the current combo into a string using valueOf
                String comboIndicator = valueOf(currentCombo);
                FontRenderer renderer = mc.font;
                MainWindow window = event.getWindow();
                MatrixStack m = new MatrixStack();
                // Get the width and height of the window
                int height = window.getGuiScaledHeight();
                int width = window.getGuiScaledWidth();
                // Subtract the width of the window by the width of the string, then divide by 2. This is how the string is centered in the middle of the screen.
                int i = ((width - renderer.width(comboIndicator)) / 2);
                // Render the string in the horizontal center of the screen and 18 pixels below the vertical center. It's rendered lower so that it doesn't overlap with the attack cooldown indicator.
                renderer.drawShadow(m, comboIndicator, i, height / 2f + 18, currentCombo >= ci.getMaxCombo() ? 16733525 : 16777045);
            }
        }
    }

    @SubscribeEvent
    public static void renderGauntletOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        // If this event is the one rendering the hotbar, and the player is holding a ComboItem in their mainhand...
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof GauntletItem) {
            ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
            // If the ComboItem has an active combo (currentCombo is greater than 0)... (Side-note: this is on a separate line from the other if statement to prevent a NullPointerException by
            // attempting to access a data tag that doesn't exist, in the event that the player is NOT holding a ComboItem).
            CompoundNBT tag = itemStack.getTag();
            int currentCombo = tag.getInt("currentCombo");
            GauntletItem ci = (GauntletItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
            if (currentCombo > 0 && currentCombo <= ci.getMaxCombo()) {
                // Convert the current combo into a string using valueOf
                String comboIndicator = valueOf(currentCombo);
                FontRenderer renderer = mc.font;
                MainWindow window = event.getWindow();
                MatrixStack m = new MatrixStack();
                // Get the width and height of the window
                int height = window.getGuiScaledHeight();
                int width = window.getGuiScaledWidth();
                // Subtract the width of the window by the width of the string, then divide by 2. This is how the string is centered in the middle of the screen.
                int i = ((width - renderer.width(comboIndicator)) / 2);
                // Render the string in the horizontal center of the screen and 18 pixels below the vertical center. It's rendered lower so that it doesn't overlap with the attack cooldown indicator.
                renderer.drawShadow(m, comboIndicator, i, height / 2f + 18, currentCombo >= ci.getMaxCombo() ? 16733525 : 16777045);
            }
        }
    }

    @SubscribeEvent
    public static void renderEnergyOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getInstance();
        PlayerEntity player = mc.player;
        boolean mainHand = player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem() instanceof EnergyItem;
        boolean offHand = player.getItemBySlot(EquipmentSlotType.OFFHAND).getItem() instanceof EnergyItem;
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && (mainHand || offHand)) {
            FontRenderer renderer = mc.font;
            MainWindow window = event.getWindow();
            MatrixStack m = new MatrixStack();
            // Get the width and height of the window
            int height = window.getGuiScaledHeight();
            int width = window.getGuiScaledWidth();
            if (mainHand && !offHand) {
                ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
                CompoundNBT tag = itemStack.getTag();
                float energy = tag.getInt("energy");
                EnergyItem ei = (EnergyItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
                // Convert the energy value into a string
                String energyIndicator = energy + "%";
                // Subtract the width of the window by the width of the string, then divide by 2. This is how the string is centered in the middle of the screen.
                int widthCentered = ((width - renderer.width(energyIndicator)) / 2);
                // Render the string in the horizontal center of the screen and 18 pixels below the vertical center. It's rendered lower so that it doesn't overlap with the attack cooldown indicator.
                renderer.drawShadow(m, energyIndicator, widthCentered, height / 2f + 18, 5636095);
            }
            else if (!mainHand) {
                ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.OFFHAND);
                CompoundNBT tag = itemStack.getTag();
                float energy = tag.getInt("energy");
                EnergyItem ei = (EnergyItem) player.getItemBySlot(EquipmentSlotType.OFFHAND).getItem();
                // Convert the energy value into a string
                String energyIndicator = energy + "%";
                // Subtract the width of the window by the width of the string, then divide by 2. This is how the string is centered in the middle of the screen.
                int widthCentered = ((width - renderer.width(energyIndicator)) / 2);
                // Render the string in the horizontal center of the screen and 18 pixels below the vertical center. It's rendered lower so that it doesn't overlap with the attack cooldown indicator.
                renderer.drawShadow(m, energyIndicator, widthCentered, height / 2f - 27, 5636095);
            }
            else {
                ItemStack itemStack1 = player.getItemBySlot(EquipmentSlotType.MAINHAND);
                CompoundNBT tag1 = itemStack1.getTag();
                float energy1 = tag1.getInt("energy");
                EnergyItem ei1 = (EnergyItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();

                ItemStack itemStack2 = player.getItemBySlot(EquipmentSlotType.OFFHAND);
                CompoundNBT tag2 = itemStack2.getTag();
                float energy2 = tag2.getInt("energy");
                EnergyItem ei2 = (EnergyItem) player.getItemBySlot(EquipmentSlotType.OFFHAND).getItem();

                // Convert the energy value into a string
                boolean handSide = player.getMainArm() == HandSide.LEFT;
                String energyIndicatorMain = energy1 + "%";
                String energyIndicatorOff = energy2 + "%";

                // Subtract the width of the window by the width of the string, then divide by 2. This is how the string is centered in the middle of the screen.
                int widthCenteredMain = ((width - renderer.width(energyIndicatorMain)) / 2);
                int widthCenteredOff = ((width - renderer.width(energyIndicatorOff)) / 2);

                // Render the string in the horizontal center of the screen and 18 pixels below the vertical center. It's rendered lower so that it doesn't overlap with the attack cooldown indicator.
                renderer.drawShadow(m, energyIndicatorMain, widthCenteredMain, height / 2f + 18, 5636095);
                renderer.drawShadow(m, energyIndicatorOff, widthCenteredOff, height / 2f - 27, 5636095);
            }
        }
    }

    @SubscribeEvent
    public static void renderBadgeOverlay (RenderGameOverlayEvent.Post event) {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && player.isAlive()) {
            if (CuriosApi.getCuriosHelper().getCuriosHandler(player).isPresent()) {
                if (CuriosApi.getCuriosHelper().getCuriosHandler(player).orElseThrow(NullPointerException::new).getStacksHandler("badge").isPresent()) {
                    ICurioStacksHandler handler = CuriosApi.getCuriosHelper().getCuriosHandler(player).orElseThrow(NullPointerException::new).getStacksHandler("badge").orElseThrow(NullPointerException::new);
                    int o = 0;
                    for (int i = 0; i < handler.getSlots(); i++) {
                        ItemStack s = handler.getStacks().getStackInSlot(i);
                        if (s.getItem() instanceof BadgeItem) {
                            BadgeItem badgeItem = (BadgeItem) s.getItem();
                            if (badgeItem.hasKeyBind()) {
                                String oKey;
                                switch (o) {
                                    case 0:
                                        oKey = FnGClientRegistry.KEY_BADGE_1.getTranslatedKeyMessage().getString();
                                        break;
                                    case 1:
                                        oKey = FnGClientRegistry.KEY_BADGE_2.getTranslatedKeyMessage().getString();
                                        break;
                                    case 2:
                                        oKey = FnGClientRegistry.KEY_BADGE_3.getTranslatedKeyMessage().getString();
                                        break;
                                    default:
                                        oKey = "?";
                                        break;
                                }
                                String key = oKey.length() > 0 ? oKey.substring(0, 1).toUpperCase(Locale.ROOT) + oKey.substring(1) : "?";
                                ItemRenderer renderer = minecraft.getItemRenderer();
                                FontRenderer fontRenderer = minecraft.font;
                                MainWindow window = event.getWindow();
                                int height = window.getGuiScaledHeight();
                                int width = window.getGuiScaledWidth();
                                MatrixStack m = new MatrixStack();
                                renderer.renderAndDecorateItem(s, 4, o * 20 + 4);
                                renderer.renderGuiItemDecorations(fontRenderer, s, 4, o * 20 + 4);
                                fontRenderer.drawShadow(m, key, 22, o * 20 + 8, 16777045);
                                o++;
                            }
                        }
                    }
                }
            }
        }
    }
}
