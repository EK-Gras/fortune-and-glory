package com.gitlab.ekgras.fortuneandglory;

import com.gitlab.ekgras.fortuneandglory.command.CommandRegistry;
import com.gitlab.ekgras.fortuneandglory.init.*;
import com.gitlab.ekgras.fortuneandglory.network.FnGPacketHandler;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartedEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("fortune-and-glory")
@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
public class FortuneAndGlory {

    public static final String MODID = "fortune-and-glory";

    public static final Logger LOGGER = LogManager.getLogger(MODID);

    public FortuneAndGlory() {
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        FnGEnchantments.ENCHANTMENTS.register(modEventBus);
        FnGBlocks.BLOCKS.register(modEventBus);
        FnGTileEntities.TILE_ENTITY_TYPES.register(modEventBus);
        FnGEntities.ENTITY_TYPES.register(modEventBus);
        FnGSounds.SOUND_EVENTS.register(modEventBus);
        FnGEffects.EFFECTS.register(modEventBus);
        FnGItems.ITEMS.register(modEventBus);
        FnGParticles.PARTICLE_TYPES.register(modEventBus);
        LOGGER.debug("Fortune and Glory present and active.");
        FnGPacketHandler.RegisterMessages();
    }

    @SubscribeEvent
    public static void onServerStart(FMLServerStartedEvent event) {
        CommandRegistry.registerCommands(event.getServer().getCommands().getDispatcher());
    }

}
