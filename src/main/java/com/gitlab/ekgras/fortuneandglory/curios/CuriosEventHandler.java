package com.gitlab.ekgras.fortuneandglory.curios;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import top.theillusivec4.curios.api.SlotTypeMessage;

@Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CuriosEventHandler {
    @SubscribeEvent
    public static void onCreateCurioTypes(InterModEnqueueEvent event) {
        InterModComms.sendTo("curios", SlotTypeMessage.REGISTER_TYPE, () ->
                new SlotTypeMessage.Builder("badge").size(6).icon(new ResourceLocation(FortuneAndGlory.MODID, "item/empty_badge_slot")).build()
        );
    }
}
