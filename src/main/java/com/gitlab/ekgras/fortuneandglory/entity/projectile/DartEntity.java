package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.abilities.ApplyEffectAbility;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.DartItem;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Rotations;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.List;
import java.util.Map;

public class DartEntity extends FnGArrowEntity implements IEntityAdditionalSpawnData {

    private static final DataParameter<Rotations> INITIAL_MOTION = EntityDataManager.defineId(FnGArrowEntity.class, DataSerializers.ROTATIONS);
    private BlockState inBlockState;
    private IntOpenHashSet piercedEntities;
    private List<Entity> hitEntities;
    private int life;
    private int despawnTime;
    private Effect effect;
    private int strength;
    private int duration;
    private ItemStack stack;

    public DartEntity(EntityType<? extends FnGArrowEntity> entityIn, World worldIn) {
        super(entityIn, worldIn);
    }

    public DartEntity(LivingEntity shooterIn, World worldIn, Effect effectIn, int strengthIn, int durationIn, ItemStack stackIn) {
        super(FnGEntities.DART.get(), shooterIn, worldIn, false, false, 1200, true, 0.6F, 1, true, false, false);
        this.effect = effectIn;
        this.strength = strengthIn;
        this.duration = durationIn;
        this.stack = stackIn;
    }

    public void writeSpawnData(PacketBuffer buffer) {
        super.writeSpawnData(buffer);
        buffer.writeRegistryId(effect);
        buffer.writeInt(strength);
        buffer.writeInt(duration);
    }

    public void readSpawnData(PacketBuffer buffer) {
        super.readSpawnData(buffer);
        this.effect = buffer.readRegistryId();
        this.strength = buffer.readInt();
        this.duration = buffer.readInt();
    }

    public void shoot(Entity shooter, float pitch, float yaw, float velocity, float inaccuracy) {
        float x = -MathHelper.sin(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        float y = -MathHelper.sin(pitch * ((float)Math.PI / 180F));
        float z = MathHelper.cos(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        Map<Enchantment, Integer> enchants = this.getItemEnchantments();
        if (enchants.containsKey(Enchantments.PUNCH_ARROWS)) this.setKnockback(enchants.get(Enchantments.PUNCH_ARROWS) - 1);
        this.shoot(x, y, z, velocity, inaccuracy);
    }

    @Override
    public void doPostHurtEffects(LivingEntity entity) {
        super.doPostHurtEffects(entity);
        EffectInstance instance = new EffectInstance(effect, duration, strength, false, true, true);
        entity.addEffect(instance);
    }

    public ItemStack getPickupItem() {
        return ItemStack.EMPTY;
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
