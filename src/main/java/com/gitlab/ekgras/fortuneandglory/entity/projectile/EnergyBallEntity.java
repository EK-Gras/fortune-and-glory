package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.IParticleData;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.Map;

public class EnergyBallEntity extends FnGArrowEntity implements IEntityAdditionalSpawnData {
    private static final DataParameter<Rotations> INITIAL_MOTION = EntityDataManager.defineId(EnergyBallEntity.class, DataSerializers.ROTATIONS);
    private BlockState lastState;
    private IParticleData particleData;
    private SoundEvent soundEvent = SoundEvents.SHULKER_BULLET_HIT;
    private int life;
    private int despawnTime;
    private boolean invisible = false;

    public EnergyBallEntity(EntityType<? extends FnGArrowEntity> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }

    public EnergyBallEntity(EntityType type, LivingEntity shooter, World worldIn, boolean noGravity, boolean constantMomentum, int life, IParticleData particleIn, boolean invisible, boolean shouldKnockback) {
        super(type, shooter, worldIn, constantMomentum, noGravity, life, true, 0.6F, 2.0D, true, shouldKnockback, false);
        this.setSoundEvent(soundEvent);
        this.particleData = particleIn;
        this.invisible = invisible;
    }

    public EnergyBallEntity(EntityType type, double x, double y, double z, World worldIn) {
        super(type, x, y, z, worldIn);
    }

    public EnergyBallEntity(World worldIn, LivingEntity shooter) {
        super(FnGEntities.ENERGY_BALL.get(), shooter, worldIn);
    }

    @Override
    public boolean isInvisible() {
        return invisible;
    }

    @Override
    public boolean displayFireAnimation() {
        return !this.invisible && this.isOnFire();
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer) {
        super.writeSpawnData(buffer);
        buffer.writeRegistryId(particleData.getType());
        buffer.writeBoolean(invisible);
    }

    @Override
    public void readSpawnData(PacketBuffer buffer) {
        super.readSpawnData(buffer);
        this.particleData = buffer.readRegistryId();
        this.invisible = buffer.readBoolean();
    }

    public void shoot(Entity shooter, float pitch, float yaw, float velocity, float inaccuracy, double damage, SoundEvent soundIn) {
        float x = -MathHelper.sin(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        float y = -MathHelper.sin(pitch * ((float)Math.PI / 180F));
        float z = MathHelper.cos(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        this.shoot(x, y, z, velocity, inaccuracy, damage, soundIn);
    }

    public void shoot(float x, float y, float z, float velocity, float inaccuracy, double damage, SoundEvent soundIn) {
        this.pickup = PickupStatus.DISALLOWED;
        this.setBaseDamage(damage);
        this.soundEvent = soundIn;
        Map<Enchantment, Integer> enchants = this.getItemEnchantments();
        if (enchants.containsKey(Enchantments.PUNCH_ARROWS)) this.setKnockback(enchants.get(Enchantments.PUNCH_ARROWS) - (this.shouldKnockback() ? 0 : 1));
        this.shoot(x, y, z, velocity, inaccuracy);
    }

    @Override
    public ItemStack getPickupItem() {
        return ItemStack.EMPTY;
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    protected SoundEvent getDefaultHitGroundSoundEvent() {
        return soundEvent;
    }

    public void tick() {
        super.tick();

        if (this.level.isClientSide && !this.inGround) {
            this.level.addParticle(this.particleData, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D, 0.0D);
        }
    }
}
