package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.mixin.accessors.ArrowEntityAccessor;
import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.server.SChangeGameStatePacket;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

import javax.annotation.Nullable;
import java.util.*;

public abstract class FnGArrowEntity extends AbstractArrowEntity implements IEntityAdditionalSpawnData, IEnchantmentProjectileData {
    @Nullable
    protected boolean inGround;
    public AbstractArrowEntity.PickupStatus pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
    private int life;
    private double baseDamage;
    private SoundEvent soundEvent = this.getDefaultHitGroundSoundEvent();
    private int despawnTime;
    private boolean despawnInAir = true;
    private float waterInertia;
    private boolean fragile = true;
    private boolean shouldKnockback = true;
    private boolean isArrow;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    protected FnGArrowEntity(EntityType<? extends FnGArrowEntity> typeIn, World worldIn) {
        super(typeIn, worldIn);
    }

    protected FnGArrowEntity(EntityType<? extends FnGArrowEntity> typeIn, double xIn, double yIn, double zIn, World worldIn) {
        this(typeIn, worldIn);
        this.setPos(xIn, yIn, zIn);
    }

    protected FnGArrowEntity(EntityType<? extends FnGArrowEntity> typeIn, LivingEntity shooterIn, World worldIn) {
        this(typeIn, shooterIn.getX(), shooterIn.getEyeY() - (double)0.1F, shooterIn.getZ(), worldIn);
        this.setOwner(shooterIn);
        this.setNoPhysics(false);
        this.despawnTime = 1200;
        this.despawnInAir = false;
        this.waterInertia = 0.6F;
        this.baseDamage = 2.0D;
        this.fragile = false;
        if (shooterIn instanceof PlayerEntity) {
            this.pickup = AbstractArrowEntity.PickupStatus.ALLOWED;
        }
    }

    protected FnGArrowEntity(EntityType<? extends FnGArrowEntity> typeIn, LivingEntity shooterIn, World worldIn,
                             boolean noPhysicsIn, boolean noGravityIn, int despawnTimeIn, boolean despawnInAirIn, float waterInertiaIn,
                             double damageIn, boolean fragileIn, boolean shouldKnockbackIn, boolean isArrow) {
        this(typeIn, shooterIn.getX(), shooterIn.getEyeY() - (double)0.1F, shooterIn.getZ(), worldIn);
        this.setOwner(shooterIn);
        this.setNoPhysics(noPhysicsIn);
        this.setNoGravity(noGravityIn);
        this.despawnTime = despawnTimeIn;
        this.despawnInAir = despawnInAirIn;
        this.waterInertia = waterInertiaIn;
        this.baseDamage = damageIn;
        this.fragile = fragileIn;
        this.shouldKnockback = shouldKnockbackIn;
        if (shooterIn instanceof PlayerEntity) {
            this.pickup = AbstractArrowEntity.PickupStatus.ALLOWED;
        }
        this.isArrow = isArrow;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeInt(despawnTime);
        buffer.writeDouble(baseDamage);
        buffer.writeBoolean(despawnInAir);
        buffer.writeBoolean(fragile);
        buffer.writeBoolean(shouldKnockback);
    }

    public void readSpawnData(PacketBuffer buffer) {
        this.despawnTime = buffer.readInt();
        this.baseDamage = buffer.readDouble();
        this.despawnInAir = buffer.readBoolean();
        this.fragile = buffer.readBoolean();
        this.shouldKnockback = buffer.readBoolean();
    }

    public void setBaseDamage(double baseDamage) {
        this.baseDamage = baseDamage;
    }

    public void setSoundEvent(SoundEvent sound) {
        super.setSoundEvent(sound);
        this.soundEvent = sound;
    }

    public boolean shouldKnockback() {
        return this.shouldKnockback;
    }

    public void setShouldKnockback(boolean shouldKnockback) {
        this.shouldKnockback = shouldKnockback;
    }

    public void tick() {
        super.tick();
        if (this.despawnInAir) {
            this.tickDespawn();
        }
    }

    @Override
    protected void tickDespawn() {
        ++this.life;
        if (this.life >= despawnTime) {
            this.remove();
        }
    }

    public DamageSource getDamageSource(Entity hitEntity) {
        Entity owner = this.getOwner();
        DamageSource damagesource;
        if (owner == null) {
            damagesource = DamageSource.arrow(this, this);
        } else {
            damagesource = DamageSource.arrow(this, owner);
            if (owner instanceof LivingEntity) {
                ((LivingEntity)owner).setLastHurtMob(hitEntity);
            }
        }
        return damagesource;
    }

    protected void onHitEntity(EntityRayTraceResult result) {
        if (this.itemEnchantments.containsKey(Enchantments.PIERCING))
            this.setPierceLevel(this.getItemEnchantments().get(Enchantments.PIERCING).byteValue());
        ArrowEntityAccessor accessor = (ArrowEntityAccessor) this;
        Entity entity = result.getEntity();
        int i = (int) this.baseDamage;
        if (!shouldKnockback && !this.getItemEnchantments().containsKey(Enchantments.PUNCH_ARROWS)) {
            // Add a tag to the entity for the purpose of cancelling knockback through an event listener.
            // This is not a perfect solution as tags can be edited in vanilla. Should probably figure out a better way to do this.
            entity.addTag("fngCancelKnockback");
        }
        if (this.getPierceLevel() > 0) {
            if (accessor.getPiercingIgnoreEntityIds() == null) {
                accessor.setPiercingIgnoreEntityIds(new IntOpenHashSet(5));
            }

            if (accessor.getPiercedAndKilledEntities() == null) {
                accessor.setPiercedAndKilledEntities(Lists.newArrayListWithCapacity(5));
            }

            if (accessor.getPiercingIgnoreEntityIds().size() >= this.getPierceLevel() + 1) {
                this.remove();
                return;
            }

            accessor.getPiercingIgnoreEntityIds().add(entity.getId());
        }

        if (this.isCritArrow()) {
            i += this.random.nextInt(i / 2 + 2);
        }

        Entity owner = this.getOwner();
        DamageSource damagesource = getDamageSource(entity);

        boolean targetIsEnderman = entity.getType() == EntityType.ENDERMAN;
        if (this.isOnFire() && !targetIsEnderman) {
            entity.setSecondsOnFire(5);
        }

        if (entity.hurt(damagesource, (float)i)) {
            if (targetIsEnderman) {
                return;
            }

            if (entity instanceof LivingEntity) {
                LivingEntity livingentity = (LivingEntity)entity;
                if (!this.level.isClientSide && this.isArrow) {
                    livingentity.setArrowCount(livingentity.getArrowCount() + 1);
                }

                if (accessor.getKnockback() > 0) {
                    Vector3d vector3d = this.getDeltaMovement().multiply(1.0D, 0.0D, 1.0D).normalize().scale((double)accessor.getKnockback() * 0.6D);
                    if (vector3d.lengthSqr() > 0.0D) {
                        livingentity.push(vector3d.x, 0.1D, vector3d.z);
                    }
                }

                if (!this.level.isClientSide && owner instanceof LivingEntity) {
                    EnchantmentHelper.doPostHurtEffects(livingentity, owner);
                    EnchantmentHelper.doPostDamageEffects((LivingEntity)owner, livingentity);
                }

                this.doPostHurtEffects(livingentity);
                if (owner != null && livingentity != owner && livingentity instanceof PlayerEntity && owner instanceof ServerPlayerEntity) {
                    ((ServerPlayerEntity)owner).connection.send(new SChangeGameStatePacket(SChangeGameStatePacket.ARROW_HIT_PLAYER, 0.0F));
                }

                if (!entity.isAlive() && accessor.getPiercedAndKilledEntities() != null) {
                    accessor.getPiercedAndKilledEntities().add(livingentity);
                }
            }

            entity.playSound(soundEvent, 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
            if (this.getPierceLevel() <= 0) {
                TickScheduler.schedule(1, this.level.isClientSide(), () -> {
                            this.remove();
                        }
                );
            }
        }
    }

    protected void onHitBlock(BlockRayTraceResult result) {
        super.onHitBlock(result);
        this.setSoundEvent(soundEvent);
        if (this.fragile) {
            this.remove();
        }
    }

    protected SoundEvent getDefaultHitGroundSoundEvent() {
        return SoundEvents.ARROW_HIT;
    }

    protected abstract ItemStack getPickupItem();

    public void setEnchantmentEffectsFromEntity(LivingEntity entity, float damage) {
        super.setEnchantmentEffectsFromEntity(entity, damage);
    }

    protected float getWaterInertia() {
        return this.waterInertia;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class ArrowEntityHandler {

        @SubscribeEvent
        public static void onArrowKnockEntity(LivingKnockBackEvent event) {
            LivingEntity entity = event.getEntityLiving();
            if (entity.getTags().contains("fngCancelKnockback")) {
                event.setCanceled(true);
                entity.removeTag("fngCancelKnockback");
            }
        }
    }
}
