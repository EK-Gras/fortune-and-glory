package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ItemParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FnGProjectileItemEntity extends ProjectileItemEntity implements IEntityAdditionalSpawnData, IEnchantmentProjectileData {

    private ItemStack projectileItem;
    private float scale;
    private int damage;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();
    private static final DataParameter<Byte> PIERCE_LEVEL = EntityDataManager.defineId(FnGProjectileItemEntity.class, DataSerializers.BYTE);
    private IntOpenHashSet piercingIgnoreEntityIds = new IntOpenHashSet(10);
    private List<Entity> piercedAndKilledEntities;

    public FnGProjectileItemEntity(EntityType<? extends FnGProjectileItemEntity> entity, World world) {
        super(entity, world);
    }

    public FnGProjectileItemEntity(World world, LivingEntity shooter, ItemStack itemStack, float scale, int damage) {
        super(FnGEntities.THROWABLE.get(), shooter, world);
        this.projectileItem = itemStack;
        this.scale = scale;
        this.damage = damage;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(PIERCE_LEVEL, (byte)0);
    }

    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeItemStack(projectileItem, false);
        buffer.writeFloat(scale);
    }

    public void readSpawnData(PacketBuffer buffer) {
        this.projectileItem = buffer.readItem();
        this.scale = buffer.readFloat();
    }

    protected Item getDefaultItem() {
        return FnGItems.BALL.get();
    }

    public ItemStack getItem() {
        return this.projectileItem;
    }

    public float getScale() {
        return this.scale;
    }

    @OnlyIn(Dist.CLIENT)
    private IParticleData getParticle() {
        ItemStack stack = this.projectileItem;
        return (stack.isEmpty() ? new ItemParticleData(ParticleTypes.ITEM, new ItemStack(FnGItems.BALL.get())) : new ItemParticleData(ParticleTypes.ITEM, stack));
    }

    @OnlyIn(Dist.CLIENT)
    public void handleEntityEvent(byte b) {
        if (b == 3) {
            IParticleData particle = this.getParticle();

            for(int i = 0; i < 8; ++i) {
                this.level.addParticle(particle, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D, 0.0D);
            }
        }

    }

    protected void onHitEntity(EntityRayTraceResult result) {
        super.onHitEntity(result);
        Entity entity = result.getEntity();
        if (this.piercingIgnoreEntityIds.contains(entity.getId())) return;
        entity.hurt(DamageSource.thrown(this, this.getOwner()), (float)damage);
        if (this.itemEnchantments.containsKey(Enchantments.PUNCH_ARROWS)) {
            Vector3d vector3d = this.getDeltaMovement().multiply(1.0D, 0.0D, 1.0D).normalize().scale(this.itemEnchantments.get(Enchantments.PUNCH_ARROWS) * 0.6D);
            if (vector3d.lengthSqr() > 0.0D) {
                entity.push(vector3d.x, 0.1D, vector3d.z);
            }
        }
        if (this.isOnFire()) entity.setSecondsOnFire(5);
        if (this.itemEnchantments.containsKey(Enchantments.PIERCING) && !this.level.isClientSide) {
            this.entityData.set(PIERCE_LEVEL, itemEnchantments.get(Enchantments.PIERCING).byteValue());
            if (this.piercingIgnoreEntityIds.size() >= this.entityData.get(PIERCE_LEVEL)) {
                this.level.broadcastEntityEvent(this, (byte)3);
                this.remove();
            }
            this.piercingIgnoreEntityIds.add(entity.getId());
        }
        else if (!this.level.isClientSide) {
            this.level.broadcastEntityEvent(this, (byte)3);
            this.remove();
        }
    }

    protected void onHitBlock(BlockRayTraceResult result) {
        if (!this.level.isClientSide) {
            this.level.broadcastEntityEvent(this, (byte)3);
            this.remove();
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
