package com.gitlab.ekgras.fortuneandglory.entity.ai.goal;

import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.TargetGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;

import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.function.Predicate;

public class MinionNearestAttackableGoal<T extends LivingEntity> extends TargetGoal {
    protected final int randomInterval;
    protected final Class<T> targetType;
    protected LivingEntity target;
    protected EntityPredicate targetConditions;
    protected MinionEntity mob;
    protected Predicate<LivingEntity> excludeOwner = (entity) -> {
        if (entity != null) {
            return !(entity.getUUID() == mob.getOwner());
        }
        return true;
    };

    public MinionNearestAttackableGoal(MinionEntity mob, Class<T> targetType, boolean mustSee, boolean excludePlayer) {
        this(mob, targetType, 10, mustSee, false, excludePlayer);
    }

    public MinionNearestAttackableGoal(MinionEntity mob, Class<T> targetType, int randomInterval, boolean mustSee, boolean mustReach, boolean excludePlayer) {
        super(mob, mustSee, mustReach);
        this.mob = mob;
        this.targetType = targetType;
        this.randomInterval = randomInterval;
        this.setFlags(EnumSet.of(Goal.Flag.TARGET));
        this.targetConditions = (new EntityPredicate()).range(this.getFollowDistance()).selector(excludeOwner);
    }

    public boolean canUse() {
        if (this.randomInterval > 0 && this.mob.getRandom().nextInt(this.randomInterval) != 0) {
            return false;
        } else {
            this.findTarget();
            return this.target != null;
        }
    }

    protected AxisAlignedBB getTargetSearchArea(double searchRange) {
        return this.mob.getBoundingBox().inflate(searchRange, 4.0D, searchRange);
    }

    protected void findTarget() {
        if (this.targetType != PlayerEntity.class && this.targetType != ServerPlayerEntity.class) {
            this.target = this.mob.level.getNearestLoadedEntity(this.targetType, this.targetConditions, this.mob, this.mob.getX(), this.mob.getEyeY(), this.mob.getZ(), this.getTargetSearchArea(this.getFollowDistance()));
        } else {
            this.target = this.mob.level.getNearestPlayer(this.targetConditions, this.mob, this.mob.getX(), this.mob.getEyeY(), this.mob.getZ());
        }

    }

    public void start() {
        this.mob.setTarget(this.target);
        super.start();
    }

    public void setTarget(@Nullable LivingEntity p_234054_1_) {
        this.target = p_234054_1_;
    }
}
