package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import com.gitlab.ekgras.fortuneandglory.items.BoomerangItem;
import com.gitlab.ekgras.fortuneandglory.util.TickScheduler;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.DamagingProjectileEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.*;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.HashMap;
import java.util.Map;

import static net.minecraft.util.Hand.MAIN_HAND;
import static net.minecraft.util.Hand.OFF_HAND;

public class BoomerangEntity extends DamagingProjectileEntity implements IEntityAdditionalSpawnData, IEnchantmentProjectileData {

    public LivingEntity shootingEntity;
    public BoomerangEntity following;
    private Vector3d followVec;
    private ItemStack boomerangItem;
    private int incrementsPassed;
    private boolean isReturning;
    private int betweenTicks;
    private int damage;
    private LivingEntity shooter;
    private Vector3d targetMotion;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    public BoomerangEntity(EntityType<? extends DamagingProjectileEntity> type, World world) {
        super(type, world);
    }

    @OnlyIn(Dist.CLIENT)
    public BoomerangEntity(World worldIn, double x, double y, double z, double accelX, double accelY, double accelZ) {
        super(FnGEntities.BOOMERANG.get(), x, y, z, accelX, accelY, accelZ, worldIn);
    }

    public BoomerangEntity(World worldIn, LivingEntity shooterIn, double accelX, double accelY, double accelZ, int damageIn, ItemStack itemStackIn) {
        super(FnGEntities.BOOMERANG.get(), shooterIn, accelX, accelY, accelZ, worldIn);
        this.shooter = shooterIn;
        this.boomerangItem = itemStackIn;
        this.damage = damageIn;
    }

    public ItemStack getItem() {
        return this.boomerangItem;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeItemStack(boomerangItem, false);
    }

    public void readSpawnData(PacketBuffer buffer) {
        this.boomerangItem = buffer.readItem();
    }

    protected void onHit(RayTraceResult result) {
        super.onHit(result);
        if (!this.level.isClientSide) {
            if (result.getType() == RayTraceResult.Type.ENTITY) {
                Entity entity = ((EntityRayTraceResult)result).getEntity();
                DamageSource damageSource = DamageSource.thrown(this, shooter);
                if (entity.invulnerableTime <= 0) {
                    if (this.itemEnchantments.containsKey(Enchantments.FLAMING_ARROWS)) {
                        entity.setSecondsOnFire(5);
                    }
                    if (this.itemEnchantments.containsKey(Enchantments.PUNCH_ARROWS)) {
                        Vector3d vector3d = entity.getPosition(1.0f).subtract(this.position()).multiply(1.0D, 0.0D, 1.0D).normalize().scale(itemEnchantments.get(Enchantments.PUNCH_ARROWS) * 0.6D);
                        if (vector3d.lengthSqr() > 0.0D) {
                            entity.push(vector3d.x, 0.1D, vector3d.z);
                        }
                    }
                }
                entity.hurt(damageSource, damage);
                boomerangItem.hurtAndBreak(1, shooter, (livingEntity) -> {
                    livingEntity.broadcastBreakEvent(shooter.getUsedItemHand());
                });
                this.doEnchantDamageEffects(this.shootingEntity, entity);
            }
            if (result.getType() == RayTraceResult.Type.BLOCK) {
                if (this.boomerangItem.getItem() instanceof BoomerangItem) {
                    BoomerangItem item = (BoomerangItem) boomerangItem.getItem();
                    World world = this.getCommandSenderWorld();
                    CompoundNBT tag = boomerangItem.getTag();
                    switch (item.getResult()) {
                        case DROP: {
                            tag.putBoolean("throwing", false);
                            ((PlayerEntity) shooter).getCooldowns().addCooldown(boomerangItem.getItem(), ((BoomerangItem) boomerangItem.getItem()).getReturnCooldown());
                            boomerangItem.hurtAndBreak(1, shooter, (entity) -> {
                                entity.broadcastBreakEvent(shooter.getUsedItemHand());
                            });
                            ItemEntity entity = new ItemEntity(world, this.getX(), this.getY(), this.getZ(), boomerangItem.split(1));
                            world.addFreshEntity(entity);
                            break;
                        }
                        case TELEPORT: {
                            if (shooter instanceof PlayerEntity) {
                                PlayerEntity player = (PlayerEntity) shooter;
                                tag.putBoolean("throwing", false);
                                boomerangItem.hurtAndBreak(1, shooter, (entity) -> {
                                    entity.broadcastBreakEvent(shooter.getUsedItemHand());
                                });
                                player.getCooldowns().addCooldown(boomerangItem.getItem(), ((BoomerangItem) boomerangItem.getItem()).getReturnCooldown());
                                if (!player.isCreative()) {
                                    if (shooter.getItemBySlot(tag.getBoolean("hand") ?
                                            EquipmentSlotType.MAINHAND : EquipmentSlotType.OFFHAND).isEmpty()) {
                                        shooter.setItemInHand(tag.getBoolean("hand") ? MAIN_HAND : OFF_HAND, boomerangItem.split(1));
                                    } else {
                                        ItemHandlerHelper.giveItemToPlayer((PlayerEntity) shooter, boomerangItem);
                                    }
                                }
                            }
                            break;
                        }
                        case RETURN: {
                            // I'm well aware this doesn't work because the entity is removed regardless.
                            // I need to figure out how this will work if the boomerang hits something while already returning.
                            this.isReturning = true;
                            break;
                        }
                    }
                }
                this.remove();
            }
        }
    }

    public boolean shouldBurn() {
        return false;
    }

    public void addAdditionalSaveData(CompoundNBT compound) {
        super.addAdditionalSaveData(compound);
        compound.putInt("Damage", this.damage);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readAdditionalSaveData(CompoundNBT compound) {
        super.readAdditionalSaveData(compound);
        if (compound.contains("Damage", 99)) {
            this.damage = compound.getInt("Damage");
        }

    }

    public void tick() {
        if (this.boomerangItem != null && this.shooter != null) {
            if (this.boomerangItem.getItem() instanceof BoomerangItem && this.shooter instanceof PlayerEntity) {
                BoomerangItem item = (BoomerangItem) boomerangItem.getItem();
                CompoundNBT tag = boomerangItem.getTag();
                Vector3d lookVector = following == null ? shooter.getLookAngle() : following.followVec == null ? shooter.getLookAngle() : following.followVec;
                if (following != null) {
                    if (following.followVec == null) {
                        this.incrementsPassed = 0;
                    }
                }
                int currentIncrement = item.getIncrementLength() * (incrementsPassed + 1);
                int betweenTicks = item.getBetweenTicks();

                if (this.betweenTicks <= 0) {
                    Vector3d targetPos = shooter.getEyePosition(1.0f).add(lookVector.scale(currentIncrement));
                    Vector3d newMotion = targetPos.subtract(this.position()).scale(1.0f / betweenTicks);

                    this.betweenTicks = betweenTicks;
                    this.targetMotion = newMotion;

                    if (this.incrementsPassed < item.getMaxIncrements() && !this.isReturning) {
                        this.incrementsPassed++;
                    } else if (this.incrementsPassed == item.getMaxIncrements() && !this.isReturning) {
                        this.isReturning = true;
                    }
                    if (this.isReturning) {
                        this.incrementsPassed--;
                    }
                    if (this.incrementsPassed < -2) {
                        if (shooter instanceof PlayerEntity && !((PlayerEntity) shooter).isCreative()) {
                            tag.putBoolean("throwing", false);
                            if (shooter.getItemBySlot(tag.getBoolean("hand") ?
                                    EquipmentSlotType.MAINHAND : EquipmentSlotType.OFFHAND).isEmpty()) {
                                shooter.setItemInHand(tag.getBoolean("hand") ? MAIN_HAND : OFF_HAND, boomerangItem.split(1));
                            }
                            else {
                                ItemHandlerHelper.giveItemToPlayer((PlayerEntity) shooter, boomerangItem);
                            }
                        }
                        this.remove();
                    }
                }
                this.setDeltaMovement(targetMotion);
                this.betweenTicks--;
                Vector3d pos = this.getPosition(1.0f);
                TickScheduler.schedule(10, this.getCommandSenderWorld().isClientSide, () -> {
                            this.followVec = lookVector;
                        }
                );
            }
            if (this.isOnFire()) {
                ServerWorld world = (ServerWorld) this.getCommandSenderWorld();
                world.sendParticles(ParticleTypes.FLAME, this.getRandomX(0.5D), this.getRandomY(), this.getRandomZ(0.5D), 0, 0, 0, 0, 1);
            }
        }

        RayTraceResult raytraceresult = ProjectileHelper.getHitResult(this, this::canHitEntity);
        if (raytraceresult.getType() != RayTraceResult.Type.MISS && !net.minecraftforge.event.ForgeEventFactory.onProjectileImpact(this, raytraceresult)) {
            this.onHit(raytraceresult);
        }

        Vector3d vec3d = this.getDeltaMovement();
        double d0 = this.getX() + vec3d.x;
        double d1 = this.getY() + vec3d.y;
        double d2 = this.getZ() + vec3d.z;
        ProjectileHelper.rotateTowardsMovement(this, 0.2F);

        if (this.isInWater()) {
            for (int i = 0; i < 4; ++i) {
                float f1 = 0.25F;
                this.level.addParticle(ParticleTypes.BUBBLE, d0 - vec3d.x * 0.25D, d1 - vec3d.y * 0.25D, d2 - vec3d.z * 0.25D, vec3d.x, vec3d.y, vec3d.z);
            }
        }

        this.setPos(d0, d1, d2);
    }

    @Override
    public boolean hurt(DamageSource source, float amount) {
        return false;
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

}
