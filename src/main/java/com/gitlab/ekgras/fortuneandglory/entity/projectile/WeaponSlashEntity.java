package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.DamagingProjectileEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.*;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.HashMap;
import java.util.Map;

public class WeaponSlashEntity extends DamagingProjectileEntity implements IEnchantmentProjectileData {

    public LivingEntity shootingEntity;
    private int ticksAlive;
    private int life;
    private int ticksInAir;
    public double accelerationX;
    public double accelerationY;
    public double accelerationZ;
    private int damage;
    private LivingEntity shooter;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    public WeaponSlashEntity(EntityType<? extends DamagingProjectileEntity> p_i50173_1_, World p_i50173_2_) {
        super(p_i50173_1_, p_i50173_2_);
    }

    @OnlyIn(Dist.CLIENT)
    public WeaponSlashEntity(World worldIn, double x, double y, double z, double accelX, double accelY, double accelZ) {
        super(FnGEntities.WEAPON_SLASH.get(), x, y, z, accelX, accelY, accelZ, worldIn);
    }

    public WeaponSlashEntity(World worldIn, LivingEntity shooterIn, double accelX, double accelY, double accelZ, int lifeIn, int damageIn) {
        super(FnGEntities.WEAPON_SLASH.get(), shooterIn, accelX, accelY, accelZ, worldIn);
        this.shooter = shooterIn;
        this.life = lifeIn;
        this.damage = damageIn;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }



    protected void onHit(RayTraceResult result) {
        super.onHit(result);
        if (!this.level.isClientSide) {
            if (result.getType() == RayTraceResult.Type.ENTITY) {
                Entity entity = ((EntityRayTraceResult)result).getEntity();
                DamageSource damageSource = DamageSource.thrown(this, shooter);
                entity.hurt(damageSource, damage);
                this.doEnchantDamageEffects(this.shootingEntity, entity);
            }
            if (result.getType() == RayTraceResult.Type.BLOCK) {
                this.remove();
            }
        }

    }

    public boolean shouldBurn() {
        return false;
    }

    public void addAdditionalSaveData(CompoundNBT compound) {
        super.addAdditionalSaveData(compound);
        compound.putInt("Damage", this.damage);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readAdditionalSaveData(CompoundNBT compound) {
        super.readAdditionalSaveData(compound);
        if (compound.contains("Damage", 99)) {
            this.damage = compound.getInt("Damage");
        }

    }

    public void tick() {

        this.ticksAlive++;
        if(this.ticksAlive == life) {
            this.remove();
        }

        RayTraceResult raytraceresult = ProjectileHelper.getHitResult(this, this::canHitEntity);
        if (raytraceresult.getType() != RayTraceResult.Type.MISS && !net.minecraftforge.event.ForgeEventFactory.onProjectileImpact(this, raytraceresult)) {
            this.onHit(raytraceresult);
        }

        Vector3d vec3d = this.getDeltaMovement();
        double d0 = this.getX() + vec3d.x;
        double d1 = this.getY() + vec3d.y;
        double d2 = this.getZ() + vec3d.z;
        ProjectileHelper.rotateTowardsMovement(this, 0.2F);
        float f = this.getInertia();
        if (this.isInWater()) {
            for(int i = 0; i < 4; ++i) {
                float f1 = 0.25F;
                this.level.addParticle(ParticleTypes.BUBBLE, d0 - vec3d.x * 0.25D, d1 - vec3d.y * 0.25D, d2 - vec3d.z * 0.25D, vec3d.x, vec3d.y, vec3d.z);
            }

            f = 0.8F;
        }

        //this.setMotion(vec3d.add(this.accelerationX, this.accelerationY, this.accelerationZ).scale((double)f));
        this.setPos(d0, d1, d2);
    }

    @Override
    public boolean hurt(DamageSource source, float amount) {
        return false;
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

}
