package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEnchantments;
import net.minecraft.command.arguments.EntityAnchorArgument;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.HashMap;
import java.util.Map;

public class FnGFishingBobberEntity extends FishingBobberEntity implements IEnchantmentProjectileData {

    private float pullStrength;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    private Entity hookedIn;

    @OnlyIn(Dist.CLIENT)
    public FnGFishingBobberEntity(World world, PlayerEntity player, double x, double y, double z) {
        super(world, player, x, y, z);
        this.setPos(x, y, z);
        this.xo = this.getX();
        this.yo = this.getY();
        this.zo = this.getZ();
    }

    public FnGFishingBobberEntity(PlayerEntity player, World world, int luck, int lureSpeed, float pullStrength, float throwPower) {
        super(player, world, luck, lureSpeed);
        float f = player.xRot;
        float f1 = player.yRot;
        float f2 = MathHelper.cos(-f1 * ((float)Math.PI / 180F) - (float)Math.PI);
        float f3 = MathHelper.sin(-f1 * ((float)Math.PI / 180F) - (float)Math.PI);
        float f4 = -MathHelper.cos(-f * ((float)Math.PI / 180F));
        float f5 = MathHelper.sin(-f * ((float)Math.PI / 180F));
        double d0 = player.getX() - (double)f3 * 0.3D;
        double d1 = player.getEyeY();
        double d2 = player.getZ() - (double)f2 * 0.3D;
        this.moveTo(d0, d1, d2, f1, f);
        Vector3d vector3d = new Vector3d((double)(-f3), (double)MathHelper.clamp(-(f5 / f4), -5.0F, 5.0F), (double)(-f2));
        double d3 = vector3d.length();
        vector3d = vector3d.multiply(0.6D / d3 + 0.5D + this.random.nextGaussian() * 0.0045D, 0.6D / d3 + 0.5D + this.random.nextGaussian() * 0.0045D, 0.6D / d3 + 0.5D + this.random.nextGaussian() * 0.0045D);
        this.setDeltaMovement(vector3d.scale(throwPower));
        this.yRot = (float)(MathHelper.atan2(vector3d.x, vector3d.z) * (double)(180F / (float)Math.PI));
        this.xRot = (float)(MathHelper.atan2(vector3d.y, (double)MathHelper.sqrt(getHorizontalDistanceSqr(vector3d))) * (double)(180F / (float)Math.PI));
        this.yRotO = this.yRot;
        this.xRotO = this.xRot;
        this.pullStrength = pullStrength;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }


    @Override
    protected void bringInHookedEntity() {
        Entity entity = this.getOwner();
        if (entity != null) {
            Vector3d vector3d = (new Vector3d(entity.getX() - this.getX(), entity.getY() - this.getY(), entity.getZ() - this.getZ())).scale(pullStrength);
            super.getHookedIn().setDeltaMovement(super.getHookedIn().getDeltaMovement().add(vector3d));
        }
    }

    @Override
    public void onHitEntity(EntityRayTraceResult result) {
        super.onHitEntity(result);
        Entity target = result.getEntity();
        if (this.getItemEnchantments().containsKey(Enchantments.SHARPNESS)) {
            DamageSource damageSource = DamageSource.thrown(this, this.getOwner());
            target.hurt(damageSource, this.getItemEnchantments().get(Enchantments.SHARPNESS));
        }
        if (this.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
            target.setSecondsOnFire(5);
        if (this.getItemEnchantments().containsKey(Enchantments.PUNCH_ARROWS)) {
            Vector3d vector3d = getOwner().getLookAngle().multiply(1.0D, 0.0D, 1.0D).normalize().scale((double)this.getItemEnchantments().get(Enchantments.PUNCH_ARROWS) * 0.6D);
            if (vector3d.lengthSqr() > 0.0D) {
                target.push(vector3d.x, 0.1D, vector3d.z);
            }
        }
    }

}
