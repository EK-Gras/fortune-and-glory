package com.gitlab.ekgras.fortuneandglory.entity.monster.minion;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.entity.ai.goal.MinionAttackGoal;
import com.gitlab.ekgras.fortuneandglory.entity.ai.goal.MinionNearestAttackableGoal;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.IEnchantmentProjectileData;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.mixin.accessors.MobEntityAccessor;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.*;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MinionEntity extends CreatureEntity implements IEntityAdditionalSpawnData, IEnchantmentProjectileData {

    private UUID owner;
    private ItemStack summoningItem;
    private PlayerEntity ownerEntity;
    private final AttributeModifierManager attributes;
    private int despawnTime;
    private int life;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    public MinionEntity(EntityType<? extends MinionEntity> entityType, World world) {
        super(entityType, world);
        this.attributes = new AttributeModifierManager(GlobalEntityTypeAttributes.getSupplier(entityType));
    }

    public MinionEntity(World world, UUID owner) {
        this(FnGEntities.MINION.get(), world);
        this.owner = owner;
    }

    public MinionEntity(World world, UUID owner, PlayerEntity ownerEntity, ItemStack summoningItem, int despawnTime) {
        this(FnGEntities.MINION.get(), world);
        this.owner = owner;
        this.ownerEntity = ownerEntity;
        this.summoningItem = summoningItem;
        this.despawnTime = despawnTime;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    protected void registerGoals() {
        this.goalSelector.addGoal(8, new LookRandomlyGoal(this));
        this.addBehaviourGoals();
    }

    protected void addBehaviourGoals() {
        this.goalSelector.addGoal(2, new MinionAttackGoal(this, 1.0D, false));
        this.targetSelector.addGoal(1, (new HurtByTargetGoal(this)));
        this.targetSelector.addGoal(2, new MinionNearestAttackableGoal<>(this, MonsterEntity.class, true, false));
        this.targetSelector.addGoal(2, new MinionNearestAttackableGoal<>(this, PlayerEntity.class, true, true));
    }

    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeUUID(owner);
        buffer.writeItemStack(summoningItem, false);
        buffer.writeInt(despawnTime);
    }

    public void readSpawnData(PacketBuffer buffer) {
        owner = buffer.readUUID();
        summoningItem = buffer.readItem();
        despawnTime = buffer.readInt();
    }

    public static AttributeModifierMap.MutableAttribute createAttributes() {
        return MonsterEntity.createMonsterAttributes().add(Attributes.FOLLOW_RANGE, 35.0D).add(Attributes.MAX_HEALTH, 8D).add(Attributes.MOVEMENT_SPEED, 0.35D).add(Attributes.ATTACK_DAMAGE, 3.0D).add(Attributes.ARMOR, 2.0D);
    }

    public UUID getOwner() {
        return this.owner;
    }

    public void addAdditionalSaveData(CompoundNBT tag) {
        super.addAdditionalSaveData(tag);
        tag.putUUID("owner", owner);
        tag.putInt("life", life);
    }

    public void readAdditionalSaveData(CompoundNBT tag) {
        super.readAdditionalSaveData(tag);
        this.owner = tag.getUUID("owner");
        this.life = tag.getInt("life");
    }

    @Nullable
    public ILivingEntityData finalizeSpawn(IServerWorld world, DifficultyInstance difficulty, SpawnReason reason, @Nullable ILivingEntityData data, @Nullable CompoundNBT tag) {
        data = super.finalizeSpawn(world, difficulty, reason, data, tag);
        return data;
    }

    @Override
    public void tick() {
        super.tick();
        // Entities removed on the server also get removed on the client but not the other way around. So it is safe to only do this on the server
        if (!this.getCommandSenderWorld().isClientSide) {
            this.life++;
            if (this.life >= this.despawnTime)
                this.remove();
        }
    }

    @Override
    public boolean doHurtTarget(Entity pEntity) {
        float f = (float)this.getAttributeValue(Attributes.ATTACK_DAMAGE);
        float f1 = (float)this.getAttributeValue(Attributes.ATTACK_KNOCKBACK);
        if (pEntity instanceof LivingEntity) {
            f += EnchantmentHelper.getDamageBonus(this.getMainHandItem(), ((LivingEntity)pEntity).getMobType());
            f1 += (float)EnchantmentHelper.getKnockbackBonus(this);
        }

        int i = EnchantmentHelper.getFireAspect(this);
        if (this.getItemEnchantments().containsKey(Enchantments.FLAMING_ARROWS))
            i += 1;
        if (i > 0) {
            pEntity.setSecondsOnFire(i * 4);
        }

        boolean flag = pEntity.hurt(DamageSource.thrown(this, this.ownerEntity), f);
        if (flag) {
            if (f1 > 0.0F && pEntity instanceof LivingEntity) {
                ((LivingEntity)pEntity).knockback(f1 * 0.5F, (double) MathHelper.sin(this.yRot * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(this.yRot * ((float)Math.PI / 180F))));
                this.setDeltaMovement(this.getDeltaMovement().multiply(0.6D, 1.0D, 0.6D));
            }

            if (pEntity instanceof PlayerEntity) {
                PlayerEntity playerentity = (PlayerEntity)pEntity;
//                ((MobEntityAccessor) this).maybeDisableShield(playerentity, this.getMainHandItem(), playerentity.isUsingItem() ? playerentity.getUseItem() : ItemStack.EMPTY);
            }

            this.doEnchantDamageEffects(this, pEntity);
            this.setLastHurtMob(pEntity);
        }

        return flag;
    }

    @Mod.EventBusSubscriber(modid = FortuneAndGlory.MODID)
    public static class MinionHandler {

        @SubscribeEvent
        public static void onLivingAttack(LivingAttackEvent event) {
            if (event.getSource().getEntity() instanceof PlayerEntity && event.getEntity() instanceof MinionEntity) {
                PlayerEntity player = (PlayerEntity) event.getSource().getEntity();
                MinionEntity minion = (MinionEntity) event.getEntity();
                event.setCanceled(player.getUUID() == minion.getOwner());
            }
            if (event.getSource().getEntity() instanceof MinionEntity) {
                MinionEntity minion = (MinionEntity) event.getSource().getEntity();
                Entity target = event.getEntity();
                Map<Enchantment, Integer> enchants = minion.getItemEnchantments();
                if (enchants.containsKey(Enchantments.FLAMING_ARROWS)) {
                    target.setSecondsOnFire(5);
                }
                if (enchants.containsKey(Enchantments.PUNCH_ARROWS)) {
                    Vector3d vector3d = minion.getLookAngle().multiply(1.0D, 0.0D, 1.0D).normalize().scale(enchants.get(Enchantments.PUNCH_ARROWS) * 0.6D);
                    if (vector3d.lengthSqr() > 0.0D) {
                        target.push(vector3d.x, 0.1D, vector3d.z);
                    }
                }
            }
        }
    }
}
