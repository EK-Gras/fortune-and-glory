package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.google.common.collect.Sets;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import org.lwjgl.system.CallbackI;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BowArrowEntity extends ArrowEntity implements IEnchantmentProjectileData {
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    public BowArrowEntity(EntityType<? extends BowArrowEntity> type, World world) {
        super(type, world);
    }

    public BowArrowEntity(World world, double x, double y, double z) {
        super(world, x, y, z);
    }

    public BowArrowEntity(World world, LivingEntity entity) {
        super(world, entity);
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    public void onHitEntity(EntityRayTraceResult result) {
        if (this.itemEnchantments.containsKey(Enchantments.PIERCING) && this.isCritArrow())
            this.setPierceLevel(this.getItemEnchantments().get(Enchantments.PIERCING).byteValue());
        super.onHitEntity(result);
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    public static BowArrowEntity fromArrow(ArrowEntity entity, ItemStack stack) {
        BowArrowEntity arrowEntity = new BowArrowEntity(entity.level, (LivingEntity) entity.getOwner());
        arrowEntity.setEffectsFromItem(stack);
        arrowEntity.setCritArrow(entity.isCritArrow());
        arrowEntity.pickup = entity.pickup;
        return arrowEntity;
    }

}
