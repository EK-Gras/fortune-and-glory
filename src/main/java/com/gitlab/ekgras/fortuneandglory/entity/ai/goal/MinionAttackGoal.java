package com.gitlab.ekgras.fortuneandglory.entity.ai.goal;

import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.monster.ZombieEntity;

public class MinionAttackGoal extends MeleeAttackGoal {
    private final MinionEntity minion;
    private int raiseArmTicks;

    public MinionAttackGoal(MinionEntity entity, double speedModifier, boolean followingTargetEvenIfNotSeen) {
        super(entity, speedModifier, followingTargetEvenIfNotSeen);
        this.minion = entity;
    }

    public void start() {
        super.start();
        this.raiseArmTicks = 0;
    }

    public void stop() {
        super.stop();
        this.minion.setAggressive(false);
    }

    public void tick() {
        super.tick();
        ++this.raiseArmTicks;
        if (this.raiseArmTicks >= 5 && this.getTicksUntilNextAttack() < this.getAttackInterval() / 2) {
            this.minion.setAggressive(true);
        } else {
            this.minion.setAggressive(false);
        }

    }
}
