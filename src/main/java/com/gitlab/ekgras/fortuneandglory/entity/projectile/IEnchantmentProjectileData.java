package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import net.minecraft.enchantment.Enchantment;

import java.util.Map;

public interface IEnchantmentProjectileData {
    public Map<Enchantment, Integer> getItemEnchantments();

    public void setItemEnchantments(Map<Enchantment, Integer> enchants);
}
