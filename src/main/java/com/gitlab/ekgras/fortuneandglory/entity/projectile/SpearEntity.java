package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.SpearItem;
import com.gitlab.ekgras.fortuneandglory.mixin.accessors.ArrowEntityAccessor;
import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class SpearEntity extends AbstractArrowEntity implements IEnchantmentProjectileData{

    private static final DataParameter<Byte> ID_LOYALTY;
    private static final DataParameter<Boolean> ID_FOIL;
    private ItemStack spearItem;
    private boolean dealtDamage;
    private final float damage;
    public int clientSideReturnSpearTickCount;
    private Map<Enchantment, Integer> itemEnchantments = new HashMap<>();

    public SpearEntity(EntityType<? extends SpearEntity> type, World world) {
        super(type, world);
        this.damage = 8.0F;
    }

    public SpearEntity(World world, LivingEntity entity, ItemStack stack) {
        super(FnGEntities.SPEAR.get(), entity, world);
        this.spearItem = stack.copy();
        this.itemEnchantments = EnchantmentHelper.getEnchantments(stack);
        this.entityData.set(ID_LOYALTY, (byte)EnchantmentHelper.getLoyalty(stack));
        this.entityData.set(ID_FOIL, stack.hasFoil());
        if (stack.getItem() instanceof SpearItem) {
            this.damage = ((SpearItem)stack.getItem()).getThrowDamage();
        }
        else {
            this.damage = 8.0F;
        }
    }

    @OnlyIn(Dist.CLIENT)
    public SpearEntity(World world, double x, double y, double z) {
        super(FnGEntities.SPEAR.get(), x, y, z, world);
        this.damage = 8.0F;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchants) {
        this.itemEnchantments = enchants;
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(ID_LOYALTY, (byte)0);
        this.entityData.define(ID_FOIL, false);
    }

    public void tick() {
        if (this.inGroundTime > 4) {
            this.dealtDamage = true;
        }

        Entity owner = this.getOwner();
        if ((this.dealtDamage || this.isNoPhysics()) && owner != null) {
            int loyaltyLevel = this.entityData.get(ID_LOYALTY);
            if (loyaltyLevel > 0 && !this.isAcceptibleReturnOwner()) {
                if (!this.level.isClientSide && this.pickup == PickupStatus.ALLOWED) {
                    this.spawnAtLocation(this.getPickupItem(), 0.1F);
                }

                this.remove();
            } else if (loyaltyLevel > 0) {
                this.setNoPhysics(true);
                Vector3d vec = new Vector3d(owner.getX() - this.getX(), owner.getEyeY() - this.getY(), owner.getZ() - this.getZ());
                this.setPosRaw(this.getX(), this.getY() + vec.y * 0.015D * (double)loyaltyLevel, this.getZ());
                if (this.level.isClientSide) {
                    this.yOld = this.getY();
                }

                double loyaltyMotion = 0.05D * (double)loyaltyLevel;
                this.setDeltaMovement(this.getDeltaMovement().scale(0.95D).add(vec.normalize().scale(loyaltyMotion)));
                if (this.clientSideReturnSpearTickCount == 0) {
                    this.playSound(SoundEvents.TRIDENT_RETURN, 10.0F, 1.0F);
                }

                ++this.clientSideReturnSpearTickCount;
            }
        }

        super.tick();
    }

    private boolean isAcceptibleReturnOwner() {
        Entity owner = this.getOwner();
        if (owner != null && owner.isAlive()) {
            return !(owner instanceof ServerPlayerEntity) || !owner.isSpectator();
        } else {
            return false;
        }
    }

    protected ItemStack getPickupItem() {
        return this.spearItem.copy();
    }

    @OnlyIn(Dist.CLIENT)
    public boolean isFoil() {
        return this.entityData.get(ID_FOIL);
    }

    @Nullable
    protected EntityRayTraceResult findHitEntity(Vector3d vec1, Vector3d vec2) {
        return this.dealtDamage ? null : super.findHitEntity(vec1, vec2);
    }

    protected void onHitEntity(EntityRayTraceResult result) {
        if (this.itemEnchantments.containsKey(Enchantments.PIERCING))
            this.setPierceLevel(this.getItemEnchantments().get(Enchantments.PIERCING).byteValue());
        Entity entity = result.getEntity();
        float damage = this.damage;
        ArrowEntityAccessor accessor = (ArrowEntityAccessor)this;

        if (this.getPierceLevel() > 0) {
            if (accessor.getPiercingIgnoreEntityIds() == null) {
                accessor.setPiercingIgnoreEntityIds(new IntOpenHashSet(5));
            }

            if (accessor.getPiercedAndKilledEntities() == null) {
                accessor.setPiercedAndKilledEntities(Lists.newArrayListWithCapacity(5));
            }

            if (accessor.getPiercingIgnoreEntityIds().size() >= this.getPierceLevel() + 1) {
                return;
            }

            accessor.getPiercingIgnoreEntityIds().add(entity.getId());
        }

        Entity owner = this.getOwner();
        DamageSource source = DamageSource.trident(this, (Entity)(owner == null ? this : owner));
        this.dealtDamage = true;
        SoundEvent sound = SoundEvents.TRIDENT_HIT;
        if (entity.hurt(source, damage)) {
            if (entity.getType() == EntityType.ENDERMAN) {
                return;
            }

            if (entity instanceof LivingEntity) {
                LivingEntity livingEntity = (LivingEntity) entity;
                if (owner instanceof LivingEntity) {
                    EnchantmentHelper.doPostHurtEffects(livingEntity, owner);
                    EnchantmentHelper.doPostDamageEffects((LivingEntity)owner, livingEntity);
                }

                if (accessor.getKnockback() > 0) {
                    Vector3d vector3d = this.getDeltaMovement().multiply(1.0D, 0.0D, 1.0D).normalize().scale((double)accessor.getKnockback() * 0.6D);
                    if (vector3d.lengthSqr() > 0.0D) {
                        livingEntity.push(vector3d.x, 0.1D, vector3d.z);
                    }
                }

                this.doPostHurtEffects(livingEntity);

                if (!entity.isAlive() && accessor.getPiercedAndKilledEntities() != null) {
                    accessor.getPiercedAndKilledEntities().add(livingEntity);
                }
            }
        }

        boolean flag = entity.getType() == EntityType.ENDERMAN;
        if (this.isOnFire() && !flag) {
            entity.setSecondsOnFire(5);
        }

        if (this.getPierceLevel() < (accessor.getPiercingIgnoreEntityIds() == null ? 1 : accessor.getPiercingIgnoreEntityIds().size()))
            this.setDeltaMovement(this.getDeltaMovement().multiply(-0.01D, -0.1D, -0.01D));
        else this.dealtDamage = false;
        float volume = 1.0F;
        if (this.level instanceof ServerWorld && this.level.isThundering() && EnchantmentHelper.hasChanneling(this.spearItem)) {
            BlockPos pos = entity.blockPosition();
            if (this.level.canSeeSky(pos)) {
                LightningBoltEntity lightning = (LightningBoltEntity)EntityType.LIGHTNING_BOLT.create(this.level);
                lightning.moveTo(Vector3d.atBottomCenterOf(pos));
                lightning.setCause(owner instanceof ServerPlayerEntity ? (ServerPlayerEntity)owner : null);
                this.level.addFreshEntity(lightning);
                sound = SoundEvents.TRIDENT_THUNDER;
                volume = 5.0F;
            }
        }
        this.playSound(sound, volume, 1.0F);
    }

    protected SoundEvent getDefaultHitGroundSoundEvent() {
        return SoundEvents.TRIDENT_HIT_GROUND;
    }

    public void playerTouch(PlayerEntity player) {
        Entity owner = this.getOwner();
        if (owner == null || owner.getUUID() == player.getUUID()) {
            super.playerTouch(player);
        }
    }

    public void readAdditionalSaveData(CompoundNBT nbt) {
        super.readAdditionalSaveData(nbt);
        if (nbt.contains("Trident", 10)) {
            this.spearItem = ItemStack.of(nbt.getCompound("Trident"));
        }

        this.dealtDamage = nbt.getBoolean("DealtDamage");
        this.entityData.set(ID_LOYALTY, (byte)EnchantmentHelper.getLoyalty(this.spearItem));
    }

    public void addAdditionalSaveData(CompoundNBT nbt) {
        super.addAdditionalSaveData(nbt);
        nbt.put("Trident", this.spearItem.save(new CompoundNBT()));
        nbt.putBoolean("DealtDamage", this.dealtDamage);
    }

    public void tickDespawn() {
        int loyaltyLevel = (Byte)this.entityData.get(ID_LOYALTY);
        if (this.pickup != PickupStatus.ALLOWED || loyaltyLevel <= 0) {
            super.tickDespawn();
        }

    }

    protected float getWaterInertia() {
        return 0.99F;
    }

    @OnlyIn(Dist.CLIENT)
    public boolean shouldRender(double xModifier, double yModifier, double zModifier) {
        return true;
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    static {
        ID_LOYALTY = EntityDataManager.defineId(SpearEntity.class, DataSerializers.BYTE);
        ID_FOIL = EntityDataManager.defineId(SpearEntity.class, DataSerializers.BOOLEAN);
    }
}
