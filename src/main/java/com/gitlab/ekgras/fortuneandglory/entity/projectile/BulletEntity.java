package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.init.FnGItems;
import com.gitlab.ekgras.fortuneandglory.init.FnGSounds;
import com.gitlab.ekgras.fortuneandglory.network.packet.FireBulletPacket;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Rotations;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.List;

public class BulletEntity extends FnGArrowEntity implements IEntityAdditionalSpawnData {

    private static final DataParameter<Rotations> INITIAL_MOTION = EntityDataManager.defineId(BulletEntity.class, DataSerializers.ROTATIONS);
    private BlockState inBlockState;
    private IntOpenHashSet piercedEntities;
    private List<Entity> hitEntities;
    private double baseDamage;
    private int knockbackStrength = 0;
    private int life;
    private int despawnTime;
    private float red;
    private float green;
    private float blue;

    /**
     * This is the entity that was in the crosshairs when the player shot the gun. If it gets hit by this bullet,
     * the damage should be ignored since it already got hit by the hitscan component in
     * {@link FireBulletPacket#processMessage(FireBulletPacket, ServerPlayerEntity)}
     */
    @Nullable
    private Entity targetEntity;

    public BulletEntity(EntityType<? extends FnGArrowEntity> entityIn, World worldIn) {
        super(entityIn, worldIn);
    }

    public BulletEntity(LivingEntity shooterIn, World worldIn, int despawnTimeIn, double damageIn, float redIn, float greenIn, float blueIn, @Nullable Entity target) {
        super(FnGEntities.BULLET.get(), shooterIn, worldIn, false, false, despawnTimeIn, true, 0.6F, damageIn, true, false, false);
        this.targetEntity = target;
        this.red = redIn;
        this.green = greenIn;
        this.blue = blueIn;
    }

    public void writeSpawnData(PacketBuffer buffer) {
        super.writeSpawnData(buffer);
        buffer.writeFloat(red);
        buffer.writeFloat(green);
        buffer.writeFloat(blue);
    }

    public void readSpawnData(PacketBuffer buffer) {
        super.readSpawnData(buffer);
        red = buffer.readFloat();
        green = buffer.readFloat();
        blue = buffer.readFloat();
    }

    public void shoot(Entity shooter, float pitch, float yaw, float velocity, Vector3d inaccuracyVec) {
        float x = -MathHelper.sin(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        float y = -MathHelper.sin(pitch * ((float)Math.PI / 180F));
        float z = MathHelper.cos(yaw * ((float)Math.PI / 180F)) * MathHelper.cos(pitch * ((float)Math.PI / 180F));
        this.life = 0;
        Vector3d vector3d = (new Vector3d(x, y, z)).normalize().add(inaccuracyVec).scale((double)velocity);
        this.setDeltaMovement(vector3d);
        float f = MathHelper.sqrt(getHorizontalDistanceSqr(vector3d));
        this.yRot = (float)(MathHelper.atan2(vector3d.x, vector3d.z) * (double)(180F / (float)Math.PI));
        this.xRot = (float)(MathHelper.atan2(vector3d.y, (double)f) * (double)(180F / (float)Math.PI));
        this.yRotO = this.yRot;
        this.xRotO = this.xRot;
        this.entityData.set(INITIAL_MOTION, new Rotations((float)this.getDeltaMovement().x, (float)this.getDeltaMovement().y, (float)this.getDeltaMovement().z));
    }

    public void shoot(Entity shooter, Vector3d lookVector, float velocity) {
        this.life = 0;
        Vector3d vector3d = lookVector.normalize().scale((double)velocity);
        this.setDeltaMovement(vector3d);
        float f = MathHelper.sqrt(getHorizontalDistanceSqr(vector3d));
        this.yRot = (float)(MathHelper.atan2(vector3d.x, vector3d.z) * (double)(180F / (float)Math.PI));
        this.xRot = (float)(MathHelper.atan2(vector3d.y, (double)f) * (double)(180F / (float)Math.PI));
        this.yRotO = this.yRot;
        this.xRotO = this.xRot;
        this.entityData.set(INITIAL_MOTION, new Rotations((float)this.getDeltaMovement().x, (float)this.getDeltaMovement().y, (float)this.getDeltaMovement().z));
    }

    @Override
    public ItemStack getPickupItem() {
        return new ItemStack(FnGItems.BULLET.get());
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    protected SoundEvent getDefaultHitGroundSoundEvent() {
        return FnGSounds.ENTITY_BULLET_HIT.get();
    }

    @Override
    public void onHitEntity(EntityRayTraceResult result) {
        Entity entity = result.getEntity();

//        if (entity.equals(targetEntity)) return;

        super.onHitEntity(result);

        entity.invulnerableTime = 0;
        if (!this.getItemEnchantments().containsKey(Enchantments.PIERCING)) this.remove();
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(INITIAL_MOTION, new Rotations(0, 0, 0));
    }

    public void tick() {
        super.tick();

        Rotations r = this.entityData.get(INITIAL_MOTION);
        this.setDeltaMovement(r.getX(), r.getY(), r.getZ());

        if (this.level.isClientSide && !this.inGround) {
            Vector3d vector3d = this.getDeltaMovement();
            double d3 = vector3d.x;
            double d4 = vector3d.y;
            double d0 = vector3d.z;
            for(int i = 0; i < 4; ++i) {
                this.level.addParticle(new RedstoneParticleData(red, green, blue, 1), this.getX() + d3 * (double)i / 4.0D, this.getY() + d4 * (double)i / 4.0D, this.getZ() + d0 * (double)i / 4.0D, 0, 0, 0);
            }
        }

        if (this.inGround) {
            this.remove();
        }
    }


}
