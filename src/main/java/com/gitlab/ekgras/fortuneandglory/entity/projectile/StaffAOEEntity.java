package com.gitlab.ekgras.fortuneandglory.entity.projectile;

import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.items.StaffItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.Map;
import java.util.Optional;

/**
 *
 */

public class StaffAOEEntity extends Entity implements IEnchantmentProjectileData {

    public Map<Enchantment, Integer> itemEnchantments;

    public StaffAOEEntity(EntityType<?> type, World world) {
        super(type, world);
    }

    public StaffAOEEntity(World world) {
        super(FnGEntities.STAFF_AOE_EFFECT.get(), world);
    }

    public void setItemEnchantments(Map<Enchantment, Integer> enchantments) {
        this.itemEnchantments = enchantments;
    }

    public Map<Enchantment, Integer> getItemEnchantments() {
        return this.itemEnchantments;
    }

    protected void defineSynchedData() {

    }

    protected void readAdditionalSaveData(CompoundNBT tag) {

    }

    protected void addAdditionalSaveData(CompoundNBT tag) {

    }

    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

}
