package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.init.FnGSounds;
import com.gitlab.ekgras.fortuneandglory.items.GunItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class ReloadGunPacket {

    private static PlayerEntity sPlayerEntity;

    private PlayerEntity playerEntity;

    public ReloadGunPacket() {

    }

    public ReloadGunPacket(PacketBuffer buf) {

    }

    public PlayerEntity getPlayerEntity() {
        return sPlayerEntity;
    }

    public void encode(PacketBuffer buf) {
    }

    public static void handle(ReloadGunPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Work that needs to be threadsafe (most work)
            ServerPlayerEntity sender = ctx.get().getSender(); // the client that sent this packet
            sPlayerEntity = (PlayerEntity) sender;
            // do stuff
            processMessage(sender);
        });
        ctx.get().setPacketHandled(true);
    }

    public static void processMessage (ServerPlayerEntity player) {
        if (player.getItemInHand(Hand.MAIN_HAND).getItem() instanceof GunItem) {
            ItemStack stack = player.getItemInHand(Hand.MAIN_HAND);
            GunItem item = (GunItem) stack.getItem();
            CompoundNBT tag = stack.getTag();
            if (!tag.getBoolean("isReloading") && tag.getInt("bulletsLoaded") < item.getCapacity()) {
                World world = player.getCommandSenderWorld();
                tag.putBoolean("isReloading", true);
                tag.putInt("reloadCooldown", item.getReloadSpeed());
                world.playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ITEM_GUN_START_RELOAD.get(), SoundCategory.PLAYERS, 2.0F, 1.0F);
            }
        }
    }
}
