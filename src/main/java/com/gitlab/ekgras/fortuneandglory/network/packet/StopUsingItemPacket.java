package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.world.NoteBlockEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.List;
import java.util.function.Supplier;

public class StopUsingItemPacket {

    int entityId;

    public StopUsingItemPacket(int entityId) {
        this.entityId = entityId;
    }

    public StopUsingItemPacket(PacketBuffer buf) {
        this.decode(buf);
    }

    public void encode(PacketBuffer buf) {
        buf.writeInt(entityId);
    }

    public void decode(PacketBuffer buf) {
        this.entityId = buf.readInt();
    }

    public static void handle(StopUsingItemPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Make sure it's only executed on the physical client
            DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> StopUsingItemPacket.handlePacket(msg, ctx));
        });
        ctx.get().setPacketHandled(true);
    }

    // In ClientPacketHandlerClass
    public static void handlePacket(StopUsingItemPacket msg, Supplier<NetworkEvent.Context> ctx) {
        LivingEntity entity = (LivingEntity) Minecraft.getInstance().level.getEntity(msg.entityId);
        entity.stopUsingItem();
    }
}
