package com.gitlab.ekgras.fortuneandglory.network;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.network.packet.*;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;

import java.util.function.Supplier;

public class FnGPacketHandler {
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(FortuneAndGlory.MODID, "main"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

    public static void RegisterMessages() {
        int i = 0;
        INSTANCE.registerMessage(i++, FireBulletPacket.class, FireBulletPacket::encode, FireBulletPacket::new, FireBulletPacket::handle);
        INSTANCE.registerMessage(i++, UseBadgePacket.class, UseBadgePacket::encode, UseBadgePacket::decode, UseBadgePacket::handle);
        INSTANCE.registerMessage(i++, RemoveMinionsOfUUIDPacket.class, RemoveMinionsOfUUIDPacket::encode, RemoveMinionsOfUUIDPacket::new, RemoveMinionsOfUUIDPacket::handle);
        INSTANCE.registerMessage(i++, ReloadGunPacket.class, ReloadGunPacket::encode, ReloadGunPacket::new, ReloadGunPacket::handle);
        INSTANCE.registerMessage(i++, UseGunPacket.class, UseGunPacket::encode, UseGunPacket::decode, UseGunPacket::handle);
        INSTANCE.registerMessage(i++, StopUsingItemPacket.class, StopUsingItemPacket::encode, StopUsingItemPacket::new, StopUsingItemPacket::handle);
        INSTANCE.registerMessage(i++, UpdateElementalPercentagesPacket.class, UpdateElementalPercentagesPacket::encode, UpdateElementalPercentagesPacket::new, UpdateElementalPercentagesPacket::handle);
    }

    public static void sendToServer(Object msg){
        INSTANCE.sendToServer(msg);
    }

    // to all players tracking the specified entity
    public static void sendToAllTrackingEntity(Object message, Supplier<Entity> entity) {
        INSTANCE.send(PacketDistributor.TRACKING_ENTITY.with(entity), message);
    }

}
