package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.items.BadgeItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class UseBadgePacket {

    private static PlayerEntity sPlayerEntity;

    private PlayerEntity playerEntity;

    ItemStack stack;

    public UseBadgePacket(ItemStack stack) {
        this.stack = stack;
    }

    public PlayerEntity getPlayerEntity() {
        return sPlayerEntity;
    }

    public void encode(PacketBuffer buf) {
        buf.writeItemStack(stack, false);
    }

    public static UseBadgePacket decode(PacketBuffer buf) {
        return new UseBadgePacket(buf.readItem());
    }

    public static void handle(UseBadgePacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Work that needs to be threadsafe (most work)
            ServerPlayerEntity sender = ctx.get().getSender(); // the client that sent this packet
            sPlayerEntity = (PlayerEntity) sender;
            // do stuff
            processMessage(sender, msg.stack);
        });
        ctx.get().setPacketHandled(true);
    }

    public static void processMessage (ServerPlayerEntity player, ItemStack stack) {
        if (stack.getItem() instanceof BadgeItem) {
            ((BadgeItem) stack.getItem()).useBadge(player, stack);
        }
    }
}
