package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BulletEntity;
import com.gitlab.ekgras.fortuneandglory.items.GunItem;
import com.gitlab.ekgras.fortuneandglory.util.RaytraceUtils;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;

import javax.annotation.Nullable;
import java.util.Random;
import java.util.function.Supplier;

public class FireBulletPacket {

    /**
     * The entity that was in the crosshairs
     */
    @Nullable
    private Entity targetEntity;

    PacketBuffer buf;
    private boolean shotFromAutomatic;
    Vector3d traceVec;

    public FireBulletPacket() {

    }

    public FireBulletPacket(Entity targetEntity, boolean shotFromAutomatic, Vector3d traceVec) {
        this.targetEntity = targetEntity;
        this.shotFromAutomatic = shotFromAutomatic;
        this.traceVec = traceVec;
    }

    public static FireBulletPacket create(GunItem holding, boolean shotFromAutomatic, Vector3d lookVector) {
        Random random = new Random();
        Vector3d inaccuracyVec = new Vector3d(random.nextGaussian() * (double)0.0075F * (double)holding.getInaccuracy(), random.nextGaussian() * (double)0.0075F * (double)holding.getInaccuracy(), random.nextGaussian() * (double)0.0075F * (double)holding.getInaccuracy());
        Vector3d traceVec = lookVector.add(inaccuracyVec);
        EntityRayTraceResult trace = RaytraceUtils.getCrosshairTargetEntity(holding.getRange(), traceVec);
        if (trace == null) {
            return new FireBulletPacket((Entity) null, shotFromAutomatic, traceVec);
        }
        return new FireBulletPacket(trace.getEntity(), shotFromAutomatic, traceVec);
    }

    public FireBulletPacket(PacketBuffer buf) {
        this.buf = buf;
    }

    public void encode(PacketBuffer buf) {
        buf.writeBoolean(shotFromAutomatic);
        if (targetEntity != null) {
            buf.writeBoolean(true);
            buf.writeInt(targetEntity.getId());
        } else {
            buf.writeBoolean(false);
        }
        buf.writeDouble(traceVec.x);
        buf.writeDouble(traceVec.y);
        buf.writeDouble(traceVec.z);
    }

    /**
     * Runs on the server side, fill in the targetEntity field using the PacketByteBuf
     */
    private void decode(ServerPlayerEntity sender) {
        this.shotFromAutomatic = this.buf.readBoolean();
        boolean hasTargetEntity = this.buf.readBoolean();
        if (hasTargetEntity) {
            int targetId = this.buf.readInt();
            this.targetEntity = sender.getCommandSenderWorld().getEntity(targetId);
        }
        this.traceVec = new Vector3d(buf.readDouble(), buf.readDouble(), buf.readDouble());
    }

    public static void handle(FireBulletPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Work that needs to be threadsafe (most work)
            ServerPlayerEntity sender = ctx.get().getSender(); // the client that sent this packet
            // do stuff
            processMessage(msg, sender);
        });
        ctx.get().setPacketHandled(true);
    }

    public static void processMessage(FireBulletPacket msg, ServerPlayerEntity player) {
        msg.decode(player);

        GunItem gun = (GunItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
        World world = player.getLevel();
        ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
        CompoundNBT tag = stack.getTag();
        if (tag.getInt("bulletsLoaded") > 0 && !tag.getBoolean("isReloading") && !tag.getBoolean("isCooldown") && msg.shotFromAutomatic == gun.isAutomatic()) {

            float velocity = gun.getVelocity();

                Entity targetEntity = msg.targetEntity;
                BulletEntity bulletentity = new BulletEntity(player, world, (int) (gun.getRange() / (velocity * 3)),
                gun.getDamage(), gun.getRed(), gun.getGreen(), gun.getBlue(), targetEntity);
                bulletentity.shoot(player, msg.traceVec, velocity * 3.0F);

                bulletentity.setBaseDamage(gun.getDamage());
                bulletentity.setItemEnchantments(EnchantmentHelper.getEnchantments(stack));

                int j = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, stack);
                if (j > 0) {
                    bulletentity.setBaseDamage(bulletentity.getBaseDamage() + (double) j * 0.5D + 0.5D / (velocity * 3.0F));
                }

                int k = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, stack);
                if (k > 0) {
                    bulletentity.setKnockback(k - 1);
                }

                if (EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, stack) > 0) {
                    bulletentity.setSecondsOnFire(100);
                }

                world.addFreshEntity(bulletentity);

                // region Hit scan
                if (targetEntity != null) {
                    EntityRayTraceResult result = new EntityRayTraceResult(targetEntity);
                    bulletentity.onHitEntity(result);
                }
                // endregion
        }
    }
}
