package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import com.gitlab.ekgras.fortuneandglory.init.FnGEntities;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.List;
import java.util.function.Supplier;

public class RemoveMinionsOfUUIDPacket {

    private static PlayerEntity sPlayerEntity;

    private PlayerEntity playerEntity;

    public RemoveMinionsOfUUIDPacket() {

    }

    public RemoveMinionsOfUUIDPacket(PacketBuffer buf) {

    }

    public PlayerEntity getPlayerEntity() {
        return sPlayerEntity;
    }

    public void encode(PacketBuffer buf) {
    }

    public static void handle(RemoveMinionsOfUUIDPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Work that needs to be threadsafe (most work)
            ServerPlayerEntity sender = ctx.get().getSender(); // the client that sent this packet
            sPlayerEntity = (PlayerEntity) sender;
            // do stuff
            processMessage(sender);
        });
        ctx.get().setPacketHandled(true);
    }

    public static void processMessage (ServerPlayerEntity player) {
        ServerWorld world = (ServerWorld) player.getCommandSenderWorld();
        List<Entity> entityList = world.getEntities(FnGEntities.MINION.get(), (entity) -> {
            return true;
        });
        for (Entity entity : entityList) {
            if (entity instanceof MinionEntity) {
                MinionEntity minion = (MinionEntity) entity;
                if (minion.getOwner() == player.getUUID()) {
                    minion.remove();
                }
            }
        }
    }
}
