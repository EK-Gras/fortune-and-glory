package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.event.BulletShotEvent;
import com.gitlab.ekgras.fortuneandglory.init.FnGSounds;
import com.gitlab.ekgras.fortuneandglory.items.GunItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.stats.Stats;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class UseGunPacket {

    private static PlayerEntity sPlayerEntity;

    boolean shotFromAutomatic;

    public UseGunPacket(boolean shotFromAutomatic) {
        this.shotFromAutomatic = shotFromAutomatic;
    }

    public PlayerEntity getPlayerEntity() {
        return sPlayerEntity;
    }

    public void encode(PacketBuffer buf) {
        buf.writeBoolean(shotFromAutomatic);
    }

    public static UseGunPacket decode(PacketBuffer buf) {
        return new UseGunPacket(buf.readBoolean());
    }

    public static void handle(UseGunPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Work that needs to be threadsafe (most work)
            ServerPlayerEntity sender = ctx.get().getSender(); // the client that sent this packet
            sPlayerEntity = sender;
            // do stuff
            processMessage(msg.shotFromAutomatic, sender);
        });
        ctx.get().setPacketHandled(true);
    }

    public static void processMessage(boolean shotFromAutomatic, ServerPlayerEntity player) {
        GunItem gun = (GunItem) player.getItemBySlot(EquipmentSlotType.MAINHAND).getItem();
        ItemStack itemStack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
        World world = player.getLevel();
        ItemStack stack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
        CompoundNBT tag = stack.getTag();
        if (tag.getInt("bulletsLoaded") > 0 && !tag.getBoolean("isReloading") && !tag.getBoolean("isCooldown") && shotFromAutomatic == gun.isAutomatic()) {

            stack.hurtAndBreak(1, player, (hand) -> {
                hand.broadcastBreakEvent(player.getUsedItemHand());
            });

            tag.putInt("bulletsLoaded", tag.getInt("bulletsLoaded") - 1);

            // If the gun still has bullets loaded, then put it in cooldown
            if (tag.getInt("bulletsLoaded") > 0) {
                tag.putBoolean("isCooldown", true);
            }

            // If the gun has no bullets loaded but is not currently reloaded, start reloading it.
            // isReloading and reloadCooldown need to be separate flags so there is a difference between when the gun has not yet started reloading and when the gun is finished reloading.
            if (tag.getInt("bulletsLoaded") <= 0 && !tag.getBoolean("isReloading")) {
                tag.putBoolean("isReloading", true);
                tag.putInt("reloadCooldown", gun.getReloadSpeed());
                world.playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ITEM_GUN_START_RELOAD.get(), SoundCategory.PLAYERS, 2.0F, 1.0F);
            }

            player.awardStat(Stats.ITEM_USED.get(gun));
        }
        world.playSound(null, player.getX(), player.getY(), player.getZ(), FnGSounds.ENTITY_BULLET_SHOOT.get(), SoundCategory.PLAYERS, 0.5F, 1.0F);

        MinecraftForge.EVENT_BUS.post(new BulletShotEvent(player, itemStack, world));
    }
}
