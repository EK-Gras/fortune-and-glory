package com.gitlab.ekgras.fortuneandglory.network.packet;

import com.gitlab.ekgras.fortuneandglory.util.ElementalPercentages;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class UpdateElementalPercentagesPacket {
    int entityId;
    float freezePercentage;
    float shockPercentage;

    public UpdateElementalPercentagesPacket(PacketBuffer buf) {
        this.decode(buf);
    }
    public UpdateElementalPercentagesPacket(int entityId, float freezePercentage, float shockPercentage) {
        this.entityId = entityId;
        this.freezePercentage = freezePercentage;
        this.shockPercentage = shockPercentage;
    }

    public void encode(PacketBuffer buf) {
        buf.writeInt(this.entityId);
        buf.writeFloat(this.freezePercentage);
        buf.writeFloat(this.shockPercentage);
    }

    private void decode(PacketBuffer buf) {
        this.entityId = buf.readInt();
        this.freezePercentage = buf.readFloat();
        this.shockPercentage = buf.readFloat();
    }

    // In Packet class
    public static void handle(UpdateElementalPercentagesPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            // Make sure it's only executed on the physical client
            DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> UpdateElementalPercentagesPacket.handlePacket(msg, ctx));
        });
        ctx.get().setPacketHandled(true);
    }

    // In ClientPacketHandlerClass
    public static void handlePacket(UpdateElementalPercentagesPacket msg, Supplier<NetworkEvent.Context> ctx) {
        ElementalPercentages toUpdate = (ElementalPercentages) Minecraft.getInstance().level.getEntity(msg.entityId);
        toUpdate.setFreezePercentage(msg.freezePercentage);
        toUpdate.setShockPercentage(msg.shockPercentage);
    }
}
