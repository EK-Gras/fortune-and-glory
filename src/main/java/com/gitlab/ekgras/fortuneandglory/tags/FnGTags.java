package com.gitlab.ekgras.fortuneandglory.tags;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;

public class FnGTags {
    public static void init() {}

    public static final Tag<Block> PLANTS = (Tag<Block>) BlockTags.getAllTags().getTag(new ResourceLocation("fortune-and-glory", "plants"));
    public static final Tag<Item> BADGES = (Tag<Item>) ItemTags.getAllTags().getTag(new ResourceLocation("fortune-and-glory", "badge"));
}
