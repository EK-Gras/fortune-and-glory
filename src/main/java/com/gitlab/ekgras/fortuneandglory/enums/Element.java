package com.gitlab.ekgras.fortuneandglory.enums;

public enum Element {
    FREEZE,
    SHOCK
}
