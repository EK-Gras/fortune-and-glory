package com.gitlab.ekgras.fortuneandglory.enums;

public enum BoomerangHitResult {
    RETURN,
    TELEPORT,
    DROP,
}
