package com.gitlab.ekgras.fortuneandglory.enums;

/**
 * Used for badges to determine how the badge loses durability.
 * TICK means durability is consumed every tick,
 * USE means durability type is consumed only when the badge is used.
 */
public enum UsageType {
    TICK,
    USE
}
