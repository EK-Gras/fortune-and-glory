//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.FnGProjectileItemEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.BlockItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3f;

public class ItemProjectileRenderer<T extends FnGProjectileItemEntity> extends EntityRenderer<T> {
    private final ItemRenderer itemRenderer;
    private final float scale;
    private final boolean fullBright;

    public ItemProjectileRenderer(EntityRendererManager managerIn, ItemRenderer itemRendererIn, float scaleIn, boolean fullBrightIn) {
        super(managerIn);
        this.itemRenderer = itemRendererIn;
        this.scale = scaleIn;
        this.fullBright = fullBrightIn;
    }

    public ItemProjectileRenderer(EntityRendererManager managerIn, ItemRenderer itemRendererIn) {
        this(managerIn, itemRendererIn, 1.0F, false);
    }

    protected int getBlockLightLevel(T entity, BlockPos pos) {
        return this.fullBright ? 15 : super.getBlockLightLevel(entity, pos);
    }

    public void render(T entity, float f1, float f2, MatrixStack stack, IRenderTypeBuffer buffer, int i) {
        if (entity.tickCount >= 2 || this.entityRenderDispatcher.camera.getEntity().distanceToSqr(entity) >= 12.25D) {
            stack.pushPose();
            stack.scale(entity.getScale(), entity.getScale(), entity.getScale());
            if (!(entity.getItem().getItem() instanceof BlockItem)) {
                stack.mulPose(this.entityRenderDispatcher.cameraOrientation());
            }
            stack.mulPose(Vector3f.YP.rotationDegrees(180.0F));
            this.itemRenderer.renderStatic(entity.getItem(), TransformType.GROUND, i, OverlayTexture.NO_OVERLAY, stack, buffer);
            stack.popPose();
            super.render(entity, f1, f2, stack, buffer, i);
        }
    }

    public ResourceLocation getTextureLocation(T entity) {
        return AtlasTexture.LOCATION_BLOCKS;
    }
}
