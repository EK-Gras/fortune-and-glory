package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.model.SpearModel;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.SpearEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class SpearRenderer extends EntityRenderer<SpearEntity> {
    public static final ResourceLocation SPEAR_LOCATION = new ResourceLocation(FortuneAndGlory.MODID, "textures/entity/iron_spear.png");
    private final SpearModel model = new SpearModel();

    public SpearRenderer(EntityRendererManager p_i48828_1_) {
        super(p_i48828_1_);
    }

    public void render(SpearEntity entityIn, float f1In, float f2In, MatrixStack stackIn, IRenderTypeBuffer bufferIn, int i1In) {
        stackIn.pushPose();
        stackIn.mulPose(Vector3f.YP.rotationDegrees(MathHelper.lerp(f2In, entityIn.yRotO, entityIn.yRot) - 90.0F));
        stackIn.mulPose(Vector3f.ZP.rotationDegrees(MathHelper.lerp(f2In, entityIn.xRotO, entityIn.xRot) + 90.0F));
        IVertexBuilder builder = ItemRenderer.getFoilBufferDirect(bufferIn, this.model.renderType(this.getTextureLocation(entityIn)), false, entityIn.isFoil());
        this.model.renderToBuffer(stackIn, builder, i1In, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        stackIn.popPose();
        super.render(entityIn, f1In, f2In, stackIn, bufferIn, i1In);
    }

    public ResourceLocation getTextureLocation(SpearEntity p_110775_1_) {
        return SPEAR_LOCATION;
    }
}
