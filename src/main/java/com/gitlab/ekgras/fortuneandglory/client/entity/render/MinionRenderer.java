package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.model.MinionModel;
import com.gitlab.ekgras.fortuneandglory.entity.monster.minion.MinionEntity;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.client.renderer.entity.model.ZombieModel;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class MinionRenderer extends BipedRenderer<MinionEntity, MinionModel> {
    private static final ResourceLocation MINION_LOCATION = new ResourceLocation(FortuneAndGlory.MODID, "textures/entity/iron_minion.png");

    public MinionRenderer(EntityRendererManager manager) {
        this(manager, new MinionModel(RenderType::entityCutoutNoCull), new MinionModel(RenderType::entityCutoutNoCull), new MinionModel(RenderType::entityCutoutNoCull));
    }

    public MinionRenderer(EntityRendererManager manager, MinionModel model1, MinionModel model2, MinionModel model3) {
        super(manager, model1, 0.5F);
        this.addLayer(new BipedArmorLayer<>(this, model2, model3));
    }

    public ResourceLocation getTextureLocation(MinionEntity entity) {
        return MINION_LOCATION;
    }

}
