// Made with Blockbench 3.9.3
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports
package com.gitlab.ekgras.fortuneandglory.client.entity.model;


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class EnergyBallModel extends Model {
	private final ModelRenderer renderer;

	public EnergyBallModel() {
		super(RenderType::entitySolid);
		texWidth = 16;
		texHeight = 16;

		renderer = new ModelRenderer(this);
		renderer.setPos(0.0F, 2.0F, 0.0F);
		renderer.texOffs(0, 0).addBox(-1.0F, -4.0F, -1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		renderer.texOffs(0, 6).addBox(-1.0F, -3.0F, -2.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		renderer.texOffs(0, 12).addBox(-2.0F, -3.0F, -1.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		renderer.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}
}