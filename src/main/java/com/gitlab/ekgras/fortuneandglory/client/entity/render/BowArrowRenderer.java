package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.BowArrowEntity;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BowArrowRenderer extends ArrowRenderer<BowArrowEntity> {
   public static final ResourceLocation NORMAL_ARROW_LOCATION = new ResourceLocation("textures/entity/projectiles/arrow.png");
   public static final ResourceLocation TIPPED_ARROW_LOCATION = new ResourceLocation("textures/entity/projectiles/tipped_arrow.png");

   public BowArrowRenderer(EntityRendererManager manager) {
      super(manager);
   }

   public ResourceLocation getTextureLocation(BowArrowEntity entity) {
      return entity.getColor() > 0 ? TIPPED_ARROW_LOCATION : NORMAL_ARROW_LOCATION;
   }
}
