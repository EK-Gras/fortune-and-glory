// Made with Blockbench 3.5.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports
package com.gitlab.ekgras.fortuneandglory.client.entity.model;

import com.gitlab.ekgras.fortuneandglory.entity.projectile.WeaponSlashEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WeaponSlashEntityModel extends EntityModel<WeaponSlashEntity> {
	private final ModelRenderer bb_main;

	public WeaponSlashEntityModel() {
		texWidth = 16;
		texHeight = 32;

		bb_main = new ModelRenderer(this);
		bb_main.setPos(0.0F, 24.0F, 0.0F);
		bb_main.texOffs(1, 1).addBox(-3.0F, -30.0F, 0.5F, 6.0F, 30.0F, 0.0F, 0.0F, false);
		bb_main.texOffs(14, 2).addBox(0.0F, -28.0F, 0.0F, 0.0F, 26.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void setupAnim(WeaponSlashEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}
}