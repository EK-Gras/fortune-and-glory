package com.gitlab.ekgras.fortuneandglory.client.renderer;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import org.lwjgl.opengl.GL11;

public class IceRenderType extends RenderType {
    public IceRenderType(String id, VertexFormat format, int mode, int bufferSize, boolean affectsCrumbling, boolean sortOnUpload, Runnable setupState, Runnable clearState) {
        super(id, format, mode, bufferSize, affectsCrumbling, sortOnUpload, setupState, clearState);
    }

//    public static final RenderType ICE_LAYER = create("ice_overlay",
//            DefaultVertexFormats.POSITION_COLOR, GL11.GL_LINES, 256,
//            RenderType.State.builder().
//                );
}
