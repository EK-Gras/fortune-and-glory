package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.model.WeaponSlashEntityModel;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.BoomerangEntity;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.WeaponSlashEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.SpriteRenderer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BoomerangRenderer extends EntityRenderer<BoomerangEntity> {

    private final ItemRenderer itemRenderer;
    private final float scale;
    private final boolean fullBright;

    public BoomerangRenderer(EntityRendererManager rendererManagerIn, ItemRenderer itemRendererIn, float scaleIn, boolean fullBrightIn) {
        super(rendererManagerIn);
        this.itemRenderer = itemRendererIn;
        this.scale = scaleIn;
        this.fullBright = fullBrightIn;
    }

    public BoomerangRenderer(EntityRendererManager renderManagerIn, ItemRenderer itemRendererIn) {
        this(renderManagerIn, itemRendererIn, 1.0F, false);
    }

    protected int getBlockLightLevel(BoomerangEntity entityIn, BlockPos posIn) {
        return this.fullBright ? 15 : super.getBlockLightLevel(entityIn, posIn);
    }

    public void render(BoomerangEntity entityIn, float f1, float f2, MatrixStack stackIn, IRenderTypeBuffer bufferIn, int i1) {
        if (entityIn.tickCount >= 2 || this.entityRenderDispatcher.camera.getEntity().distanceToSqr(entityIn) >= 12.25D) {
            Matrix4f matrix = stackIn.last().pose();
            stackIn.pushPose();
            stackIn.scale(this.scale, this.scale, this.scale);
            stackIn.translate(0, 0.125, 0);
            stackIn.mulPose(Vector3f.YN.rotationDegrees(entityIn.yRot + 90));
            entityIn.getAddEntityPacket();
            this.itemRenderer.renderStatic((entityIn).getItem(), ItemCameraTransforms.TransformType.GROUND, i1, OverlayTexture.NO_OVERLAY, stackIn, bufferIn);
            stackIn.popPose();
            super.render(entityIn, f1, f2, stackIn, bufferIn, i1);
        }
    }

    /**
     * Returns the location of an entity's texture.
     */
    public ResourceLocation getTextureLocation(BoomerangEntity entity) {
        return AtlasTexture.LOCATION_BLOCKS;
    }

}
