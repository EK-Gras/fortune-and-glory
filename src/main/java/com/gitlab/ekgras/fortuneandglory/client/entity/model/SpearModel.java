// Made with Blockbench
package com.gitlab.ekgras.fortuneandglory.client.entity.model;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class SpearModel extends Model {
	public static final ResourceLocation TEXTURE = new ResourceLocation(FortuneAndGlory.MODID, "textures/entity/iron_spear");
	private final ModelRenderer renderer;

	public SpearModel() {
		super(RenderType::entitySolid);
		texWidth = 32;
		texHeight = 32;

		renderer = new ModelRenderer(this);
		renderer.setPos(0F, 8.5F, 0F);
		renderer.texOffs(0, 0).addBox(-0.5F, -15.5F, -0.5F, 1.0F, 31.0F, 1.0F, 0.0F, true);
		renderer.texOffs(4, 0).addBox(-1.5F, -10.5F, -0.5F, 3.0F, 2.0F, 1.0F, 0.0F, true);
		renderer.texOffs(4, 8).addBox(-0.5F, -10.5F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, true);
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		renderer.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}
}