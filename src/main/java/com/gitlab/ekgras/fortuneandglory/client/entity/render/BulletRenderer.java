package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.model.BulletModel;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.BulletEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BulletRenderer extends EntityRenderer<BulletEntity> {
    public static final ResourceLocation BULLET_LOCATION = new ResourceLocation(FortuneAndGlory.MODID, "textures/entity/bullet.png");
    private final BulletModel model = new BulletModel();

    public BulletRenderer(EntityRendererManager manager) {
        super(manager);
    }

    public void render(BulletEntity entityIn, float yawIn, float pitchIn, MatrixStack stackIn, IRenderTypeBuffer bufferIn, int i1In) {
        stackIn.pushPose();
        stackIn.mulPose(Vector3f.YP.rotationDegrees(MathHelper.lerp(pitchIn, entityIn.yRotO, entityIn.yRot) - 90.0F));
        stackIn.mulPose(Vector3f.ZP.rotationDegrees(MathHelper.lerp(pitchIn, entityIn.xRotO, entityIn.xRot) + 90.0F));
        IVertexBuilder builder = ItemRenderer.getFoilBufferDirect(bufferIn, this.model.renderType(this.getTextureLocation(entityIn)), false, false);
        this.model.renderToBuffer(stackIn, builder, i1In, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        stackIn.popPose();
        super.render(entityIn, yawIn, pitchIn, stackIn, bufferIn, i1In);
    }

    public ResourceLocation getTextureLocation(BulletEntity entity) {
        return BULLET_LOCATION;
    }
}
