package com.gitlab.ekgras.fortuneandglory.client.entity.model;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.extensions.IForgeUnbakedModel;

public class IronGauntletModel extends Model implements IForgeUnbakedModel {
    public static final ResourceLocation TEXTURE = new ResourceLocation(FortuneAndGlory.MODID, "textures/item/iron_gauntlet_hand");
    private final ModelRenderer renderer;

    public IronGauntletModel() {
        super(RenderType::entitySolid);
        texWidth = 16;
        texHeight = 16;

        renderer = new ModelRenderer(this);
        renderer.setPos(0.0F, 24.0F, 0.0F);
        renderer.texOffs(0, 10).addBox(-10.5F, -8.5F, 7.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);
        renderer.texOffs(0, 3).addBox(-10.05F, -8.05F, 12.0F, 4.0F, 4.0F, 1.0F, 0.0F, false);
        renderer.texOffs(0, 12).addBox(-10.5F, -8.5F, 13.0F, 5.0F, 5.0F, 2.0F, 0.0F, false);
    }

    @Override
    public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        renderer.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
}
