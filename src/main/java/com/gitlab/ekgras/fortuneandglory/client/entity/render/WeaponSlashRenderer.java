package com.gitlab.ekgras.fortuneandglory.client.entity.render;

import com.gitlab.ekgras.fortuneandglory.FortuneAndGlory;
import com.gitlab.ekgras.fortuneandglory.client.entity.model.WeaponSlashEntityModel;
import com.gitlab.ekgras.fortuneandglory.entity.projectile.WeaponSlashEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WeaponSlashRenderer extends EntityRenderer<WeaponSlashEntity> {
    public static final ResourceLocation RES_WEAPON_SLASH = new ResourceLocation(FortuneAndGlory.MODID, "textures/entity/weapon_slash.png");
    private final WeaponSlashEntityModel model = new WeaponSlashEntityModel();

    public WeaponSlashRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    public void render(WeaponSlashEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
        matrixStackIn.pushPose();
        matrixStackIn.mulPose(Vector3f.YN.rotationDegrees(entityIn.yRot + 90));
//        matrixStackIn.translate(0, 0.0f, 0);
        IVertexBuilder ivertexbuilder = net.minecraft.client.renderer.ItemRenderer.getFoilBuffer(bufferIn, this.model.renderType(this.getTextureLocation(entityIn)), false, false);
        this.model.renderToBuffer(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        matrixStackIn.popPose();
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    }

    /**
     * Returns the location of an entity's texture.
     */
    public ResourceLocation getTextureLocation(WeaponSlashEntity entity) {
        return RES_WEAPON_SLASH;
    }
}
